DROP TABLE IF EXISTS users;

CREATE TABLE users (
  id SERIAL,
  email varchar(256) NOT NULL DEFAULT '',
  hash varchar(256) NOT NULL DEFAULT '',
  telegram_id BIGINT NOT NULL UNIQUE,
  name varchar(256),
  surname varchar(256),
  username varchar(256),
  type varchar(50),
  PRIMARY KEY (id)
);

ALTER TABLE users ADD created_at timestamp default current_timestamp;
ALTER TABLE users ADD status INTEGER default 0;

---------------------------------------------------------------------------------------

DROP TABLE IF EXISTS sessions;

CREATE TABLE sessions (
  telegram_id BIGINT references users(telegram_id),
  status INTEGER
);

---------------------------------------------------------------------------------------

DROP TABLE IF EXISTS corsi;

CREATE TABLE corsi (
  id SERIAL,
  nome varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (id)
);

---------------------------------------------------------------------------------------

DROP TABLE IF EXISTS users_corsi;

CREATE TABLE users_corsi (
  users_id INTEGER DEFAULT NULL,
  corsi_id INTEGER DEFAULT NULL,
  CONSTRAINT corsi_id FOREIGN KEY (corsi_id) REFERENCES corsi (id) ON DELETE set null ON UPDATE set null,
  CONSTRAINT users_id FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE set null ON UPDATE set null
);
CREATE UNIQUE INDEX users_corsi_idx ON users_corsi (users_id, corsi_id);

---------------------------------------------------------------------------------------

DROP TABLE IF EXISTS dipartimenti;

CREATE TABLE dipartimenti (
  id SERIAL,
  rss_link varchar(256) NOT NULL DEFAULT '',
  nome varchar(256) NOT NULL DEFAULT '',
  url varchar(26) DEFAULT NULL,
  id_uni INTEGER DEFAULT NULL,
  dipartimenti_id INTEGER DEFAULT NULL,
  PRIMARY KEY (id)
);
CREATE UNIQUE INDEX id_uni_idx ON dipartimenti (id_uni);

---------------------------------------------------------------------------------------

DROP TABLE IF EXISTS users_dipart;

CREATE TABLE users_dipart (
  users_id INTEGER DEFAULT NULL,
  dipart_id INTEGER DEFAULT NULL,
  CONSTRAINT dipart_id FOREIGN KEY (dipart_id) REFERENCES dipartimenti (id) ON DELETE set null ON UPDATE set null,
  CONSTRAINT users_id FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE set null ON UPDATE set null
);
CREATE UNIQUE INDEX users_dipart_idx ON users_dipart (users_id, dipart_id);

---------------------------------------------------------------------------------------

DROP TABLE IF EXISTS corsi_dipart;

CREATE TABLE corsi_dipart (
  id SERIAL,
  corsi_id INTEGER DEFAULT NULL,
  dipart_id INTEGER DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT corsi_id FOREIGN KEY (corsi_id) REFERENCES corsi (id) ON DELETE set null ON UPDATE set null,
  CONSTRAINT dipart_id FOREIGN KEY (dipart_id) REFERENCES dipartimenti (id) ON DELETE set null ON UPDATE set null
);
CREATE UNIQUE INDEX corsi_dipart_idx ON corsi_dipart (corsi_id,dipart_id);

---------------------------------------------------------------------------------------

DROP TABLE IF EXISTS insegnamenti;

CREATE TABLE insegnamenti (
  id SERIAL,
  corsi_id INTEGER NOT NULL,
  nome varchar(256) DEFAULT NULL,
  name varchar(256) DEFAULT NULL,
  PRIMARY KEY (id)
);

---------------------------------------------------------------------------------------

DROP TABLE IF EXISTS users_insegnamenti;

CREATE TABLE users_insegnamenti (
  id SERIAL,
  users_id INTEGER DEFAULT NULL,
  insegnamenti_id INTEGER DEFAULT NULL,
  PRIMARY KEY (id)
);
