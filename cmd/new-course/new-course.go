package main

import (
	"bufio"
	"fmt"
	"github.com/olekukonko/tablewriter"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
	"gitlab.com/giovanni-liboni/univrnews/pkg/utils"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	var ds *googlecloud.DatastoreGC
	var err error
	// ctx := context.Background()

	ds, err = googlecloud.NewDS()
	if err != nil {
		log.Fatalln("googlecloud.NewDS :", err)
	}

	// Retrieve the list of department
	deps, err := ds.GetAllDepartments()
	if err != nil {
		log.Fatalln("GetAllDepartments() :", err)
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"ID", "Name", "URL"})
	for _, dep := range deps {
		// fmt.Println("(" + strconv.FormatInt(dep.ID, 10) + ")\t" + dep.Name + "\t\t" + dep.URL)
		table.Append([]string{strconv.FormatInt(dep.ID, 10), dep.Name, dep.URL})
	}
	table.Render()
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter department ID: ")
	text, _ := reader.ReadString('\n')
	fmt.Println(text)
	table.ClearRows()
	table.ClearFooter()


	depID, err := strconv.ParseInt(strings.TrimSuffix(text, "\n"), 10, 32)
	if err != nil {
		log.Fatalln("ID not valid :", err)
	}

	found := false
	for _, dep := range deps {
		if (dep.ID == depID) {
			found = true
			err = dep.Load(ds)
			if err != nil {
				log.Fatalln("Error during loading department:", err)
			}
			depCourses, err := dep.GetCourses(ds)
			if err != nil {
				log.Fatalln("GetCourses:", err)
			}
			table = tablewriter.NewWriter(os.Stdout)
			table.SetHeader([]string{"ID", "Name", "Notices URL", "Crawling URL", "Users"})
			for _, c := range depCourses {
				table.Append([]string{strconv.FormatInt(c.ID, 10), c.Name, c.NoticesURL, c.CrawlingURL, strconv.Itoa(len(c.FollowersKeys))})
			}
			table.Render()
			fmt.Println("Add a course to", dep.Name)
			table.ClearRows()
		}
	}
	if !found {
		log.Fatalln("No department matches with the provided ID")
	}




	var course googlecloud.Course

	// Ask for the new course
	fmt.Println("Ex. 954")
	fmt.Print("Enter new course ID: ")
	courseID, _ := reader.ReadString('\n')
	fmt.Println("New course name:", courseID)

	course.ID, err = strconv.ParseInt(strings.TrimSuffix(courseID, "\n"), 10, 32)
	if err != nil {
		log.Fatalln("Course ID not valid :", err)
	}

	// Ask for the new course
	fmt.Println("Ex. Master's degree in Computer Engineering for Robotics and Smart Industry")
	fmt.Print("Enter new course name: ")
	course.Name, _ = reader.ReadString('\n')
	fmt.Println("New course name:", course.Name)

	// Ask for the new url course
	fmt.Println("Ex. https://www.scienzeingegneria.univr.it/?ent=cs&id=954&tcs=MA")

	fmt.Print("Enter new course URL: ")
	course.CrawlingURL, _ = reader.ReadString('\n')
	fmt.Println("New course URL:", course.CrawlingURL)
	course.CrawlingURL = utils.NormalizeURL(course.CrawlingURL)

	// Ask for the new url course
	fmt.Println("Ex. https://www.scienzeingegneria.univr.it/?ent=avvisoin&cs=954")
	fmt.Print("Enter new course URL Notices: ")
	course.NoticesURL, _ = reader.ReadString('\n')
	fmt.Println("New course URL Notices:", course.NoticesURL)
	course.NoticesURL = utils.NormalizeURL(course.NoticesURL)

	table.SetHeader([]string{"ID", "Name", "Crawling URL", "Notices URL"})
	table.Append([]string{strconv.FormatInt(course.ID, 10), course.Name, course.CrawlingURL, course.NoticesURL})
	fmt.Print("Write yes if everthing is right, otherwise abort the process: ")
	response, _ := reader.ReadString('\n')
	if strings.Compare(response, "yes\n") != 0 {
		log.Fatalln("Adios!")
	}

		for _, dep := range deps {
		if dep.ID == depID {
			err = dep.Load(ds)
			if err != nil {
				log.Fatalln("Something went very wrong :", err)
			}
			depCourses, err := dep.GetCourses(ds)
			if err != nil {
				log.Fatalln("GetCourses:", err)
			}

			for _, c := range depCourses {
				fmt.Println("Parsing", c.Name)
				if c.ID == course.ID {
					log.Fatalln("A course with the same ID already exists")
				}
				if strings.Compare(c.Name, course.Name) == 0{
					log.Fatalln("A course with the same Name already exists")
				}
				if strings.Compare(c.CrawlingURL, course.CrawlingURL) == 0{
					log.Fatalln("A course with the same CrawlingURL already exists")
				}
				if strings.Compare(c.NoticesURL, course.NoticesURL) == 0{
					log.Fatalln("A course with the same NoticesURL already exists")
				}
			}
			fmt.Println("Adding ", course.Name, "to", dep.Name)
			err = dep.AddCourse(&course)
			if err != nil {
				log.Fatalln("Something went wrong :", err)
			}
			err = dep.Save(ds)
			if err != nil {
				log.Fatalln("Something went very wrong :", err)
			}
			course.Save(ds.ClientDS)

			fmt.Println("", course.Name, "added to", dep.Name)
		}
	}



}
