package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	"cloud.google.com/go/datastore"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
)

func main() {
	var ds *googlecloud.DatastoreGC
	var err error
	ctx := context.Background()

	countChat := flag.Bool("count-chat", false, "Count the number of active users")
	deleteCourseMigration := flag.Bool("delete-course-migration", false, "Delete Course_migration schema")
	fixChatCourse := flag.Bool("fccf", false, "Starting from the followers courses in the Chat enity, it fixes the following path")

	courseID := flag.Int("get-followers", 0, "-get-followers <course-ID>")

	flag.Parse()

	// 	if flag.NFlag > 0 {
	ds, err = googlecloud.NewDS()
	if err != nil {
		log.Fatalln("googlecloud.NewDS :", err)
	}
	//	} else {
	//		flag.PrintDefaults()
	// return
	//	}

	if *countChat {
		fmt.Print("> Counting chats...")
		res, err := ds.GetAllChats()
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println(len(res), " active users")
	} else if *deleteCourseMigration {
		var cs []googlecloud.Course

		ds, err := googlecloud.NewDS()
		if err != nil {
			log.Fatalln("googlecloud.NewDS :", err)
		}
		q := datastore.NewQuery("Course_migration")
		_, err = ds.ClientDS.GetAll(ctx, q, &cs)
		if err != nil {
			log.Fatalln("ds.ClientDS.GetAll :", err)
		}
		for _, c := range cs {
			err = ds.ClientDS.Delete(ctx, c.Key)
			if err != nil {
				log.Fatalln("ds.ClientDS.Delete :", err)
			} else {
				log.Println("Course ", c.Name, "deleted")
			}
		}
	} else if *fixChatCourse {

	} else if courseID != nil {
		course := googlecloud.Course{
			ID: int64(*courseID),
		}
		tmp, err := course.GetFollowers(ds)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println(tmp)
	}
}
