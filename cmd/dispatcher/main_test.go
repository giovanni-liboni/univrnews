package main

import (
	"fmt"
	"net/url"
	"os"
	"reflect"
	"strconv"
	"testing"

	newstojson "gitlab.com/giovanni-liboni/univrnews/pkg/news"
)

func TestMessageTooLong(t *testing.T) {

	link, err := url.Parse("http://www.medicina.univr.it/?ent=avviso&dest=&id=140548")
	if err != nil {
		t.Error(err)
	}
	n1, err := newstojson.ParseFromLink(link)
	if err != nil {
		t.Error(err)
	}
	err = n1.CompleteParse()
	if err != nil {
		t.Error(err)
	}

	// Create the new dispatcher
	d, err := NewDispatcher()
	if err != nil {
		t.Fatal(err)
	}

	n1.Content = n1.Content + "asfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkaasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkasfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlkassfkljaslkgjlaskjflksjflksdjflkjsdflkajsl;fkjasdlk"
	resNews := d.toTelegramTemplate(n1)
	fmt.Println(len(resNews))

	id, _ := strconv.Atoi(os.Getenv("TELEGRAM_USER_ID_TEST"))
	d.sendToFollowers(n1, []int64{int64(id)})
}

func Test_checkHost(t *testing.T) {
	type args struct {
		hostname string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := checkHost(tt.args.hostname); got != tt.want {
				t.Errorf("checkHost() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getFollowers(t *testing.T) {
	type args struct {
		n *newstojson.News
	}
	tests := []struct {
		name    string
		args    args
		want    []int64
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	d, err := NewDispatcher()
	if err != nil {
		t.Fatal(err)
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := d.getFollowers(tt.args.n)
			if (err != nil) != tt.wantErr {
				t.Errorf("getFollowers() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getFollowers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sendToFollowers(t *testing.T) {
	type args struct {
		n         *newstojson.News
		followers []int64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.

	}
	d, err := NewDispatcher()
	if err != nil {
		t.Fatal(err)
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := d.sendToFollowers(tt.args.n, tt.args.followers); (err != nil) != tt.wantErr {
				t.Errorf("sendToFollowers() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestSendToFollowers(t *testing.T) {
	var tests = []struct {
		s        string // input
		expected bool   // expected result
	}{
		// {"http://www.di.univr.it/?ent=avviso&dest=165&id=118877", true},
		// {"http://www.dsg.univr.it/?ent=avviso&dest=&id=118478", true},
		// {"http://www.dsg.univr.it/?ent=avviso&dest=&id=125827", true},
		// {"http://www.dsg.univr.it/?ent=avviso&dest=&id=40564", true},
		{"http://www.di.univr.it/?dest=&ent=avviso&id=130133", true},
	}

	for _, tt := range tests {
		link, err := url.Parse(tt.s)
		if err != nil {
			t.Error(err)
		}
		n1, err := newstojson.ParseFromLink(link)
		if err != nil {
			t.Error(err)
		}
		err = n1.CompleteParse()
		if err != nil {
			t.Error(err)
		}

		// Create the new dispatcher
		d, err := NewDispatcher()
		if err != nil {
			t.Fatal(err)
		}
		userID, err := strconv.Atoi(os.Getenv("TELEGRAM_USER_ID_TEST"))
		if err != nil {
			t.Fatal(err)
		}

		err = d.sendToFollowers(n1, []int64{int64(userID)})
		if err != nil {
			t.Fatal(err)
		}

		err = d.sendToFollowers(n1, []int64{int64(userID)})
		if err != nil {
			t.Fatal(err)
		}

	}
}

// func Test_newsUpdateJob(t *testing.T) {
// 	type args struct {
// 		j *que.Job
// 	}
// 	tests := []struct {
// 		name    string
// 		args    args
// 		wantErr bool
// 	}{
// 		// TODO: Add test cases.
// 	}
// 	d, err := NewDispatcher()
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if err := d.newsUpdateJob(tt.args.j); (err != nil) != tt.wantErr {
// 				t.Errorf("newsUpdateJob() error = %v, wantErr %v", err, tt.wantErr)
// 			}
// 		})
// 	}
// }

func Test_main(t *testing.T) {
	tests := []struct {
		name string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			main()
		})
	}
}
