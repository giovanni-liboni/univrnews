package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html"
	"html/template"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	graylog "github.com/gemnasium/logrus-graylog-hook/v3"
	"gitlab.com/giovanni-liboni/univrnews/pkg/utils"

	"cloud.google.com/go/pubsub"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/giovanni-liboni/univrnews/pkg/crawlers"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
	newstojson "gitlab.com/giovanni-liboni/univrnews/pkg/news"
	"gitlab.com/giovanni-liboni/univrnews/pkg/ratelimiter"
	tb "gopkg.in/tucnak/telebot.v2"
)

var log = logrus.New()

type Dispatcher struct {
	limiter *ratelimiter.Limiter
	ds      *googlecloud.DatastoreGC
	ps      *googlecloud.PubSubGC
	b       *tb.Bot
	Users   map[int64]*tb.Chat
}

func main() {
	fmt.Println("==> Initializing Dispatcher")
	var mu sync.Mutex
	var news *newstojson.News

	// ---------------------------------------------------------------------------
	// Environment checks
	// ---------------------------------------------------------------------------
	envs := []string{
		"TELEBOT_SECRET",
		"SUBSCRIPTION_NAME",
		"GOOGLE_PROJECT_ID",
		"GRAYLOG_SERVER",
	}

	for _, s := range envs {
		if os.Getenv(s) == "" {
			log.Fatalln("Please setup", s, " env first")
		}
	}

	// ---------------------------------------------------------------------------
	// Logger setup
	// ---------------------------------------------------------------------------
	//hook, err := sdhook.New(
	//	sdhook.GoogleServiceAccountCredentialsJSON([]byte(os.Getenv("GOOGLE_KEY"))),
	//)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//log.Hooks.Add(hook)
	//log.Level = logrus.InfoLevel

	hook := graylog.NewAsyncGraylogHook(os.Getenv("GRAYLOG_SERVER"), map[string]interface{}{"cmd": "dispatcher"})
	defer hook.Flush()
	log.AddHook(hook)
	log.Level = logrus.DebugLevel

	// ---------------------------------------------------------------------------
	// Create new Dispatcher
	// ---------------------------------------------------------------------------
	d, err := NewDispatcher()
	if err != nil {
		log.WithError(err).Errorln("Error creating the dispatcher")
	}
	log.Println("==> Starting to receive messages")

	// Start messagge handler
	sub := d.ps.ClientPS.Subscription(os.Getenv("SUBSCRIPTION_NAME"))
	cctx, cancel := context.WithCancel(context.Background())

	// ---------------------------------------------------------------------------
	// Gracefull shutdown
	// ---------------------------------------------------------------------------
	var gracefulStop = make(chan os.Signal, 1)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	go func() {
		sig := <-gracefulStop
		log.Infoln("Graceful Stop started")
		log.Printf("caught sig: %+v", sig)
		cancel()
		err = d.ds.ClientDS.Close()
		if err != nil {
			log.WithError(err).Errorln("Error during closing ClientDS")
		}
		err = d.ps.ClientPS.Close()
		if err != nil {
			log.WithError(err).Errorln("Error during closing ClientPS")
		}
		os.Exit(0)
	}()

	err = sub.Receive(cctx, func(ctx context.Context, msg *pubsub.Message) {
		mu.Lock()
		defer mu.Unlock()
		defer msg.Ack()

		// -------------------------------------------------------------------------
		// Retrive News from Job
		// -------------------------------------------------------------------------
		news, err = d.getNewsFromMsg(msg.Data)
		// Bisogna omogeneizzare le news qui, nel mezzo della transizione ci sono news con solo il dipartimento altre con
		// anche i corsi segnati
		if err != nil {
			log.WithError(err).Errorln("Error deconding the message")
			// Something goes wrong to decode the message, the best solution right now
			// is to delete the messagge to avoid a chain of errors
			msg.Ack()
			cancel()
		} else {

			log.WithField("post-id", news.ID).Infoln("Received post ID", news.ID, "with", len(news.Courses), "courses defined")
			if len(news.Courses) > 0 {
				for i := range news.Courses {
					post := news
					if news.Courses[i].ID == 0 {
						// Something is wrong
						log.WithError(err).WithField("post-id", news.ID).WithField("post", news.String()).Errorln("Course with ID 0 provided. Cannot send the post. Please check the attached post.")
						break
					}

					err = news.Courses[i].Load(d.ds)
					if err != nil {
						log.WithError(err).WithField("post-id", news.ID).WithField("post", post.String()).Errorln("Failed to load course from datastore")
					}
					// -----------------------------------------------------------------------
					// Check if the news is sendable
					// -----------------------------------------------------------------------
					if d.isSendableToCourse(post, &news.Courses[i]) {
						err = d.sendToCourseFollowers(post, &news.Courses[i])
						if err == nil {
							log.WithField("post-id", news.ID).Infoln("News sent!")
						} else {
							log.WithError(err).Errorln("Post propagation failed!")
						}
					} else {
						log.WithField("post-id", news.ID).Infoln("News with courses already sent or there was a problem")
						msg.Ack()
						time.Sleep(500 * time.Millisecond)
					}
				}
			} else {
				// Only the department is specified, there is not any reference to a course
				// -----------------------------------------------------------------------
				// Check if the news is sendable
				// -----------------------------------------------------------------------
				if d.isSendable(news) {
					// ---------------------------------------------------------------------
					// Complete the parsing to get the degrees
					// ---------------------------------------------------------------------
					err = d.completeParse(news)
					if err == nil {
						// -------------------------------------------------------------------
						// Get followers to send the news
						// -------------------------------------------------------------------
						followers, err := d.getFollowersv1(news)
						if err == nil {
							// -----------------------------------------------------------------
							// Send the news to the followers
							// -----------------------------------------------------------------
							err = d.sendToFollowers(news, followers)
							if err == nil {
								log.WithField("post-id", news.ID).Infoln("News sent!")
							}
						}
					}
				} else {
					log.WithField("post-id", news.ID).Infoln("News already sent!")
					msg.Ack()
				}
			}
		}
		//time.Sleep(0,5 * time.Second)
	})

	if err != nil {
		cancel()
		log.WithError(err).Errorln("Error receiving messages")
	}
}

func NewDispatcher() (*Dispatcher, error) {
	var err error
	// Rate limiter (Al momento (25/08/2016) Telegram fissa il rate limit per l'invio di messaggi a 30ps )
	d := Dispatcher{
		limiter: ratelimiter.NewLimiter(30),
		ds:      new(googlecloud.DatastoreGC),
		ps:      new(googlecloud.PubSubGC),
		b:       new(tb.Bot),
		Users:   make(map[int64]*tb.Chat),
	}
	d.ds, err = googlecloud.NewDS()
	if err != nil {
		logrus.WithError(err).Errorln("googlecloud.NewDS :", err)
		return nil, err
	}
	d.ps, err = googlecloud.NewPS()
	if err != nil {
		log.WithError(err).Errorln("googlecloud.NewPS :", err)
		return nil, err
	}

	d.b, err = tb.NewBot(tb.Settings{
		Token:    os.Getenv("TELEBOT_SECRET"),
		Reporter: func(e error) { log.Errorln(e) },
	})
	if err != nil {
		log.WithError(err).Errorln("tb.NewBot :", err)
		return nil, err
	}

	return &d, nil
}

func (d *Dispatcher) toTelegramTemplate(news *newstojson.News) string {
	// 	{{ range .Courses }}
	// <i>{{ . | StripAllTags }}</i> {{ end }}
	const mailTempl = `

	<b>{{ .Title | StripAllTags }}</b>{{ if not .ModTime.IsZero }}

	<b>AVVISO MODIFICATO</b>{{ end }}

	<i>Pubblicato da: {{ .Author | StripAllTags }}</i>

	{{ .Content | Noescape }}
	{{ with .Attachments }}
	Allegati:
	  {{ range . }}
	      <a href="{{ .Link }}"> {{ .Title }} </a>
	  {{ end }} {{ end }}
	<a href="{{ .Link }}">  Clicca qui per visualizzare l'avviso </a>
	`

	var doc bytes.Buffer

	t := template.New("telegram") // Create a template.

	funcMap := template.FuncMap{
		"Noescape":     Noescape,
		"StripAllTags": StripAllTags,
	}
	t.Funcs(funcMap)

	// Se contiene gia' i caratteri di escape allora eseguo il noescape sul contenuto
	if strings.Contains(news.Content, "&#") || strings.Contains(news.Content, "&lt;") {

	} else {
		// Eseguo l'escape sul contenuto
		news.Content = html.EscapeString(news.Content)
	}

	news.Content = strings.Replace(news.Content, "&amp;", "&", -1)

	t, err := t.Parse(mailTempl)
	if err != nil {
		log.Errorln(err)
	}

	err = t.Execute(&doc, news)
	if err != nil {
		log.Errorln(err)
	}

	return doc.String()
}

// checkHost return true if it's a valid host, false otherwise
func checkHost(hostname string) bool {
	return strings.HasSuffix(hostname, "univr.it")
}

// getNewsFromJob retrive a News from the Job struct
func (d *Dispatcher) getNewsFromMsg(msg []byte) (*newstojson.News, error) {
	n := new(newstojson.News)

	err := json.Unmarshal(msg, n)
	if err != nil {
		return nil, errors.Wrap(err, "Unable to unmarshal job arguments into News: "+string(msg))
	}

	// It is better to fix urls if necessary
	n.Link, err = url.Parse(utils.NormalizeURL(n.Link.String()))
	if err != nil {
		return nil, err
	}

	if !checkHost(n.Link.Hostname()) {
		log.WithField("post-id", n.ID).Errorln("Got this message ", n.String())
		return nil, errors.New("La stringa del link non contiene univr.it come host but got " + n.Link.Hostname())
	}

	log.WithField("url", n.Link.String()).Info("Processing news...")

	if n.PubTime.IsZero() || n.Title == "" || n.Content == "" {
		n, err = newstojson.ParseFromLink(n.Link)
		if err != nil {
			log.WithError(err).Errorln("Parse from link error : ", err)
			return nil, err
		}
	}

	return n, nil
}

// -----------------------------------------------------------------------
// completeParse
// -----------------------------------------------------------------------
func (d *Dispatcher) completeParse(n *newstojson.News) error {
	// Complete the crawling operation if it is incomplete
	// If the n.Course.ID is zero then a course is specified an it's already loaded
	if len(n.DegreeIds) == 0 {
		err := n.CompleteParse()
		if err != nil {
			return err
		}
		n.GetContentFromURL()
		err = crawlers.SetDegreeIDs(d.ds, n)
		if err != nil {
			log.WithField("post-id", n.ID).Errorln("crawlers.SetDegreeIDs: ", err)
			return err
		}
	}
	return nil
}

// -----------------------------------------------------------------------
// isSendable
// -----------------------------------------------------------------------
func (d *Dispatcher) isSendable(n *newstojson.News) bool {
	// Nuovo controllo basandosi su datastore
	nn := googlecloud.News{
		ID:           int64(n.ID),
		PubTime:      n.PubTime,
		ModTime:      n.ModTime,
		DepartmentID: n.Department.ID,
	}
	return nn.IsSendable(d.ds)
}

func (d *Dispatcher) isSendableToCourse(n *newstojson.News, course *googlecloud.Course) bool {
	nn := googlecloud.News{
		ID:           int64(n.ID),
		PubTime:      n.PubTime,
		ModTime:      n.ModTime,
		DepartmentID: n.Department.ID,
		CourseID:     course.ID,
	}
	return nn.IsSendable(d.ds)
}

// -----------------------------------------------------------------------
// getFollowersv1
// -----------------------------------------------------------------------
func (d *Dispatcher) getFollowersv1(n *newstojson.News) ([]int64, error) {
	var err error
	res := make([]int64, 0, 8000)

	// If there is no followers, it means that it is for the department
	if len(n.DegreeIds) == 0 && len(n.Courses) == 0 {
		if n.Department.ID != 0 {
			// Retrieve all the followers for the departement (includes the followers from each courses in the department)
			res, err = n.Department.GetFollowersWithCourses(d.ds)
			if err != nil {
				return res, err
			}
		} else {
			// TODO: se department id == 0
			// Errore : i crawlers impostano sempre il dipartimento
			log.WithField("post-id", n.ID).Warningln("Set the department for the news")
		}
	} else {
		// Retrieves only the followers for the courses
		for _, courseID := range n.DegreeIds {
			course := googlecloud.Course{
				ID: int64(courseID),
			}
			tmp, err := course.GetFollowers(d.ds)
			if err != nil {
				return nil, err
			}
			res = append(res, tmp...)
		}
	}
	// if len(res) == 0 {
	// 	log.Errorln("Message ", n.ID, "has no followers but should be sent. Check it!")
	// }
	return RemoveDuplicates(res), err
}

// -----------------------------------------------------------------------
// getFollowers
// -----------------------------------------------------------------------
func (d *Dispatcher) getFollowers(n *newstojson.News) ([]int64, error) {
	res := make([]int64, 0, 5000)
	var err error
	// Nuovo controllo basandosi su datastore
	nn := googlecloud.News{
		ID:           int64(n.ID),
		PubTime:      n.PubTime,
		ModTime:      n.ModTime,
		DepartmentID: n.Department.ID,
	}

	if nn.IsSendable(d.ds) {
		// Completo il parsing dell'avviso
		if len(n.DegreeIds) == 0 {
			err = n.CompleteParse()
			if err != nil {
				return nil, err
			}
			n.GetContentFromURL()
			err = crawlers.SetDegreeIDs(d.ds, n)
			if err != nil {
				log.WithField("post-id", n.ID).WithError(err).Errorln("crawlers.SetDegreeIDs: ", err)
			}
		}

		if len(n.DegreeIds) == 0 {
			// Get the department from its url
			dep := googlecloud.Department{
				URL: n.Link.Host,
			}
			// Retrieve all the followers for the departement (includes the followers from each courses in the department)
			res, err = dep.GetFollowersWithCourses(d.ds)
			if err != nil {
				return res, err
			}
		} else {
			// Retrieves only the followers for the courses
			for _, courseID := range n.DegreeIds {
				course := googlecloud.Course{
					ID: int64(courseID),
				}
				tmp, err := course.GetFollowers(d.ds)
				if err != nil {
					return nil, err
				}
				res = append(res, tmp...)
			}
		}
		if len(res) == 0 {
			log.WithField("post-id", n.ID).Errorln("Message ", n.ID, "has no followers but should be sent. Check it!")
		}
		return RemoveDuplicates(res), err
	}
	// log.Infoln("Message ", nn.ID, "has already sent")
	return res, nil
}

func (d *Dispatcher) sendToSingleFollower(n *newstojson.News, follower int64) {
	// TODO
}

// -----------------------------------------------------------------------
// sendToFollowers
//
// NOTE Rendere questo metodo parallelo: si possono inviare fino a 30 messaggi
// al secondo (questo non avviene ad oggi). Per farlo bisogna rendere thread-safe
// queste funzioni:
// - limiter
// - eliminazione utente
// - errori
// -----------------------------------------------------------------------
func (d *Dispatcher) sendToFollowers(n *newstojson.News, followers []int64) error {
	if len(followers) == 0 {
		log.WithField("post-id", n.ID).Infoln("Discard message", n.ID, "because it has no followers")
		return nil
	}
	log.WithField("post-id", n.ID).Info("Invio il messaggio a ", len(followers), " utenti.")

	tmpCount := 0
	var err error

	// Remove duplicates if any
	followers = RemoveDuplicates(followers)

	inlineKeys := [][]tb.InlineButton{}
	inlineBtn := tb.InlineButton{
		Unique: "homepage",
		Text:   "🔙  Vai al menù principale",
		Data:   "homepage",
	}
	inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtn})

	// ---------------------------------------------------------------------------
	// Send the message to the followers
	// ---------------------------------------------------------------------------
	testo := d.toTelegramTemplate(n)
	for _, follower := range followers {
		var chat *tb.Chat
		// -------------------------------------------------------------------------
		//
		// -------------------------------------------------------------------------

		if _, ok := d.Users[follower]; !ok {
			d.limiter.Wait()
			chat, err = d.b.ChatByID(strconv.Itoa(int(follower)))
		} else {
			chat = d.Users[follower]
		}

		if err != nil {
			log.WithField("post-id", n.ID).WithField("follower", follower).Errorln("ChatByID :", err)
		} else {
			d.Users[follower] = chat

			// -----------------------------------------------------------------------
			//
			// -----------------------------------------------------------------------

			d.limiter.Wait()
			_, err := d.b.Send(chat, testo, &tb.SendOptions{
				ReplyMarkup: &tb.ReplyMarkup{
					InlineKeyboard: inlineKeys,
				},
				ParseMode: tb.ModeHTML,
			})

			// -----------------------------------------------------------------------
			// Handle errors
			// -----------------------------------------------------------------------
			if err != nil {
				if strings.Contains(err.Error(), "bot was blocked by the user") || strings.Contains(err.Error(), "user is deactivated") {
					c := googlecloud.Chat{
						ID: follower,
					}
					err = c.Delete(d.ds)
					delete(d.Users, follower)
					if err != nil {
						log.Errorln("Chat.Delete:", err)
					}
				} else if strings.Contains(err.Error(), "message is too long") {
					// Send the message without the description and attachments
					n.Content = "Visita il sito per visualizzare l'avviso"
					testTMP := d.toTelegramTemplate(n)
					d.limiter.Wait()
					_, err = d.b.Send(chat, testTMP, &tb.SendOptions{
						ReplyMarkup: &tb.ReplyMarkup{
							InlineKeyboard: inlineKeys,
						},
						ParseMode: tb.ModeHTML,
					})
					if err != nil {
						log.WithField("post-id", n.ID).WithField("follower", follower).Errorln("Send message is too long:", err)
					} else {
						tmpCount++
					}
				} else if strings.Contains(err.Error(), "Too Many Requests") {
					// Retry after 5 seconds (best effort)
					time.Sleep(5 * time.Second)
					_, err = d.b.Send(chat, testo, &tb.SendOptions{
						ReplyMarkup: &tb.ReplyMarkup{
							InlineKeyboard: inlineKeys,
						},
						ParseMode: tb.ModeHTML,
					})
					if err != nil {
						log.WithField("post-id", n.ID).WithField("follower", follower).Errorln("Error while retrying to send the message after Too Many Requests error from Telegram", err)
					}

				} else {
					log.WithField("post-id", n.ID).WithField("follower", follower).Errorln("Send :", err)
				}
			} else {
				tmpCount++
			}
		} // End else
	} // End loop followers

	var nn googlecloud.News
	if len(n.Courses) == 1 {
		nn = googlecloud.News{
			ID:           int64(n.ID),
			PubTime:      n.PubTime,
			ModTime:      n.ModTime,
			DepartmentID: n.Department.ID,
			CourseID:     n.Courses[0].ID,
		}
	} else {
		// Nuovo controllo basandosi su datastore
		nn = googlecloud.News{
			ID:           int64(n.ID),
			PubTime:      n.PubTime,
			ModTime:      n.ModTime,
			DepartmentID: n.Department.ID,
		}
	}

	err = nn.Sent(d.ds)
	if err != nil {
		log.WithError(err).WithField("post-id", n.ID).Errorln("Tag as sent the news")
	}

	log.WithField("post-id", n.ID).Info("Inviato correttamente a ", tmpCount, " utenti.")

	return nil
}

// -----------------------------------------------------------------------
// sendToFollowers
//
// NOTE Rendere questo metodo parallelo: si possono inviare fino a 30 messaggi
// al secondo (questo non avviene ad oggi). Per farlo bisogna rendere thread-safe
// queste funzioni:
// - limiter
// - eliminazione utente
// - errori
// -----------------------------------------------------------------------
func (d *Dispatcher) sendToCourseFollowers(n *newstojson.News, course *googlecloud.Course) error {
	tmpCount := 0
	followers, err := course.GetFollowers(d.ds)
	if err != nil {
		return err
	}

	if len(followers) == 0 {
		log.WithField("post-id", n.ID).Infoln("Discard message", n.ID, "because it has no followers")
		return nil
	}
	log.WithField("post-id", n.ID).Info("Invio il messaggio a ", len(followers), " utenti.")

	// Remove duplicates if any
	followers = RemoveDuplicates(followers)

	inlineKeys := [][]tb.InlineButton{}
	inlineBtn := tb.InlineButton{
		Unique: "homepage",
		Text:   "🔙  Vai al menù principale",
		Data:   "homepage",
	}
	inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtn})

	// ---------------------------------------------------------------------------
	// Send the message to the followers
	// ---------------------------------------------------------------------------
	testo := d.toTelegramTemplate(n)
	for _, follower := range followers {
		var chat *tb.Chat
		// -------------------------------------------------------------------------
		//
		// -------------------------------------------------------------------------

		if _, ok := d.Users[follower]; !ok {
			d.limiter.Wait()
			chat, err = d.b.ChatByID(strconv.Itoa(int(follower)))
		} else {
			chat = d.Users[follower]
		}

		if err != nil {
			log.WithField("post-id", n.ID).WithField("follower", follower).Errorln("ChatByID :", err)
		} else {
			d.Users[follower] = chat

			// -----------------------------------------------------------------------
			//
			// -----------------------------------------------------------------------

			d.limiter.Wait()
			_, err := d.b.Send(chat, testo, &tb.SendOptions{
				ReplyMarkup: &tb.ReplyMarkup{
					InlineKeyboard: inlineKeys,
				},
				ParseMode: tb.ModeHTML,
			})

			// -----------------------------------------------------------------------
			// Handle errors
			// -----------------------------------------------------------------------
			if err != nil {
				if strings.Contains(err.Error(), "bot was blocked by the user") || strings.Contains(err.Error(), "user is deactivated") {
					c := googlecloud.Chat{
						ID: follower,
					}
					err = c.Delete(d.ds)
					delete(d.Users, follower)
					if err != nil {
						log.Errorln("Chat.Delete:", err)
					}
				} else if strings.Contains(err.Error(), "message is too long") {
					// Send the message without the description and attachments
					n.Content = "Visita il sito per visualizzare l'avviso"
					testTMP := d.toTelegramTemplate(n)
					d.limiter.Wait()
					_, err = d.b.Send(chat, testTMP, &tb.SendOptions{
						ReplyMarkup: &tb.ReplyMarkup{
							InlineKeyboard: inlineKeys,
						},
						ParseMode: tb.ModeHTML,
					})
					if err != nil {
						log.WithField("post-id", n.ID).WithField("follower", follower).Errorln("Send message is too long:", err)
					} else {
						tmpCount++
					}
				} else {
					log.WithField("post-id", n.ID).WithField("follower", follower).Errorln("Send :", err)
				}
			} else {
				tmpCount++
			}
		} // End else
	} // End loop followers

	nn := googlecloud.News{
		ID:           int64(n.ID),
		PubTime:      n.PubTime,
		ModTime:      n.ModTime,
		DepartmentID: n.Department.ID,
		CourseID:     course.ID,
	}

	err = nn.Sent(d.ds)
	if err != nil {
		log.WithError(err).WithField("post-id", n.ID).Errorln("Tag as sent the news")
	}
	log.WithField("post-id", n.ID).Info("Inviato correttamente a ", tmpCount, " utenti.")

	return nil
}
