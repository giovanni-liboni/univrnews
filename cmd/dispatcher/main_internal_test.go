package main

import (
	"fmt"
	"net/url"
	"os"
	"testing"
	"time"

	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
	newstojson "gitlab.com/giovanni-liboni/univrnews/pkg/news"
	tb "gopkg.in/tucnak/telebot.v2"
)

// func TestGetNewsFromJob(t *testing.T) {
// 	var j que.Job
// 	var n queue.News
//
// 	// Create the new dispatcher
// 	d, err := NewDispatcher()
// 	if err != nil {
// 		t.Fatal(err)
// 	}
//
// 	_, err = d.getNewsFromJob(&j)
//
// 	if err == nil && err.Error() != "Unable to unmarshal job arguments into News: "+string(j.Args) {
// 		t.Error("Expected error")
// 	}
// 	n.Link, err = url.Parse("http://www.dsg.unipd.it/?ent=avviso&dest=165&id=117546")
// 	if err != nil {
// 		t.Error(err)
// 	}
//
// 	_, err = d.getNewsFromJob(&j)
// 	if err == nil && err.Error() != "La stringa del link non contiene univr.it come host" {
// 		t.Error("Expected error on host ")
// 	}
//
// 	// Passo un job corretto
// 	n.Link, err = url.Parse("http://www.dsg.univr.it/?ent=avviso&dest=165&id=117546")
// 	if err != nil {
// 		t.Error(err)
// 	}
// 	n.Title = "PER GLI STUDENTI DELL'AREA INFORMATICA"
//
// 	enc, err := json.Marshal(n)
// 	if err != nil {
// 		t.Error(err)
// 	}
//
// 	j = que.Job{
// 		Type: queue.NewsUpdateJob,
// 		Args: enc,
// 	}
// 	res, err := d.getNewsFromJob(&j)
// 	if err != nil {
// 		t.Error(err)
// 	}
//
// 	if res.Title != "PER GLI STUDENTI DELL'AREA INFORMATICA: 1° bando 2016 del Programma di tirocinio MIUR Fondazione CRUI  – Università Italiane." {
// 		t.Error("Expected PER GLI STUDENTI DELL'AREA INFORMATICA: 1° bando 2016 del Programma di tirocinio MIUR Fondazione CRUI  – Università Italiane. as title, but got ", res.Title)
// 	}
// 	if res.Author != "U.O. Didattica - Area Scienze e Ingegneria" {
// 		t.Error("Expected U.O. Didattica - Area Scienze e Ingegneria as author, but got ", res.Author)
// 	}
// 	var defaultTime time.Time
// 	if defaultTime.Unix() == res.PubTime.Unix() {
// 		t.Error("Expected a pub time in the past")
// 	}
// }

func TestGetFollowersv1(t *testing.T) {
	d, err := NewDispatcher()
	if err != nil {
		t.Fatal(err)
	}
	// link, err := url.Parse("http://www.dea.univr.it/?ent=avviso&dest=&id=140709")
	// if err != nil {
	// 	t.Error(err)
	// }
	n := &newstojson.News{
		ID:        140470,
		DegreeIds: []int{},
	}
	if err != nil {
		log.Errorln("PollingOnAvvisi.SetDegreeIDs :", err)
	}

	dep := googlecloud.Department{
		Name:        "Economica",
		ID:          36,
		RSSLink:     "http://www.dea.univr.it/?ent=avviso&dest=179&rss=0",
		URL:         "www.dea.univr.it",
		NoticesURL:  "http://www.dea.univr.it/?ent=avviso",
		CrawlingURL: "",
		Courses:     nil,
	}
	n.Department = dep
	_, err = d.getFollowersv1(n)
	// News sent
	if err != nil {
		t.Error(err)
	}
}

func TestIsSendable(t *testing.T) {
	link, err := url.Parse("http://www.di.univr.it/?ent=avviso&dest=&id=140470")
	if err != nil {
		t.Error(err)
	}
	dep := googlecloud.Department{
		URL: "www.scienzeingegneria.univr.it",
	}

	n := &newstojson.News{
		ID:      140470,
		PubTime: time.Now(),
		ModTime: time.Now(),
		Link:    link,
	}

	nn := googlecloud.News{
		ID:           int64(n.ID),
		PubTime:      n.PubTime,
		ModTime:      n.ModTime,
		DepartmentID: 1,
	}

	// Create the new dispatcher
	d, err := NewDispatcher()
	if err != nil {
		t.Fatal(err)
	}

	// Load the department first
	err = dep.Load(d.ds)
	if err != nil {
		t.Fatal(err)
	}
	n.Department = dep

	// Remove it from datastore
	nn.Remove(d.ds)

	res := d.isSendable(n)
	if !res {
		t.Error("Expected true but got false")
	}
	nn.Sent(d.ds)

	res = d.isSendable(n)
	if res {
		t.Error("Expected false but got true")
	}
}

func TestGetFollowers(t *testing.T) {
	link, err := url.Parse("http://www.di.univr.it/?ent=avviso&dest=&id=140470")
	if err != nil {
		t.Error(err)
	}
	dep := googlecloud.Department{
		URL: "www.scienzeingegneria.univr.it",
	}

	n := &newstojson.News{
		ID:      140470,
		PubTime: time.Now(),
		ModTime: time.Now(),
		Link:    link,
	}

	nn := googlecloud.News{
		ID:           int64(n.ID),
		PubTime:      n.PubTime,
		ModTime:      n.ModTime,
		DepartmentID: 1,
	}

	// Create the new dispatcher
	d, err := NewDispatcher()
	if err != nil {
		t.Fatal(err)
	}

	// Load the department first
	err = dep.Load(d.ds)
	if err != nil {
		t.Fatal(err)
	}
	n.Department = dep

	// Remove it from datastore
	nn.Remove(d.ds)

	res, err := d.getFollowersv1(n)
	// News sent
	if err != nil {
		t.Error(err)
	}
	if len(res) == 0 {
		t.Error("Expected len(res) to be greater than 0")
	}
	// // Set it as sent
	// err = nn.Sent(d.ds)
	// if err != nil {
	// 	t.Error(err)
	// }
	//
	// // Retry with the same news, it should retrieve 0 followers ( news already sent )
	// res, err = d.getFollowersv1(n)
	// // News sent
	// if err != nil {
	// 	t.Error(err)
	// }
	// if len(res) > 0 {
	// 	t.Error("Expected len(res) == 0 but got", len(res))
	// }
	//
	// // Simulate a modified message
	// n.ModTime = time.Now().Add(time.Hour*1 + time.Minute*15 + time.Second*10)
	// res, err = d.getFollowersv1(n)
	// // News sent
	// if err != nil {
	// 	t.Error(err)
	// }
	// if len(res) == 0 {
	// 	t.Error("Expected len(res) to be greater than 0")
	// }
	nn.Remove(d.ds)
}

func TestCheckHost(t *testing.T) {
	var tests = []struct {
		s        string // input
		expected bool   // expected result
	}{
		{"di.univr.it", true},
		{"di.univr.com", false},
		{"di.univr.it:3333", false},
		{"univr.it", true},
		{"univr.it.example.it", false},
		{"www.dsg.univr.it", true},
	}
	for _, tt := range tests {
		actual := checkHost(tt.s)
		if actual != tt.expected {
			t.Errorf("checkHost(%s): expected %t, actual %t", tt.s, tt.expected, actual)
		}
	}
}

func TestToTelegramTemplate(t *testing.T) {
	var tests = []struct {
		s        string // input
		expected bool   // expected result
	}{
		// {"http://www.di.univr.it/?ent=avviso&dest=165&id=118877", true},
		// {"http://www.dsg.univr.it/?ent=avviso&dest=&id=118478", true},
		// {"http://www.dsg.univr.it/?ent=avviso&dest=&id=125827", true},
		// {"http://www.dsg.univr.it/?ent=avviso&dest=&id=40564", true},
		{"http://www.di.univr.it/?dest=&ent=avviso&id=130133", true},
	}

	for _, tt := range tests {
		link, err := url.Parse(tt.s)
		if err != nil {
			t.Error(err)
		}
		n1, err := newstojson.ParseFromLink(link)
		if err != nil {
			t.Error(err)
		}
		err = n1.CompleteParse()
		if err != nil {
			t.Error(err)
		}

		// Create the new dispatcher
		d, err := NewDispatcher()
		if err != nil {
			t.Fatal(err)
		}
		resNews := d.toTelegramTemplate(n1)
		fmt.Println(resNews)

		if os.Getenv("TELEBOT_SECRET_TEST") != "" {
			b, err := tb.NewBot(tb.Settings{
				Token:    os.Getenv("TELEBOT_SECRET_TEST"),
				Reporter: func(e error) { log.Errorln(e) },
			})

			resNews := d.toTelegramTemplate(n1)
			// mode := tgbot.Html
			// userTestID, _ := strconv.Atoi(os.Getenv("TELEGRAM_USER_ID_TEST"))

			inlineKeys := [][]tb.InlineButton{}
			inlineBtn := tb.InlineButton{
				Unique: "homepage",
				Text:   "🔙  Vai al menù principale",
				Data:   "homepage",
			}
			inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtn})

			// Invio il messaggio
			d.limiter.Wait()
			chat, err := b.ChatByID(os.Getenv("TELEGRAM_USER_ID_TEST"))
			if err != nil {
				log.Errorln("ChatByID :", err)
			} else {
				_, err := b.Send(chat, resNews, &tb.SendOptions{
					ReplyMarkup: &tb.ReplyMarkup{
						InlineKeyboard: inlineKeys,
					},
					ParseMode: tb.ModeHTML,
				})
				if err != nil {
					log.Errorln("Send :", err)
				}
			}
		}
	}
}
