package main

import (
	"bytes"
	"github.com/PuerkitoBio/goquery"
	"html/template"
)

func RemoveDuplicates(elements []int64) []int64 {
	// Use map to record duplicates as we find them.
	encountered := map[int64]bool{}
	result := []int64{}

	for v := range elements {
		if encountered[elements[v]] {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[elements[v]] = true
			// Append to result slice.
			result = append(result, elements[v])
		}
	}
	// Return the new slice.
	return result
}

func Noescape(s string) template.HTML {
	return template.HTML(s)
}

func StripAllTags(s string) template.HTML {
	b := bytes.NewBufferString(s)
	d, _ := goquery.NewDocumentFromReader(b)
	return template.HTML(d.Text())
}
