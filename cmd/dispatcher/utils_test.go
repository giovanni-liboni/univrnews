package main

import (
	"testing"
)

func TestRemoveDuplicates(t *testing.T) {
	a := []int64{1, 2, 3, 4, 4, 5, 6, 4, 6}
	res := []int64{1, 2, 3, 4, 5, 6}
	toTest := RemoveDuplicates(a)

	if len(toTest) != 6 {
		t.Error("Expected 6 elements into slice, got", len(toTest))
	}
	for i := range toTest {
		if toTest[i] != res[i] {
			t.Error("Expected slice without duplicates")
		}
	}
}

// func Test_noescape(t *testing.T) {
// 	type args struct {
// 		s string
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 		want template.HTML
// 	}{
// 	// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := Noescape(tt.args.s); !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("noescape() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }
//
// func Test_stripAllTags(t *testing.T) {
// 	type args struct {
// 		s string
// 	}
// 	tests := []struct {
// 		name string
// 		args args
// 		want template.HTML
// 	}{
// 	// TODO: Add test cases.
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := StripAllTags(tt.args.s); !reflect.DeepEqual(got, tt.want) {
// 				t.Errorf("stripAllTags() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }
