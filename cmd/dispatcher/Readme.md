# Environment Variables for Testing

- `TELEGRAM_USER_ID_TEST` : Define the Telegram ID for the user used to test the application
- `TELEBOT_SECRET_TEST` : Define a valid TELEGRAM_KEY to send the test messages  
- `REDIS_URL` : Url di connessione a Redis. If the tool is deployed on Heroku
add the Redis module.

# Dipendenze

The `gitlab.com/giovanni-liboni/univrnews/pkg/news` package required to set
the variable `REDIS_URL`.

# Env Variable

 - `REDIS_URL` : Url di connessione a Redis. If the tool is deployed on Heroku
 add the Redis module.

# Deploy on Heroku
- `heroku addons:create heroku-postgresql:hobby-dev`
- `heroku buildpacks:clear`
- `heroku buildpacks:set https://github.com/giovanni-liboni/heroku-buildpack-go-multi-binaries`
- `heroku buildpacks:add heroku/go`
- `heroku config:set APP_PATH=cmd/dispatcher`
