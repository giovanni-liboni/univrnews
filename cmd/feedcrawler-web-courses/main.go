package main

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	"gitlab.com/giovanni-liboni/univrnews/pkg/news"

	graylog "github.com/gemnasium/logrus-graylog-hook/v3"
	"github.com/olekukonko/tablewriter"
	"github.com/sirupsen/logrus"
	"gitlab.com/giovanni-liboni/univrnews/pkg/crawlers"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"

	"github.com/panjf2000/ants/v2"
)

var activateDate time.Time
var log = logrus.New()

type FeedcrawlerWeb struct {
	activateDate time.Time
	wg           sync.WaitGroup
	depsURL      []string
	msg          chan string
	ds           *googlecloud.DatastoreGC
	ps           *googlecloud.PubSubGC
	deps         []googlecloud.Department
}

//var oldPosts map[int64][]newstojson.News
var oldPosts sync.Map

func main() {
	defer ants.Release()
	var err error
	// ---------------------------------------------------------------------------
	// Environment checks
	// ---------------------------------------------------------------------------
	envs := []string{
		"TOPIC_NAME", // The name of the queue on Google Pub/Sub
		"GOOGLE_PROJECT_ID",
		"GRAYLOG_SERVER",
	}

	for _, s := range envs {
		if os.Getenv(s) == "" {
			log.Fatalln("Please setup", s, " env first")
		}
	}
	// ---------------------------------------------------------------------------
	// Logger setup
	// ---------------------------------------------------------------------------
	//hook, err := sdhook.New(
	//	sdhook.GoogleServiceAccountCredentialsJSON([]byte(os.Getenv("GOOGLE_KEY"))),
	//)
	//if err != nil {
	//	fmt.Println(err)
	//}

	hook := graylog.NewAsyncGraylogHook(os.Getenv("GRAYLOG_SERVER"), map[string]interface{}{"cmd": "feedcrawler-web-courses"})
	defer hook.Flush()
	log.AddHook(hook)
	log.Level = logrus.DebugLevel

	// ---------------------------------------------------------------------------
	// Start Feed Crawler Web Courses
	// ---------------------------------------------------------------------------
	fmt.Println("==> Starting Feed Crawler Web")
	now := time.Now()

	fcw := FeedcrawlerWeb{
		activateDate: now.Add(-1 * time.Hour),
		ds:           new(googlecloud.DatastoreGC),
		ps:           new(googlecloud.PubSubGC),
	}
	activateDate = fcw.activateDate

	// Initialize Datastore client
	fcw.ds, err = googlecloud.NewDS()
	if err != nil {
		log.WithError(err).Fatalln("googlecloud.NewDS initialization")
	}

	// Initialize PubSub Client
	fcw.ps, err = googlecloud.NewPS()
	if err != nil {
		log.WithError(err).Errorln("googlecloud.NewPS initialization")
	}
	log.Println("==> Starting Feed Crawler Web Version")
	log.Println("==> Retriving departments")
	fcw.deps, err = fcw.ds.GetAllDepartments()
	if err != nil {
		log.WithError(err).Fatalln("Datastore GetDepartments get")
	}

	// ---------------------------------------------------------------------------
	// Retrieve the list of courses
	// ---------------------------------------------------------------------------
	var univrCourses []*googlecloud.Course
	for _, val := range fcw.deps {
		courses, err := val.GetCourses(fcw.ds)
		if err != nil {
			log.WithError(err).Errorln("GetCourses")
		}
		univrCourses = append(univrCourses, courses...)
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"#", "ID", "Name", "NoticesURL"})
	for i, c := range univrCourses {
		// fmt.Println("(" + strconv.FormatInt(dep.ID, 10) + ")\t" + dep.Name + "\t\t" + dep.URL)
		table.Append([]string{strconv.Itoa(i), strconv.FormatInt(c.ID, 10), c.Name, c.NoticesURL})
	}
	table.Render()
	table.ClearRows()
	table.ClearFooter()

	// ---------------------------------------------------------------------------
	// Gracefull shutdown
	// ---------------------------------------------------------------------------
	var gracefulStop = make(chan os.Signal, 1)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	go func() {
		sig := <-gracefulStop
		log.Infoln("Graceful Stop started")
		log.Printf("caught sig: %+v", sig)
		err = fcw.ds.ClientDS.Close()
		if err != nil {
			log.WithError(err).Errorln("During closing ClientDS")
		}
		err = fcw.ps.ClientPS.Close()
		if err != nil {
			log.WithError(err).Errorln("During closing ClientPS")
		}
		os.Exit(0)
	}()
	// ---------------------------------------------------------------------------
	// Pool Setup
	// The idea is to have a pool of 150 goroutines that act ah crawlers:
	// Each one crawls it's URL (associated with the old oldPosts retrieves)
	// ---------------------------------------------------------------------------

	// Use the pool with a function,
	p, err := ants.NewPoolWithFunc(150, func(c interface{}) {
		course := c.(*googlecloud.Course)
		fcw.RetrieveNewPostsFromCourseURL(*course.Department, *course)
	})
	if err != nil {
		log.WithError(err).Errorln("Error creating pool")
	}
	defer p.Release()

	for {
		for _, course := range univrCourses {
			// Invoke the goroutine with the associated function defined above.
			// It takes as parameter the course. Then, at each course is associated
			// a single goroutine that will take care for retriving the news
			err = p.Invoke(course)
			if err != nil {
				log.WithError(err).Warnln("Goroutine returns an error")
			}
			// Wait 5 seconds before launch another goroutine, it avoids to DOS the
			// website
			time.Sleep(time.Second * 5)
		}

		// After the courses are all parsed, let's wait few seconds before starting
		// it again. It should avoid to DOS the univr website in order to avoid to
		// be banned
		nBig, err := rand.Int(rand.Reader, big.NewInt(20))
		if err != nil {
			log.Errorln("Failed to generate random integer number:", err)
		}
		time.Sleep(time.Duration(nBig.Int64()+10) * time.Second)
	}
}

// Send takes a piece of news formatted as a JSON string and sends it to the
// corresponding queue defined in the TOPIC_NAME environment variable.
func (fcw *FeedcrawlerWeb) Send(data string) error {
	topic := os.Getenv("TOPIC_NAME")
	if err := fcw.ps.Publish(topic, data); err != nil {
		fmt.Println("Failed to publish :", err)
		return fmt.Errorf("Failed to publish on topic %s with data (%s)", topic, data)
	}
	return nil
}

// RetrieveNewPostsFromCourseURL retrieves the news of the course and sends the
// newest ones
func (fcw *FeedcrawlerWeb) RetrieveNewPostsFromCourseURL(dep googlecloud.Department, c googlecloud.Course) {

	var previuousPosts []news.News
	var actualPostsToParse []news.News

	// Retrieve the posts from the course page
	tmps, err := crawlers.RetrievesNews(&dep, &c, 10)
	if err != nil {
		log.WithError(err).Errorln("Error crawlers.GetAvvisiC")
	}

	// Fetch an item that doesn't exist yet.
	result, ok := oldPosts.Load(c.ID)
	if ok {
		previuousPosts = result.([]news.News)
	}
	actualPostsToParse = getNewAvvisi(tmps, previuousPosts)
	log.Infoln("Retrieved", len(previuousPosts), "posts (new", len(actualPostsToParse), ") for the course", c.Name)

	for _, tmp := range actualPostsToParse {
		tmp := tmp
		tmp.GetContentFromURL()
		if tmp.IsNew(activateDate) {
			// Retrieve all the information
			log.Infoln("Parsing new post @", tmp.Link.String())
			tmp.Department = dep
			tmp.DegreeIds = append(tmp.DegreeIds, int(c.ID))
			tmp.Courses = append(tmp.Courses, c)
			tmp.CreatedAt = time.Now()
			err := fcw.Send(tmp.String())
			if err != nil {
				log.WithField("post-id", tmp.ID).WithField("topic", os.Getenv("TOPIC_NAME")).WithField("data", tmp.String()).Errorln("Failed to send the post on the queue")
			} else {
				log.WithField("post-id", tmp.ID).WithField("topic", os.Getenv("TOPIC_NAME")).Infoln("Sent on the queue")
			}
		}
	}
	oldPosts.Store(c.ID, tmps)
}

func getNewAvvisi(new, old []news.News) []news.News {
	res := make([]news.News, 0, 30)
	// Gli avvisi sono ordinati (almeno si spera)
	if len(old) > 0 {
		// Range on new news
		for _, n := range new {
			found := false
			for _, o := range old {
				o := o
				if n.EqualsTo(&o) {
					// Found
					found = true
				}
			}
			if !found {
				res = append(res, n)
			}
		}
		return res
	}
	return new
}
