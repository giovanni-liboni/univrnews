package main

import (
	"testing"
	"time"

	"gitlab.com/giovanni-liboni/univrnews/pkg/news"
)

func TestGetNewAvvisi(t *testing.T) {
	newsNew := []news.News{
		news.News{
			ID:      1,
			PubTime: time.Now(),
			ModTime: time.Now(),
		},
		news.News{
			ID:      2,
			PubTime: time.Now(),
			ModTime: time.Now().Add(1 * time.Hour),
		},
		news.News{
			ID:      3,
			PubTime: time.Now(),
			ModTime: time.Now().Add(3 * time.Second),
		},
	}

	res := getNewAvvisi(newsNew, newsNew)
	if len(res) > 0 {
		t.Error("Expected len 0 but got", len(res))
	}

	newsOld := make([]news.News, len(newsNew))
	copy(newsOld, newsNew)
	newsNew[0].ModTime = newsNew[0].ModTime.Add(1 * time.Hour)
	res = getNewAvvisi(newsNew, newsOld)
	if len(res) != 1 {
		t.Error("Expected len 1 but got", len(res))
	}
}
