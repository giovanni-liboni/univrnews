package main

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"

	graylog "github.com/gemnasium/logrus-graylog-hook/v3"
	"github.com/olekukonko/tablewriter"
	"github.com/sirupsen/logrus"
	"gitlab.com/giovanni-liboni/univrnews/pkg/crawlers"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"

	newstojson "gitlab.com/giovanni-liboni/univrnews/pkg/news"
)

var activateDate time.Time
var log = logrus.New()

type FeedcrawlerWeb struct {
	activateDate time.Time
	wg           sync.WaitGroup
	msg          chan string
	ds           *googlecloud.DatastoreGC
	ps           *googlecloud.PubSubGC
	deps         []googlecloud.Department
}

func main() {
	var err error
	// ---------------------------------------------------------------------------
	// Environment checks
	// ---------------------------------------------------------------------------
	envs := []string{
		"TOPIC_NAME", // The name of the queue on Google Pub/Sub
		"GOOGLE_PROJECT_ID",
		"GRAYLOG_SERVER",
	}

	for _, s := range envs {
		if os.Getenv(s) == "" {
			log.Fatalln("Please setup", s, " env first")
		}
	}
	// ---------------------------------------------------------------------------
	// Logger setup
	// ---------------------------------------------------------------------------
	//hook, err := sdhook.New(
	//	sdhook.GoogleServiceAccountCredentialsJSON([]byte(os.Getenv("GOOGLE_KEY"))),
	//)
	//if err != nil {
	//	fmt.Println(err)
	//}

	hook := graylog.NewAsyncGraylogHook(os.Getenv("GRAYLOG_SERVER"), map[string]interface{}{"cmd": "feedcrawler-web-courses"})
	defer hook.Flush()
	log.AddHook(hook)
	log.Level = logrus.DebugLevel

	// ---------------------------------------------------------------------------
	// Start Feed Crawler Web Courses
	// ---------------------------------------------------------------------------
	fmt.Println("==> Starting Feed Crawler Web")
	now := time.Now()

	fcw := FeedcrawlerWeb{
		activateDate: now.Add(-1 * time.Hour),
		ds:           new(googlecloud.DatastoreGC),
		ps:           new(googlecloud.PubSubGC),
	}
	activateDate = fcw.activateDate

	// Initialize Datastore client
	fcw.ds, err = googlecloud.NewDS()
	if err != nil {
		log.WithError(err).Fatalln("googlecloud.NewDS initialization")
	}

	// Initialize PubSub Client
	fcw.ps, err = googlecloud.NewPS()
	if err != nil {
		log.WithError(err).Errorln("googlecloud.NewPS initialization")
	}
	log.Println("==> Starting Feed Crawler Web Version")
	log.Println("==> Retriving departments")
	fcw.deps, err = fcw.ds.GetAllDepartments()
	if err != nil {
		log.WithError(err).Fatalln("Datastore GetDepartments get")
	}

	// Add the sender goroutine
	fcw.wg.Add(1)
	fcw.msg = make(chan string)
	go fcw.Send()

	for _, val := range fcw.deps {
		courses, err := val.GetCourses(fcw.ds)
		if err != nil {
			log.WithError(err).Errorln("GetCourses")
		}
		for _, c := range courses {
			// log.Infoln("Creating goroutine for ", val)
			go fcw.PollingOnCourse(val, *c)
			fcw.wg.Add(1)
			time.Sleep(500 * time.Millisecond)
		}
	}

	// ---------------------------------------------------------------------------
	// Gracefull shutdown
	// ---------------------------------------------------------------------------
	var gracefulStop = make(chan os.Signal, 1)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	go func() {
		sig := <-gracefulStop
		log.Infoln("Graceful Stop started")
		log.Printf("caught sig: %+v", sig)
		err = fcw.ds.ClientDS.Close()
		if err != nil {
			log.WithError(err).Errorln("During closing ClientDS")
		}
		err = fcw.ps.ClientPS.Close()
		if err != nil {
			log.WithError(err).Errorln("During closing ClientPS")
		}
		os.Exit(0)
	}()

	// TODO: Migliorare gestione thread
	fcw.wg.Wait()
}

func (fcw *FeedcrawlerWeb) Send() {
	defer fcw.wg.Done()
	for {
		data := <-fcw.msg
		topic := os.Getenv("TOPIC_NAME")
		if err := fcw.ps.Publish(topic, data); err != nil {
			fmt.Println("Failed to publish :", err)
			log.WithField("topic", topic).WithField("data", string(data)).Warnln("Failed to publish :", err)
			// Retry
			fcw.msg <- data
		} else {
			log.WithField("topic", topic).Infoln("Sent to queue:", string(data))
		}
	}
}

func (fcw *FeedcrawlerWeb) PollingOnCourse(dep googlecloud.Department, c googlecloud.Course) {
	log.Infoln("Added on polling url:", c.NoticesURL)
	// Variabile degli avvisi
	var old []newstojson.News
	for {
		// Recupero gli avvisi
		tmp, err := crawlers.RetrievesNews(&dep, &c, 10)
		if err != nil {
			log.WithError(err).Errorln("Error crawlers.GetAvvisiC")
		}

		fmt.Println("Rendering table for course", c.Name)
		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Title", "PubTime", "ModTime"})
		for _, avviso := range tmp {
			avviso := avviso
			time.Sleep(1 * time.Second)
			avviso.GetContentFromURL()
			// fmt.Println("(" + strconv.FormatInt(dep.ID, 10) + ")\t" + dep.Name + "\t\t" + dep.URL)
			table.Append([]string{strconv.Itoa(avviso.ID), avviso.Title, avviso.PubTime.String(), avviso.ModTime.String()})
		}
		fmt.Println("Table for course", c.Name)
		table.Render()
		table.ClearRows()
		table.ClearFooter()
		fmt.Println("### Rendering table for course", c.Name, "DONE ###")

		toSend := getNewAvvisi(tmp, old)
		for _, tmp := range toSend {
			tmp := tmp
			if tmp.IsNew(activateDate) {
				// Retrieve all the information
				log.Infoln("Parsing new post @", tmp.Link.String())
				tmp.Department = dep
				//crawlers.GetSingleNewsv1(tmp.Link.String(), &tmp)
				//crawlers.GetSingleNews(tmp.Link.String(), &tmp)
				//// Wait few moments
				//time.Sleep(5 * time.Second)

				err = fcw.SetDegreeIDs(&dep, &tmp)
				if err != nil {
					log.WithError(err).Errorln("Error PollingOnAvvisi.SetDegreeIDs")
				}

				// If no degree is found then set the ID for this degree
				if len(tmp.DegreeIds) == 0 {
					tmp.DegreeIds = append(tmp.DegreeIds, int(c.ID))
					tmp.Courses = append(tmp.Courses, c)
					// But send an error (for the moment)
					log.Errorln("No degrees set for", tmp.ID, " on the course", c.Name)
				}
				tmp.CreatedAt = time.Now()
				// tmp.String()
				// enc, err := json.Marshal(tmp)
				// if err != nil {
				// 	log.Errorln(err)
				// }
				fcw.msg <- tmp.String()
				log.WithField("url", c.NoticesURL).Infoln("Sent to queue:", tmp.String())
			}
		}
		old = tmp
		nBig, err := rand.Int(rand.Reader, big.NewInt(30))
		if err != nil {
			log.WithField("url", c.NoticesURL).Errorln("Failed to generate random integer number:", err)
		}
		time.Sleep(time.Duration(nBig.Int64()+40) * time.Second)
	}
}

func getNewAvvisi(news, old []newstojson.News) []newstojson.News {
	res := make([]newstojson.News, 0, 30)
	// Gli avvisi sono ordinati (almeno si spera)
	if len(old) > 0 {
		// Range on new news
		for _, n := range news {
			found := false
			for _, o := range old {
				o := o
				if n.EqualsTo(&o) {
					// Found
					found = true
				}
			}
			if !found {
				res = append(res, n)
			}
		}
		return res

		// idOld := old[0].ID
		// for i := 0; i < len(old) && i < len(new); i++ {
		// 	if new[i].ID == idOld {
		// 		// Trovato il punto in cui i messaggi sono vecchi
		// 		return new[:i]
		// 	}
		// }
	}
	return news
}

func (fcw *FeedcrawlerWeb) SetDegreeIDs(d *googlecloud.Department, n *newstojson.News) error {
	courses, err := d.GetCourses(fcw.ds)
	if err != nil {
		log.Errorln("SetIDsCourses.GetCourses :", err)
		return err
	}
	for _, c := range courses {
		newsPerCourse, err := crawlers.RetrievesNews(d, c, 10)
		if err != nil {
			log.Errorln("SetIDsCourses.GetAvvisiC :", err)
			return err
		}
		// Retrives all the IDS for the news
		for _, npc := range newsPerCourse {
			if npc.ID == n.ID {
				// We found the course!
				n.DegreeIds = append(n.DegreeIds, int(c.ID))
				n.Courses = append(n.Courses, *c)
			}
		}
	}
	return nil
}
