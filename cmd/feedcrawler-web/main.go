package main

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"
	"time"

	graylog "github.com/gemnasium/logrus-graylog-hook/v3"

	"github.com/sirupsen/logrus"
	"gitlab.com/giovanni-liboni/univrnews/pkg/crawlers"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"

	newstojson "gitlab.com/giovanni-liboni/univrnews/pkg/news"
)

var activateDate time.Time
var log = logrus.New()

type FeedcrawlerWeb struct {
	activateDate time.Time
	wg           sync.WaitGroup
	depsURL      []string
	msg          chan string
	ds           *googlecloud.DatastoreGC
	ps           *googlecloud.PubSubGC
	deps         []googlecloud.Department
}

func (fcw *FeedcrawlerWeb) RetrieveDepartmentsURL() error {
	var err error
	// Retrieve rss urls from datastore
	fcw.deps, err = fcw.ds.GetAllDepartments()
	if err != nil {
		log.Fatalf("Datastore GetDepartments get: %v", err)
	}
	fmt.Println("\t> Retrieved", len(fcw.deps), "departments from Datastore")
	for _, dep := range fcw.deps {
		if dep.NoticesURL != "" {
			fcw.depsURL = append(fcw.depsURL, dep.NoticesURL)
		}
	}
	return nil
}

// ---------------------------------------------------------------------------
// Feedcrawler-web main functions
// ---------------------------------------------------------------------------
func main() {
	var err error
	fmt.Println("==> Starting Feed Crawler Web")
	now := time.Now()

	// ---------------------------------------------------------------------------
	// Environment checks
	// ---------------------------------------------------------------------------
	envs := []string{
		"TOPIC_NAME",
		"GOOGLE_PROJECT_ID",
		"GRAYLOG_SERVER",
	}

	for _, s := range envs {
		if os.Getenv(s) == "" {
			log.Fatalln("Please setup", s, " env first")
		}
	}
	// ---------------------------------------------------------------------------
	// Logger setup
	// ---------------------------------------------------------------------------
	//hook, err := sdhook.New(
	//	sdhook.GoogleServiceAccountCredentialsJSON([]byte(os.Getenv("GOOGLE_KEY"))),
	//)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//log.Hooks.Add(hook)
	//log.Level = logrus.InfoLevel

	hook := graylog.NewAsyncGraylogHook(os.Getenv("GRAYLOG_SERVER"), map[string]interface{}{"cmd": "feedcrawler-web"})
	defer hook.Flush()
	log.AddHook(hook)
	log.Level = logrus.DebugLevel

	// ---------------------------------------------------------------------------
	// FeedcrawlerWeb setup
	// ---------------------------------------------------------------------------
	fcw := FeedcrawlerWeb{
		activateDate: now.Add(-1 * time.Hour),
		ds:           new(googlecloud.DatastoreGC),
		ps:           new(googlecloud.PubSubGC),
	}
	activateDate = fcw.activateDate

	// Initialize Datastore client
	fcw.ds, err = googlecloud.NewDS()
	if err != nil {
		log.WithError(err).Errorln("googlecloud.NewDS error")
	}

	// Initialize PubSub Client
	fcw.ps, err = googlecloud.NewPS()
	if err != nil {
		log.WithError(err).Errorln("googlecloud.NewPS error")
	}
	log.Println("==> Starting Feed Crawler Web Version")

	err = fcw.RetrieveDepartmentsURL()
	if err != nil {
		log.WithError(err).Fatalln("Impossible to retrieve the department : ", err)
	}

	// Aspetto per tutti i canali che verranno creati
	fcw.wg.Add(len(fcw.depsURL) + 1)
	fcw.msg = make(chan string)
	go fcw.Send()

	for _, val := range fcw.deps {
		log.Infoln("Creating goroutine for ", val)
		go fcw.PollingOnAvvisi(val)
	}

	// TODO: Migliorare gestione thread
	fcw.wg.Wait()
}

func (fcw *FeedcrawlerWeb) Send() {
	defer fcw.wg.Done()
	for {
		data := <-fcw.msg
		topic := os.Getenv("TOPIC_NAME")
		if err := fcw.ps.Publish(topic, data); err != nil {
			log.WithField("topic", topic).WithField("data", string(data)).WithError(err).Errorln("Failed to publish :", err)
			// Retry TODO: Limit the number of unsuccessful attempts
			fcw.msg <- data
		} else {
			log.WithField("topic", topic).Infoln("Sent to queue:", string(data))
		}
	}
}

func (fcw *FeedcrawlerWeb) PollingOnAvvisi(dep googlecloud.Department) {
	log.Infoln("Added on polling url:", dep.NoticesURL)
	// Variabile degli avvisi
	var old []newstojson.News
	for {
		// Retrieves all the news
		tmp, err := crawlers.RetrievesNews(&dep, nil, 10)
		if err != nil {
			log.Errorln(err)
		}

		toSend := getNewAvvisi(tmp, old)
		if len(toSend) > 0 {
			log.WithField("url", dep.NoticesURL).Infoln("Retrived", len(toSend), " messages.")
		}

		for _, tmp := range toSend {
			tmp := tmp
			if tmp.IsNew(activateDate) {
				// Retrieve all the information
				tmp.Department = dep
				tmp.GetContentFromURL() // Retrieve all the content starting from the link
				err = fcw.SetDegreeIDs(&dep, &tmp)
				if err != nil {
					log.Errorln("PollingOnAvvisi.SetDegreeIDs :", err)
				}

				tmp.CreatedAt = time.Now()
				enc, err := json.Marshal(tmp)
				if err != nil {
					log.Errorln(err)
				}
				fcw.msg <- string(enc)
				log.WithField("url", dep.NoticesURL).Infoln("Sent to queue:", string(enc))
			}
		}
		old = tmp
		time.Sleep(30 * time.Second)
	}
}

func getNewAvvisi(news, old []newstojson.News) []newstojson.News {
	res := make([]newstojson.News, 0, 30)
	// Gli avvisi sono ordinati (almeno si spera)
	if len(old) > 0 {
		// Range on new news
		for _, n := range news {
			found := false
			for _, o := range old {
				o := o
				if n.EqualsTo(&o) {
					// Found
					found = true
				}
			}
			if !found {
				res = append(res, n)
			}
		}
		return res
	}
	return news
}

func (fcw *FeedcrawlerWeb) SetDegreeIDs(d *googlecloud.Department, n *newstojson.News) error {
	courses, err := d.GetCourses(fcw.ds)
	if err != nil {
		log.Errorln("SetIDsCourses.GetCourses :", err)
		return err
	}
	for _, c := range courses {
		// Update the notices url because the old one it's no more available
		c.NoticesURL = d.NoticesURL
		c.Save(fcw.ds.ClientDS)
		newsPerCourse, err := crawlers.RetrievesNews(d, c, 5)
		if err != nil {
			log.Errorln("SetIDsCourses.GetAvvisiC :", err)
			return err
		}
		// Retrives all the IDS for the news
		for _, npc := range newsPerCourse {
			if npc.ID == n.ID {
				// We found the course!
				n.DegreeIds = append(n.DegreeIds, int(c.ID))
				n.Courses = append(n.Courses, *c)
			}
		}
	}
	return nil
}
