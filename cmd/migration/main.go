package main

import (
	"log"
	"os"

	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
	"gitlab.com/giovanni-liboni/univrnews/pkg/sqldb"

	// Imports the Google Cloud Datastore client package.
	"cloud.google.com/go/datastore"
	"golang.org/x/net/context"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	pb "gopkg.in/cheggaaa/pb.v1"
)

/*
	Migrazione utenti dalla versione v1 alla versione v2.
	Differenze: Dipartimenti con ID differenti, corsi con ID simili.
	Recuperare tutti icorsi con i rispettivi utenti e spostarli in quelli nuovi.
	Per i dipartimenti creare una specifica tabella dove ci sono le referenze per
	i corsi vecchi e i corsi nuovi.
*/

func main() {
	log.Println("Started migration from PostgreSQL to Google Cloud Datastore")
	ctx := context.Background()

	// Connect to Heroku database
	DB, err := sqlx.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatalln(err)
	}
	if err = DB.Ping(); err != nil {
		log.Fatalln(err)
	}

	// Set your Google Cloud Platform project ID.
	projectID := os.Getenv("GOOGLE_PROJECT_ID")

	// Creates a client.
	client, err := datastore.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	ds := googlecloud.DatastoreGC{
		ClientDS: client,
	}

	log.Println("Migrating users...")
	// ---------------------------------------------------------------------------
	// Migrate all users to chat
	// ---------------------------------------------------------------------------
	var chats map[int64]googlecloud.Chat = make(map[int64]googlecloud.Chat)
	sum := 0
	// Chat migration section
	{
		var item googlecloud.Chat
		var count int
		err := sqldb.GetDB().Get(&count, "SELECT COUNT(*) as count FROM users")
		if err != nil {
			log.Fatalf("Failed to execute count query for chat: %v", err)
		}
		log.Println("Retrieved", count, " users to migrate")
		bar := pb.StartNew(count)
		rows, err := sqldb.GetDB().Queryx("SELECT telegram_id, status, created_at FROM users")
		if err != nil {
			log.Fatalf("Failed to execute query: %v", err)
		}

		for rows.Next() {
			bar.Increment()
			err = rows.StructScan(&item)
			if err != nil {
				log.Fatalln("Error on chat scan: ", err)
			}
			item.Save(client)
			chats[item.ID] = item
		}
		bar.FinishPrint("Users to chat migration completed!")
	}

	// ---------------------------------------------------------------------------
	// Migrate all Departments to Datastore with the followers and courses
	// ---------------------------------------------------------------------------

	log.Println("Migrating departments")
	// departments := SQLGetDepartments()
	// var departments []googlecloud.Department
	{
		ids := SQLGetDepartmentsID()
		for _, id := range ids {
			department := googlecloud.Department{
				ID: id,
			}
			err := department.Load(&ds)
			if err != nil {
				log.Fatalln(err)
			}
			// Set the new key for the department
			department.SetKey()
			// Get all followers
			followers := SQLGetFollowerDepartment(department.ID)
			// Add the user to the department followers
			for _, follower := range followers {
				department.AddChat(follower)

				c := chats[follower]
				c.AddDepartment(department)
				chats[follower] = c
			}
			department.Save(&ds)
		}
	}

	// ---------------------------------------------------------------------------
	// Migrate all courses to Datastore with the followers
	// ---------------------------------------------------------------------------
	log.Println("Migrating courses")
	{

		// Get all courses with their Followers
		rowsC, err := sqldb.GetDB().Queryx("SELECT id FROM corsi")
		if err != nil {
			log.Fatalf("Failed to execute query: %v", err)
		}

		for rowsC.Next() {
			// var course googlecloud.Course
			var id int
			err = rowsC.Scan(&id)
			// err = rowsC.StructScan(&id)
			if err != nil {
				log.Fatalf("Failed to execute query: %v", err)
			}
			course := googlecloud.Course{
				ID: int64(id),
			}
			// Load the followers
			var followers []int64
			err := sqldb.GetDB().Select(&followers, "SELECT users.telegram_id FROM users_corsi JOIN users ON (users.id = users_corsi.users_id) WHERE corsi_id=$1", course.ID)
			if err != nil {
				log.Fatalf("Failed to execute query: %v", err)
			}

			sum += len(followers)

			err = course.Load(&ds)
			if err != nil {
				if err != datastore.ErrNoSuchEntity {
					log.Println("course.Load", course.Name, ":", err)
				} else if len(followers) > 0 {
					// Course not found and it has followers
					log.Println("-------------------------------------------------------")
					log.Println(" Course Name      :", course.Name)
					log.Println(" Course ID        :", course.ID)
					log.Println(" Course followers :", len(followers))
					log.Println(" Error            :", err)
					log.Println("-------------------------------------------------------")
				}
			} else {
				if course.Key != nil {
					for _, follower := range followers {
						c := chats[follower]
						course.AddChat(c.GetKey())
						c.AddCourse(course.Key)
						chats[follower] = c
					}
					course.Save(client)
					log.Println("****************************")
					log.Println("Course migrated  :", course.Name)
					log.Println("Course followers :", len(followers))
					log.Println("Dep              :", course.Key)
					log.Println("****************************")
				} else {
					// Corso vecchio
					// log.Println("DepartmentKey is NULL for ", course.Name)
				}
			}
		} // End rowsC.Next()
	} // End block

	// ---------------------------------------------------------------------------
	// Save departments and chats
	// ---------------------------------------------------------------------------

	// for _, dep := range departments {
	// 	dep.Save(client)
	// }
	// Save all the chats ONCE
	log.Println("Saving all users...")
	bar := pb.StartNew(len(chats))
	for _, c := range chats {
		c.Save(client)
		bar.Increment()
	}
	bar.Finish()
	log.Println("Total users :", sum)
}

// SQLGetDepartments gets departments
func SQLGetDepartments() []googlecloud.Department {
	var department googlecloud.Department
	var departments []googlecloud.Department
	rows, err := sqldb.GetDB().Queryx("SELECT id FROM dipartimenti")
	if err != nil {
		log.Fatalf("Failed to execute query: %v", err)
	}
	for rows.Next() {
		err = rows.StructScan(&department)
		if err != nil {
			log.Fatalln("Error on department scan: ", err)
		}
		departments = append(departments, department)
	}
	return departments
}

// SQLGetDepartments gets departments
func SQLGetDepartmentsID() []int64 {
	var department int64
	var departments []int64
	rows, err := sqldb.GetDB().Queryx("SELECT id FROM dipartimenti")
	if err != nil {
		log.Fatalf("Failed to execute query: %v", err)
	}
	for rows.Next() {
		err = rows.Scan(&department)
		if err != nil {
			log.Fatalln("Error on department scan: ", err)
		}
		departments = append(departments, department)
	}
	return departments
}

// SQLGetFollowerDepartment gets followers for the department
func SQLGetFollowerDepartment(id int64) []int64 {
	var followers []int64
	// Find all users
	err := sqldb.GetDB().Select(&followers, "SELECT users.telegram_id FROM users_dipart JOIN users ON (users.id = users_dipart.users_id) WHERE dipart_id=$1", id)
	if err != nil {
		log.Fatalf("Failed to execute query: %v", err)
	}
	return followers
}

func SQLGetCoursesDepartment(id int64) []googlecloud.Course {
	// Find the courses associated with the department
	var course googlecloud.Course
	var courses []googlecloud.Course
	rowsC, err := sqldb.GetDB().Queryx("SELECT corsi.id, corsi.nome FROM corsi JOIN corsi_dipart ON (corsi_dipart.corsi_id = corsi.id) WHERE corsi_dipart.dipart_id=$1", id)
	if err != nil {
		log.Fatalf("Failed to execute query: %v", err)
	}

	for rowsC.Next() {
		err = rowsC.StructScan(&course)
		if err != nil {
			log.Fatalf("Failed to execute query: %v", err)
		}
		courses = append(courses, course)
	}
	return courses
}
