# Quickstart

Setup these env variables first:
- `GOOGLE_APPLICATION_CREDENTIALS` : Path to the credentials.json file
- `GOOGLE_PROJECT_ID` : Name of the Google Cloud Project

Run this tool as follow:
`DATABASE_URL=$(heroku config:get DATABASE_URL -a <your-app>) go run main.go`

with `<your-app>` the name of the Heroku app.
