package main

import (
	"log"

	"github.com/gocolly/colly"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
)

func main() {

	_, err := googlecloud.NewDS()
	if err != nil {
		log.Fatalln("googlecloud.NewDS :", err)
	}

	/*
	 * Retrieve all the courses for Scuole di specializzazione (area di Medicina e Chirurgia)
	 * URL: https://www.univr.it/it/post-laurea/2018-2019/tutte-le-aree/scuole-di-specializzazione-area-di-medicina-e-chirurgia
	 */
	// //*[@id="portlet_it_univr_aolux_portlet_lista_post_PostLaureaListaPortlet_INSTANCE_iKOyenVyRQwD"]/div/div/div/div/div/div/div
	coursesCollector := colly.NewCollector()
	coursesCollector.OnHTML("tr", func(e *colly.HTMLElement) {

	})
}
