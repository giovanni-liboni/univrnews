# UnivrNews Crawler
This module retrieves the news from the RSS website of the
`Università degli Studi di Verona`.
It parses the news and then sends them to the Google Pub/Sub queue system.

# Hosting
This module is powered by Heroku, which is connected to this repository.
The running code is inside the master branch. All the tests run before deployment.

# Environment

- `GOOGLE_CREDENTIALS`
- `GOOGLE_KEY`

# Tips

## Heroku

First, we need to configure the remote Heroku project locally.

- `heroku addons:create heroku-postgresql:hobby-dev`
- `heroku buildpacks:clear`
- `heroku buildpacks:set https://github.com/giovanni-liboni/heroku-buildpack-go-multi-binaries`
- `heroku buildpacks:add heroku/go`
- `heroku config:set APP_PATH=cmd/dispatcher`

### Environment variables

We can set an environment variable in Heroku using the following command:

```
heroku config:set VARIABLE=<value>
```
This application needs these variables:
- `GOOGLE_ACCOUNT_TYPE` : es. `service-account`;
- `GOOGLE_APPLICATION_CREDENTIALS` : The path to the file in which the credentials can be stored. Eg. `/tmp/credentials.json`;
- `GOOGLE_CLIENT_EMAIL` : The email associated with your Google Account;
- `GOOGLE_KEY` : The Google Key;
- `GOOGLE_PRIVATE_KEY` : Private Key;
- `GOOGLE_PROJECT_ID` : The project ID;

## `Procfile`

Due to the usage of the buildpack `https://github.com/giovanni-liboni/heroku-buildpack-go-multi-binaries`, the worker name is set to `univrnews`.