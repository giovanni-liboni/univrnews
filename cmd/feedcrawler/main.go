package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"sync"
	"time"

	graylog "github.com/gemnasium/logrus-graylog-hook/v3"

	"github.com/sirupsen/logrus"
	"gitlab.com/giovanni-liboni/univrnews/pkg/crawlers"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
	newstojson "gitlab.com/giovanni-liboni/univrnews/pkg/news"
	rss "gitlab.com/giovanni-liboni/univrnews/pkg/rss"
	"gitlab.com/giovanni-liboni/univrnews/pkg/xmlx"
)

var activateDate time.Time
var wg sync.WaitGroup
var log = logrus.New()
var depsMap map[string]googlecloud.Department
var fc Feedcrawler

type Feedcrawler struct {
	ds           *googlecloud.DatastoreGC
	ps           *googlecloud.PubSubGC
	ActivateDate time.Time
}

func main() {
	var err error
	log.Println("==> Starting Feed Crawler")
	// ---------------------------------------------------------------------------
	// Environment checks
	// ---------------------------------------------------------------------------
	envs := []string{
		"TOPIC_NAME",
		"GOOGLE_PROJECT_ID",
		"GRAYLOG_SERVER",
	}

	for _, s := range envs {
		if os.Getenv(s) == "" {
			log.Fatalln("Please setup", s, " env first")
		}
	}
	// ---------------------------------------------------------------------------
	// Logger setup
	// ---------------------------------------------------------------------------
	//hook, err := sdhook.New(
	//	sdhook.GoogleServiceAccountCredentialsJSON([]byte(os.Getenv("GOOGLE_KEY"))),
	//)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//log.Hooks.Add(hook)
	//log.Level = logrus.InfoLevel

	hook := graylog.NewAsyncGraylogHook(os.Getenv("GRAYLOG_SERVER"), map[string]interface{}{"cmd": "feedcrawler-rss"})
	defer hook.Flush()
	log.AddHook(hook)
	log.Level = logrus.DebugLevel

	// ---------------------------------------------------------------------------
	// Initialization
	// ---------------------------------------------------------------------------
	fc.ds, err = googlecloud.NewDS()
	if err != nil {
		log.WithError(err).Fatalln("Failed googlecloud.NewDS")
	}
	fc.ps, err = googlecloud.NewPS()
	if err != nil {
		log.WithError(err).Fatalln("Failed googlecloud.NewPS")
	}
	fc.ActivateDate = time.Now()
	activateDate = time.Now().Add(-10 * time.Minute)

	// ---------------------------------------------------------------------------
	// Get all department from the Datastore
	// ---------------------------------------------------------------------------
	deps, err := fc.ds.GetAllDepartments()
	if err != nil {
		log.WithError(err).Fatalln("Failed fc.ds.GetAllDepartments")
	}
	depsMap = make(map[string]googlecloud.Department)

	for _, d := range deps {
		if d.RSSLink != "" {
			depsMap[d.RSSLink] = d
		}
	}

	// ---------------------------------------------------------------------------
	// Create all the goroutine to listen each Department RSS link
	// ---------------------------------------------------------------------------
	for _, dep := range deps {
		if dep.RSSLink != "" {
			wg.Add(1)
			go PollFeedDep(dep, 1, charsetReader)
		} else {
			log.Println("Department", dep.Name, "has no RSS link")
		}
	}
	log.Infoln("Starting setup completed")
	// ---------------------------------------------------------------------------
	// Waiting...
	// ---------------------------------------------------------------------------
	wg.Wait()
}

/*
chanHandler to handle channel
*/
func chanHandler(feed *rss.Feed, newchannels []*rss.Channel) {
	fmt.Printf("%d new channel(s) in %s\n", len(newchannels), feed.Url)
}

/*
itemHandler the rss items
*/
func itemHandler(feed *rss.Feed, ch *rss.Channel, newitems []*rss.Item) {
	log.WithField("feedURL", feed.Url).WithField("len", len(newitems)).Infoln("New item(s) in feed")
	for _, val := range newitems {
		news, err := newstojson.Parse(val)
		if err != nil {
			log.WithError(err).Errorln("Error during newstojson parsing")
		} else {
			if news.IsNew(activateDate) {
				log.WithField("NewsID", news.ID).Infoln("Start parsing news")
				// Retrieve all the information
				news.Department = depsMap[feed.Url]
				err = news.CompleteParse()
				if err != nil {
					log.WithError(err).WithField("NewsID", news.ID).Errorln("fcw.CompleteParse")
				}
				news.GetContentFromURL()
				err = crawlers.SetDegreeIDs(fc.ds, news)
				if err != nil {
					log.WithError(err).WithField("DegreeID", val.Id).Errorln("fcw.SetDegreeIDs")
				}
				log.WithField("NewsID", news.ID).Infoln("Parsing news completed")
				enc, err := json.Marshal(news)
				if err != nil {
					log.WithError(err).Errorln("Error during encoding news to JSON")
				}
				err = fc.ps.Publish(os.Getenv("TOPIC_NAME"), string(enc))
				if err != nil {
					log.WithField("post-id", news.ID).WithError(err).Errorln("Failed to publish the post on the queue")
				} else {
					log.WithField("post-id", news.ID).Infoln("Published post on", os.Getenv("TOPIC_NAME"), "with ID", news.ID)
				}
			}
		}
	}
}

/*
PollFeed to fetch the channel and parse its news.
*/
func PollFeed(uri string, timeout int, cr xmlx.CharsetFunc) {
	feed := rss.New(timeout, true, chanHandler, itemHandler)
	defer wg.Done()
	for {
		if err := feed.Fetch(uri, cr); err != nil {
			log.WithError(err).WithField("RSSLink", uri).Errorln("Failed to fetch RSS")
		}
		<-time.After(time.Duration(feed.SecondsTillUpdate() * 1e9))
	}
}

func PollFeedDep(dep googlecloud.Department, timeout int, cr xmlx.CharsetFunc) {
	feed := rss.New(timeout, true, chanHandler, itemHandler)
	defer wg.Done()
	for {
		if err := feed.Fetch(dep.RSSLink, cr); err != nil {
			log.WithError(err).WithField("RSSLink", dep.RSSLink).Errorln("Failed to fetch department RSS")
		}
		<-time.After(time.Duration(feed.SecondsTillUpdate() * 1e9))
	}
}

/*
charsetReader to accept only iso-8859-1 encoded rss news
*/
func charsetReader(charset string, r io.Reader) (io.Reader, error) {
	if charset == "ISO-8859-1" || charset == "iso-8859-1" {
		return r, nil
	}
	return nil, errors.New("Unsupported character set encoding: " + charset)
}
