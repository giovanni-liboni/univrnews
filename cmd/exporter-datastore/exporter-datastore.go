package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"

	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
)

func main() {

	var schat string
	flag.StringVar(&schat, "chat", "", "Path to the output JSON exported Chat")
	flag.Parse()

	ds, err := googlecloud.NewDS()
	if err != nil {
		log.Fatalln("googlecloud.NewDS :", err)
	}

	// Export Chat
	if schat != "" {
		log.Println("Exporting chat")
		res, err := ds.GetAllChats()
		if err != nil {
			log.Fatalln(err)
		}
		jsonRes, err := json.Marshal(res)
		if err != nil {
			log.Fatalln(err)
		}
		err = ioutil.WriteFile(schat, jsonRes, 0600)
		if err != nil {
			log.Fatalln(err)
		}
		log.Println("Chats:", len(res))
	}
}
