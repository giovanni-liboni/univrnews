package main

import (
	"os"
	"strings"
	"time"

	graylog "github.com/gemnasium/logrus-graylog-hook/v3"

	"cloud.google.com/go/logging"
	mailgun "github.com/mailgun/mailgun-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
	tb "gopkg.in/tucnak/telebot.v2"
)

type Backend struct {
	b      *tb.Bot
	log    *logrus.Logger
	ds     *googlecloud.DatastoreGC
	ps     *googlecloud.PubSubGC
	mg     mailgun.Mailgun
	logger *logging.Logger
}

var log = logrus.New()

func main() {
	// ---------------------------------------------------------------------------
	// Environment checks
	// ---------------------------------------------------------------------------
	envs := []string{
		"TELEBOT_SECRET",
		"GOOGLE_PROJECT_ID",
		"GRAYLOG_SERVER",
	}

	for _, s := range envs {
		if os.Getenv(s) == "" {
			log.Fatalln("Please setup", s, " env first")
		}
	}

	// ---------------------------------------------------------------------------
	// Logger setup
	// ---------------------------------------------------------------------------
	// create hook using service account credentials
	//hook, err := sdhook.New(
	//	sdhook.GoogleServiceAccountCredentialsJSON([]byte(os.Getenv("GOOGLE_KEY"))),
	//)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//log.Hooks.Add(hook)
	//log.Level = logrus.InfoLevel

	hook := graylog.NewAsyncGraylogHook(os.Getenv("GRAYLOG_SERVER"), map[string]interface{}{"cmd": "tgbot-backend"})
	defer hook.Flush()
	log.AddHook(hook)
	log.Level = logrus.DebugLevel

	// ---------------------------------------------------------------------------
	// Telegram bot setup
	// ---------------------------------------------------------------------------
	log.Infoln("Starting tgbot-backend...")
	log.Infoln("\tCreating a new bot")

	b, err := tb.NewBot(tb.Settings{
		Token: os.Getenv("TELEBOT_SECRET"),
		// Poller: &tb.Webhook{Listen: ":" + os.Getenv("PORT"), Endpoint: &tb.WebhookEndpoint{PublicURL: os.Getenv("TELEBOT_WEBHOOK")}},
		Poller:   &tb.LongPoller{Timeout: 10 * time.Second},
		Reporter: func(e error) { log.Errorln(e) },
	})
	if err != nil {
		log.WithError(err).Fatalln("Error while initializing bot")
		return
	}
	be := Backend{
		b:   b,
		log: log,
		mg:  mailgun.NewMailgun(os.Getenv("MAILGUN_DOMAIN"), os.Getenv("MAILGUN_API_KEY")),
	}

	// ---------------------------------------------------------------------------
	// Datastore client setup
	// ---------------------------------------------------------------------------
	log.Infoln("\tCreating the Datastore Client")
	be.ds, err = googlecloud.NewDS()
	if err != nil {
		log.WithError(err).Fatalln("Error while initializing datastore")
		return
	}

	// ---------------------------------------------------------------------------
	// Pub/Sub client setup
	// ---------------------------------------------------------------------------
	log.Println("\tCreating the Pub/Sub Client")
	be.ps, err = googlecloud.NewPS()
	if err != nil {
		log.WithError(err).Fatalln("Error while initializing pub/sub")
		return
	}

	// ---------------------------------------------------------------------------
	// Initialize the inline buttons with the prefilled fields
	// ---------------------------------------------------------------------------
	be.InitInlineButtons()
	log.Infoln("Inline buttons initialized!")

	// ---------------------------------------------------------------------------
	// Adding commands endpoints to the bot
	// ---------------------------------------------------------------------------
	b.Handle("/start", be.TGStart)
	b.Handle("/chart", be.createChart)

	// all the text messages that weren't
	// captured by existing handlers
	b.Handle(tb.OnText, be.TGSupportMessage)

	b.Handle(tb.OnQuery, func(q *tb.Query) {
		log.Infoln("OnQuery")
	})

	b.Handle(tb.OnCallback, func(q *tb.Callback) {
		if strings.Contains(q.Data, "homepage|homepage") {
			be.TGStart(q.Message)
		}
	})

	// ---------------------------------------------------------------------------
	// Start polling to retrieve messages and reply
	// ---------------------------------------------------------------------------
	log.Infoln("Start Polling...")
	b.Start()
	log.Infoln("Started!")
}
