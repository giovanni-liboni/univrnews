package main

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"cloud.google.com/go/datastore"
	"github.com/sirupsen/logrus"
	chart "github.com/wcharczuk/go-chart"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
	tb "gopkg.in/tucnak/telebot.v2"
)

var (
	welcomeMessage = `
  ➡ Ricevi direttamente su Telegram gli avvisi del tuo corso! 📨
➡ Tutti gli avvisi inerenti al tuo corso ti verranno inviati appena saranno pubblicati 🕐
  `
	chooseDepartment = `➡ Inizia subito, scegli il tuo dipartimento dall'elenco sottostante:`
)

// TGStart send back the welcomeMessage
func (be *Backend) TGStart(m *tb.Message) {
	chat := googlecloud.Chat{
		ID: m.Chat.ID,
	}
	err := chat.Load(be.ds)
	if err != nil {
		if err == datastore.ErrNoSuchEntity {
			chat.CreatedAT = time.Now()
			chat.Save(be.ds.ClientDS)
		} else {
			be.log.WithError(err).WithField("method", "TGStart").Errorln("Error loading the chat")
		}
	}
	status := chat.Status

	if status != 0 && status > 0 {
		chat.Status = 0
		chat.Save(be.ds.ClientDS)
	}
	// Starting messagge
	be.b.Send(m.Chat, welcomeMessage, &tb.SendOptions{
		ReplyMarkup: &tb.ReplyMarkup{
			InlineKeyboard: InlineHomeBts,
		},
		ParseMode: tb.ModeHTML,
	})
}

func (be *Backend) TGSupport(c *tb.Callback) {
	log.WithField("user", c.Message.Chat.ID).Infoln("TGSupport")
	chat := googlecloud.Chat{
		ID: c.Message.Chat.ID,
	}
	err := chat.Load(be.ds)
	if err != nil {
		if err == datastore.ErrNoSuchEntity {
			chat.CreatedAT = time.Now()
			chat.Save(be.ds.ClientDS)
		} else {
			be.log.WithError(err).WithField("method", "TGSupport").Errorln("Error loading the chat")
		}
	}
	// Status 21: The user sent a support request
	chat.Status = 21
	go chat.Save(be.ds.ClientDS)

	inlineBtnNo := tb.InlineButton{
		Unique: "backToHome",
		Text:   "🔙  Indietro",
		Data:   "",
	}
	be.b.Handle(&inlineBtnNo, be.TGHomeCallback)

	// Starting messagge
	be.b.Send(c.Message.Chat, "Se non trovi il corso o non ricevi gli avvisi, puoi contattarci scrivendo qui sotto. Cercheremo di risolvere il problema il prima possibile.", &tb.SendOptions{
		ParseMode: tb.ModeMarkdown,
		ReplyMarkup: &tb.ReplyMarkup{
			InlineKeyboard: [][]tb.InlineButton{[]tb.InlineButton{inlineBtnNo}},
		},
	})
}

type SupportMessage struct {
	UserID   int64
	Name     string
	Surname  string
	Username string
	Message  string
	Data     time.Time
}

func (be *Backend) TGSupportMessage(m *tb.Message) {
	chat := googlecloud.Chat{
		ID: m.Chat.ID,
	}
	err := chat.Load(be.ds)
	if err != nil {
		be.log.Error(err)
		if err == datastore.ErrNoSuchEntity {
			chat.CreatedAT = time.Now()
			chat.Save(be.ds.ClientDS)
		} else {
			be.log.WithError(err).WithField("method", "TGSupportMessage").Errorln("Error loading the chat")
		}
	}
	if chat.Status == 21 {
		// for the moment just log the message
		be.log.WithField("chatID", m.Chat.ID).WithField("type", "support").Warnln(m.Text)
		ctx := context.Background()
		message := SupportMessage{
			UserID:   m.Chat.ID,
			Message:  m.Text,
			Data:     time.Now(),
			Name:     m.Chat.FirstName,
			Surname:  m.Chat.LastName,
			Username: m.Chat.Username,
		}
		key := datastore.IncompleteKey("Support_message", nil)

		_, err := be.ds.ClientDS.Put(ctx, key, &message)
		if err != nil {
			be.log.WithError(err).WithField("method", "TGSupportMessage").Errorln("TGSupportMessage.ClientDS.Put")
		}

		// Send also an email
		go be.sendMail(m.Text, message.UserID)

		// Starting messagge
		inlineKeys := [][]tb.InlineButton{}
		inlineBtn := tb.InlineButton{
			Unique: "homepage",
			Text:   "🔙  Vai al menù principale",
			Data:   "homepage",
		}
		inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtn})

		be.b.Send(m.Chat, "Messaggio ricevuto! Grazie per il feedback!", &tb.SendOptions{
			ReplyMarkup: &tb.ReplyMarkup{
				InlineKeyboard: inlineKeys,
			},
			ParseMode: tb.ModeHTML,
		})
		chat.Status = 0
		go chat.Save(be.ds.ClientDS)
	}
}

func (be *Backend) TGHomeCallback(c *tb.Callback) {
	log.WithField("user", c.Message.Chat.ID).Infoln("New TGHomeCallback!")
	// Starting messagge
	be.b.Edit(c.Message, welcomeMessage, &tb.SendOptions{
		ReplyMarkup: &tb.ReplyMarkup{
			InlineKeyboard: InlineHomeBts,
		},
		ParseMode: tb.ModeHTML,
	})
}

func (be *Backend) TGHome(c *tb.Callback) {
	be.log.WithField("user", c.Message.Chat.ID).Infoln("New TGHome!")

	res, _ := strconv.Atoi(c.Data)
	if res == 1 {
		be.b.Edit(c.Message, chooseDepartment, &tb.SendOptions{
			ReplyMarkup: &tb.ReplyMarkup{
				InlineKeyboard: InlineDepartmentsBts,
			},
			ParseMode: tb.ModeHTML,
		})
	} else { // Unsubscribe from news

		inlineKeys, err := be.InlineUnsubscribeCourses(c.Message.Chat.ID)
		if err != nil {
			be.log.WithError(err).WithField("method", "TGHome").Errorln("Error InlineUnsubscribeCourses")
			be.b.Edit(c.Message, "Errore durante l'iscrizione, riprovare più tardi")
			return
		}
		be.b.Edit(c.Message, "➡ Per non ricevere più gli avvisi selezionare il corso corrispondente:", &tb.SendOptions{
			ReplyMarkup: &tb.ReplyMarkup{
				InlineKeyboard: inlineKeys,
			},
			ParseMode: tb.ModeHTML,
		})
	}
}

// TGSubscribeCourseFromDepartment take as input message a department
// and send back the list of courses. The inline button is built in
// inlineButtons.go file
func (be *Backend) TGSubscribeCourseFromDepartment(c *tb.Callback) {
	logger := be.log.WithFields(logrus.Fields{
		"userID": c.Message.Chat.ID,
		"method": "TGSubscribeCourseFromDepartment",
	})
	logger.Infoln("New TGSubscribeCourseFromDepartment!")
	// Send courses list
	depID, err := strconv.ParseInt(c.Data, 10, 64)
	if err != nil {
		logger.WithError(err).Errorln("Error during Atoi conversion")
		return
	}
	dep := googlecloud.Department{
		ID: depID,
	}
	err = dep.Load(be.ds)
	if err != nil {
		logger.WithError(err).Errorln("Error loading department")
		return
	}

	chooseCourse := "➡ Hai selezionato <b>" + dep.Name + "</b>, ora scegli il tuo corso tra quelli disponibili:"

	be.b.Edit(c.Message, chooseCourse, &tb.SendOptions{
		ReplyMarkup: &tb.ReplyMarkup{
			InlineKeyboard: InlineCoursesBts[depID],
		},
		ParseMode: tb.ModeHTML,
	})
}

func (be *Backend) TGSubscribeCourse(c *tb.Callback) {
	logger := be.log.WithFields(logrus.Fields{
		"userID": c.Message.Chat.ID,
		"method": "TGSubscribeCourse",
	})
	logger.Infoln("New TGSubscribeCourse!")
	res := strings.Split(c.Data, ",")
	courseID, _ := strconv.Atoi(res[0])
	depID, _ := strconv.Atoi(res[1])
	// If press back then re-send departments
	if courseID == 0 {
		be.b.Edit(c.Message, chooseDepartment, &tb.SendOptions{
			ReplyMarkup: &tb.ReplyMarkup{
				InlineKeyboard: InlineDepartmentsBts,
			},
			ParseMode: tb.ModeHTML,
		})
		return
	}
	course := googlecloud.Course{
		ID: int64(courseID),
	}
	err := course.Load(be.ds)
	if err != nil {
		logger.WithError(err).Errorln("Error loading course")
	}

	chooseYesOrBack := "➡ Vuoi ricevere gli avvisi per " + course.Name + " ?"
	inlineKeys, _ := InlineSubscribeOrBack(courseID, depID)
	be.b.Edit(c.Message, chooseYesOrBack, &tb.SendOptions{
		ReplyMarkup: &tb.ReplyMarkup{
			InlineKeyboard: inlineKeys,
		},
		ParseMode: tb.ModeHTML,
	})
}

func (be *Backend) TGSubscribe(c *tb.Callback) {
	logger := be.log.WithFields(logrus.Fields{
		"userID": c.Message.Chat.ID,
		"method": "TGSubscribe",
	})
	logger.Infoln("New TGSubscribe!")
	data := strings.Split(c.Data, ",")
	res, _ := strconv.Atoi(data[0])
	id, _ := strconv.ParseInt(data[1], 10, 64)
	if res == 1 {
		// Answer is YES

		// Load the chat
		chat := googlecloud.Chat{
			ID: c.Message.Chat.ID,
		}
		err := chat.Load(be.ds)
		if err != nil {
			logger.WithError(err).Errorln("Error loading chat")
			be.b.Edit(c.Message, "Errore durante l'iscrizione, riprovare più tardi")
			return
		}

		// Load the course
		course := googlecloud.Course{
			ID: id,
		}
		err = course.Load(be.ds)
		if err != nil {
			logger.WithError(err).Errorln("Error loading course")
			be.b.Edit(c.Message, "Errore durante l'iscrizione, riprovare più tardi")
			return
		}

		// Add the course to the chat
		chat.AddCourse(course.Key)

		// Add the chat to the course
		course.AddChat(chat.Key)

		course.Save(be.ds.ClientDS)

		chat.Save(be.ds.ClientDS)

		// Success
		inlineKeys := [][]tb.InlineButton{}
		inlineBtn := tb.InlineButton{
			Unique: "goToStart",
			Text:   "🔙  Vai al menù principale",
			Data:   "0",
		}
		be.b.Handle(&inlineBtn, be.TGHomeCallback)
		inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtn})
		be.log.WithField("user", c.Message.Chat.ID).Infoln("Course Name :", course.Name)
		be.b.Edit(c.Message, "Iscrizione avvenuta con successo. Da questo momento riceverai gli avvisi relativi al corso: "+course.Name+".", &tb.SendOptions{
			ReplyMarkup: &tb.ReplyMarkup{
				InlineKeyboard: inlineKeys,
			},
			ParseMode: tb.ModeHTML,
		})
		return
	}
	// Answer is BACK
	dep := googlecloud.Department{
		ID: id,
	}
	err := dep.Load(be.ds)
	if err != nil {
		logger.WithError(err).Errorln("Error loading department")
		be.b.Edit(c.Message, "Errore durante l'iscrizione, riprovare più tardi")
		return
	}

	chooseCourse := "➡ Hai selezionato " + dep.Name + ", ora scegli il tuo corso"

	be.b.Edit(c.Message, chooseCourse, &tb.SendOptions{
		ReplyMarkup: &tb.ReplyMarkup{
			InlineKeyboard: InlineCoursesBts[int64(id)],
		},
		ParseMode: tb.ModeHTML,
	})
}

/*
 * TGUnsubscribe send back a list of courses
 */
func (be *Backend) TGUnsubscribe(c *tb.Callback) {
	be.log.WithField("user", c.Message.Chat.ID).Infoln("New TGUnsubscribe!")
	chatID := c.Message.Chat.ID
	courseID, _ := strconv.ParseInt(c.Data, 10, 64)

	// Load chat
	chat := googlecloud.Chat{
		ID: chatID,
	}
	err := chat.Load(be.ds)
	if err != nil {
		be.log.Errorf("TGUnsubscribe: %v", err)
		be.b.Edit(c.Message, "Errore durante la richiesta, riprovare più tardi")
		return
	}

	course := googlecloud.Course{
		ID: courseID,
	}
	err = course.Load(be.ds)
	if err != nil {
		be.log.Errorf("GetCourse: %v", err)
		be.b.Edit(c.Message, "Errore durante l'iscrizione, riprovare più tardi")
		return
	}

	err = chat.RemoveCourse(be.ds, courseID)
	if err != nil {
		be.log.Errorf("TGUnsubscribe: %v", err)
		be.b.Edit(c.Message, "Errore durante la richiesta, riprovare più tardi")
		return
	}

	err = course.RemoveChat(be.ds, chatID)
	if err != nil {
		be.log.Errorf("TGUnsubscribe: %v", err)
		be.b.Edit(c.Message, "Errore durante la richiesta, riprovare più tardi")
		return
	}

	course.Save(be.ds.ClientDS)
	chat.Save(be.ds.ClientDS)

	// Success
	inlineKeys := [][]tb.InlineButton{}
	inlineBtn := tb.InlineButton{
		Unique: "goToStart",
		Text:   "🔙  Vai al menù principale",
		Data:   "0",
	}
	be.b.Handle(&inlineBtn, be.TGHomeCallback)
	inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtn})

	be.b.Edit(
		c.Message,
		"Disiscritto con successo! Da questo momento non riceverai più gli avvisi del corso scelto.",
		&tb.SendOptions{
			ReplyMarkup: &tb.ReplyMarkup{
				InlineKeyboard: inlineKeys,
			},
			ParseMode: tb.ModeHTML,
		})
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
func (be *Backend) sendMail(message string, userID int64) {
	sender := os.Getenv("MAILGUN_SENDER")
	subject := "UnivrNews: Support request from " + strconv.FormatInt(userID, 10)
	recipient := os.Getenv("MAILGUN_RECIPIENT")

	// The message object allows you to add attachments and Bcc recipients
	msg := be.mg.NewMessage(sender, subject, message, recipient)

	// ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	// defer cancel()

	// Send the message	with a 10 second timeout
	_, _, err := be.mg.Send(msg)

	if err != nil {
		be.log.Fatal(err)
	}
}

// -----------------------------------------------------------------------------

type timeSlice []time.Time

func (s timeSlice) Less(i, j int) bool { return s[i].Before(s[j]) }
func (s timeSlice) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s timeSlice) Len() int           { return len(s) }

var chats []googlecloud.Chat

// -----------------------------------------------------------------------------
func (be *Backend) createChart(m *tb.Message) {
	log.Println("Creating chart")
	var err error
	// var dates []time.Time
	var d map[time.Time]int = make(map[time.Time]int)

	if len(chats) == 0 {
		chats, err = be.ds.GetAllChats()
		if err != nil {
			log.Errorln("be.ds.GetAllChats", err)
			return
		}
	}
	for _, c := range chats {
		rounded := time.Date(c.CreatedAT.Year(), c.CreatedAT.Month(), 0, 0, 0, 0, 0, c.CreatedAT.Location())
		d[rounded] = d[rounded] + 1
	}

	var dateSlice timeSlice
	var dateTime []time.Time

	for key := range d {
		dateTime = append(dateTime, key)
	}
	dateSlice = dateTime
	sort.Sort(dateSlice)

	var data []float64
	for i := 0; i < len(dateSlice); i++ {
		data = append(data, float64(d[dateSlice[i]]))
	}
	for i := 1; i < len(data); i++ {
		data[i] = data[i-1] + data[i]
	}

	graph := chart.Chart{
		XAxis: chart.XAxis{
			Style: chart.StyleShow(),
		},
		YAxis: chart.YAxis{
			Name:      "Users",
			NameStyle: chart.StyleShow(),
			Style:     chart.StyleShow(),
		},
		Series: []chart.Series{
			chart.TimeSeries{
				XValues: dateSlice,
				YValues: data,
			},
		},
	}
	// res.Header().Set("Content-Type", "image/png")
	buf := new(bytes.Buffer)
	err = graph.Render(chart.PNG, buf)
	if err != nil {
		log.Errorln("graph.Render", err)
	}

	p := &tb.Photo{File: tb.File{FileReader: buf}}

	_, err = be.b.Send(m.Chat, p)
	if err != nil {
		log.Errorln("be.b.Send", err)
	}
	fmt.Println(p.InCloud())
}
