package main

// TODO: Restore these tests
// func TestInitInlineButtons(t *testing.T) {
// 	os.Setenv("APPENGINE_DEV_APPSERVER", os.Getenv("APPENGINE_DEV_APPSERVER_TEST"))
// 	_, done, err := aetest.NewContext()
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	defer done()
//
// 	_, err = telebot.NewBot(telebot.Settings{
// 		Token:   "3424234:uh54h5jk4hk",
// 		HookURL: &telebot.WebHook{Path: "/test"},
// 	})
// 	if err != nil {
// 		t.Fatal(err)
// 	}
//
// 	// InitInlineButtons(ctx, b)
// }

// func TestInlineUnsubscribeCourses(t *testing.T) {
// 	os.Setenv("APPENGINE_DEV_APPSERVER", os.Getenv("APPENGINE_DEV_APPSERVER_TEST"))
// 	ctx, done, err := aetest.NewContext()
// 	if err != nil {
// 		t.Fatal(err)
// 	}
// 	defer done()
//
// 	// Populate datastore
// 	chatKey1 := datastore.NewKey(ctx, "Chat", "", 1, nil)
// 	chatKey2 := datastore.NewKey(ctx, "Chat", "", 2, nil)
//
// 	courseKey2 := datastore.NewKey(ctx, "Course", "", 2, nil)
// 	if _, err = datastore.Put(ctx, courseKey2, &gae.Course{ID: 2, Name: "Test", FollowersKeys: []*datastore.Key{chatKey1, chatKey2}}); err != nil {
// 		t.Fatal(err)
// 	}
// 	courseKey3 := datastore.NewKey(ctx, "Course", "", 3, nil)
// 	if _, err = datastore.Put(ctx, courseKey3, &gae.Course{ID: 3, Name: "Test", FollowersKeys: []*datastore.Key{chatKey1, chatKey2}}); err != nil {
// 		t.Fatal(err)
// 	}
//
// 	_, err = datastore.Put(ctx, chatKey1, &gae.Chat{ID: 1, CoursesKeys: []*datastore.Key{courseKey2, courseKey3}})
// 	if err != nil {
// 		t.Fatal(err)
// 	}
//
// 	// Create new bot
// 	b, err := telebot.NewBot(telebot.Settings{
// 		Token:   "3424234:uh54h5jk4hk",
// 		HookURL: &telebot.WebHook{Path: "/test"},
// 	})
//
// 	res, err := InlineUnsubscribeCourses(ctx, b, 1)
// 	if err != nil {
// 		t.Fatal(err)
// 	}
//
// 	if len(res) != 3 {
// 		t.Fatal("Expected lenght 3 but got", len(res))
// 	}
// }
