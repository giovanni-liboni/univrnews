package main

import (
	"strconv"

	"github.com/sirupsen/logrus"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"

	// tb "gitlab.com/giovanni-liboni/univrnews/pkg/telebot"
	tb "gopkg.in/tucnak/telebot.v2"
)

var InlineHomeBts, InlineDepartmentsBts [][]tb.InlineButton

var InlineCoursesBts map[int64][][]tb.InlineButton

var InlineUnsubCourses map[int64][]tb.InlineButton

var inlineBtnYes, inlineBtnNo tb.InlineButton

func (be *Backend) InitInlineButtons() {
	logger := log.WithFields(logrus.Fields{
		"method": "InitInlineButtons",
	})
	// We have to init all inline buttons
	InlineHomeBts = be.InlineHome()
	InlineDepartmentsBts, _ = be.InlineDepartments()

	departments, err := be.ds.GetAllDepartments()
	if err != nil {
		logger.WithError(err).Errorln("InitInlineButtons")
		return
	}
	// Map [CourseID]InlineButton
	InlineUnsubCourses = make(map[int64][]tb.InlineButton)

	// Map [DepartmentID]Array of Courses
	InlineCoursesBts = make(map[int64][][]tb.InlineButton)

	for _, dep := range departments {
		InlineCoursesBts[dep.ID], _ = be.InlineCourses(dep.ID)

		// log.WithFields(logrus.Fields{
		// 	"depName": dep.Name,
		// 	"depID":   dep.ID,
		// }).Debugln("Department with ID")

		// Build Unsubscribe buttons
		courses, err := dep.GetCourses(be.ds)
		if err != nil {
			logger.WithError(err).Errorln("InitInlineButtons GetCourses")
			return
		}
		for _, c := range courses {
			tmp := tb.InlineButton{
				Unique: "Unsubscribe_" + strconv.FormatInt(c.ID, 10),
				Text:   c.Name,
				Data:   strconv.FormatInt(c.ID, 10),
			}
			InlineUnsubCourses[c.ID] = []tb.InlineButton{tmp}
			be.b.Handle(&tmp, be.TGUnsubscribe)
		}
	}

	inlineBtnYes = tb.InlineButton{
		Unique: "subscribe",
		Text:   "SI",
		Data:   "",
	}
	be.b.Handle(&inlineBtnYes, be.TGSubscribe)

	inlineBtnNo = tb.InlineButton{
		Unique: "cancelYesOrNo",
		Text:   "🔙  Indietro",
		Data:   "",
	}
	be.b.Handle(&inlineBtnNo, be.TGSubscribe)

	inlineBtnNo = tb.InlineButton{
		Unique: "homepage",
		Text:   "🔙  Vai al menù principale",
		Data:   "",
	}
	be.b.Handle(&inlineBtnNo, be.TGStart)
}

// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
func (be *Backend) InlineHome() [][]tb.InlineButton {
	inlineKeys := [][]tb.InlineButton{}
	inlineBtnStart := tb.InlineButton{
		Unique: "startChoosing",
		Text:   "Scegli il tuo corso",
		Data:   "1",
	}
	be.b.Handle(&inlineBtnStart, be.TGHome)

	inlineBtnUnsub := tb.InlineButton{
		Unique: "cancelSubscription",
		Text:   "Non ricevere più gli avvisi di un corso",
		Data:   "2",
	}
	be.b.Handle(&inlineBtnUnsub, be.TGHome)
	inlineBtnSupp := tb.InlineButton{
		Unique: "support",
		Text:   "Support",
		Data:   "3",
	}
	be.b.Handle(&inlineBtnSupp, be.TGSupport)
	inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtnStart})
	inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtnUnsub})
	inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtnSupp})

	return inlineKeys
}

func (be *Backend) InlineDepartments() ([][]tb.InlineButton, error) {
	logger := log.WithFields(logrus.Fields{
		"method": "InlineDepartments",
	})
	i := 0
	inlineKeys := [][]tb.InlineButton{}

	// departments, err := gae.GetDepartments(ctx)
	departments, err := be.ds.GetAllDepartments()
	if err != nil {
		logger.WithError(err).Errorln("Inline Departments datastore")
		return inlineKeys, err
	}
	for _, dep := range departments {
		inlineBtn := tb.InlineButton{
			Unique: strconv.FormatInt(dep.ID, 10),
			Text:   dep.Name,
			Data:   strconv.FormatInt(dep.ID, 10),
		}
		be.b.Handle(&inlineBtn, be.TGSubscribeCourseFromDepartment)
		inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtn})
		i++
	}

	inlineBtn := tb.InlineButton{
		Unique: "cancelUnsubscribe",
		Text:   "🔙  Indietro",
		Data:   "0",
	}
	be.b.Handle(&inlineBtn, be.TGHomeCallback)
	inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtn})

	return inlineKeys, nil
}

// InlineCourses NOTE: In Data field there are 2 ids: the first one is
// the course and the second one is the department
func (be *Backend) InlineCourses(departmentID int64) ([][]tb.InlineButton, error) {
	logger := log.WithFields(logrus.Fields{
		"method": "InlineCourses",
	})

	inlineKeys := [][]tb.InlineButton{}
	dep := googlecloud.Department{
		ID: departmentID,
	}
	courses, err := dep.GetCourses(be.ds)
	if err != nil {
		logger.WithError(err).Errorln("Inline Courses datastore")
		return inlineKeys, err
	}
	for _, course := range courses {
		inlineBtn := tb.InlineButton{
			Unique: strconv.FormatInt(course.ID, 10),
			Text:   course.Name,
			Data:   strconv.FormatInt(course.ID, 10) + "," + strconv.FormatInt(departmentID, 10),
		}
		be.b.Handle(&inlineBtn, be.TGSubscribeCourse)
		inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtn})
	}

	inlineBtn := tb.InlineButton{
		Unique: "cancelCourses",
		Text:   "🔙  Indietro",
		Data:   "0," + strconv.FormatInt(departmentID, 10),
	}
	be.b.Handle(&inlineBtn, be.TGSubscribeCourse)
	inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtn})

	return inlineKeys, nil
}

func InlineSubscribeOrBack(courseID int, departmentID int) ([][]tb.InlineButton, error) {
	inlineKeys := [][]tb.InlineButton{}

	inlineBtnYes.Data = "1," + strconv.Itoa(courseID)
	inlineBtnNo.Data = "0," + strconv.Itoa(departmentID)

	inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtnYes, inlineBtnNo})

	return inlineKeys, nil
}

func (be *Backend) InlineUnsubscribeCourses(chatID int64) ([][]tb.InlineButton, error) {
	inlineKeys := [][]tb.InlineButton{}
	chat := googlecloud.Chat{
		ID: chatID,
	}
	// Retrieve courses
	courses, err := chat.GetSubscribedCourses(be.ds)
	if err != nil {
		return inlineKeys, err
	}
	for _, c := range courses {
		// inlineBtn := tb.InlineButton{
		// 	Unique: "unsubscribeCourses",
		// 	Text:   c.Name,
		// 	Data:   strconv.FormatInt(c.ID, 10),
		// }
		// inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtn})
		inlineKeys = append(inlineKeys, InlineUnsubCourses[c.ID])
	}
	inlineBtn := tb.InlineButton{
		Unique: "cancelUnsubscribe",
		Text:   "🔙  Indietro",
		Data:   "0",
	}
	be.b.Handle(&inlineBtn, be.TGHomeCallback)
	inlineKeys = append(inlineKeys, []tb.InlineButton{inlineBtn})

	return inlineKeys, nil
}
