package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"strings"

	"gitlab.com/giovanni-liboni/univrnews/pkg/crawlers"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
)

var depID = map[string]int64{
	// "www.medicina.univr.it":          43,
	"www.scienzeingegneria.univr.it": 30,
	// "www.dsg.univr.it":               31,
	// "www.dsnm.univr.it":              32,
	// "www.dtesis.univr.it":            33,
	// "www.dbt.univr.it":               34,
	// "www.dc.univr.it":                35,
	// "www.dea.univr.it":               36,
	// "www.dfpp.univr.it":              37,
	// "www.dlls.univr.it":              38,
	// "www.dspmc.univr.it":             39,
	// "www.dse.univr.it":               41,
	// "www.dfll.univr.it":              44,
}

func main() {
	// var ds googlecloud.DatastoreGC
	var deps []googlecloud.Department

	log.Println("Init Datastore Client...")
	// err := ds.InitializeDSOnHeroku()
	// if err != nil {
	// 	log.Fatalln(err)
	// }
	log.Println("Retrieving deps...")
	log.Println()
	res := crawlers.RetrieveDep()
	for i := range res {
		crawlers.RetrieveCourses(&res[i])
		if strings.Contains(res[i].Name, "Scienze Motorie") {
			res[i].ID = 200
			res[i].URL = "www.dnbm.univr.it"
		} else {
			res[i].ID = depID[res[i].URL]
		}
		crawlers.SetRSS(&res[i])
		crawlers.SetNoticesURL(&res[i])
		log.Println("Department :", res[i].Name)
		log.Println("URL        :", res[i].URL)
		log.Println("RSS        :", res[i].RSSLink)
		log.Println("NoticesURL :", res[i].NoticesURL)
		log.Println("CrawlingURL:", res[i].CrawlingURL)
		log.Println("ID         :", res[i].ID)
		// err = d.Save(&ds)
		// if err != nil {
		// 	log.Fatalln(err)
		// }
		log.Println("Getting courses...")
		crawlers.RetrieveCourses(&res[i])
		for _, c := range res[i].Courses {
			crawlers.RetrieveSingleCourse(c)
			log.Println("\tName ", c.Name)
			log.Println("\tID   ", c.ID)
			log.Println("\tURL  ", c.NoticesURL)
			log.Println("\tCurl ", c.CrawlingURL)
			log.Println()
			c.SetDepartment(&res[i])
			// c.Save(ds.ClientDS)
			err := res[i].AddCourse(c)
			if err != nil {
				log.Println(err)
			}
		}
		// err = d.Save(&ds)
		// if err != nil {
		// 	log.Fatalln(err)
		// }
		deps = append(deps, res[i])
	}
	jsonDep, err := json.Marshal(deps)
	if err != nil {
		log.Fatalln(err)
	}
	err = ioutil.WriteFile("output.json", jsonDep, 0600)
	if err != nil {
		log.Fatalln(err)
	}
}
