package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
)

/*
  go run importer <path-to-exported-json-file>
*/
func main() {

	var sdep string
	flag.StringVar(&sdep, "dep", "", "Path to the JSON exported Departmets (with Courses)")
	flag.Parse()

	ds, err := googlecloud.NewDS()
	if err != nil {
		log.Fatalln("googlecloud.NewDS :", err)
	}

	if sdep != "" {
		fmt.Print("=> Importing Departments...")
		file, err := ioutil.ReadFile(sdep)
		if err != nil {
			log.Fatalln("ioutil.ReadFile :", err)
		}
		var deps []googlecloud.Department
		json.Unmarshal(file, &deps)

		for _, dep := range deps {
			dep := dep
			dep.SetKey()
			dep.Save(ds)
			for _, c := range dep.Courses {
				c.SetDepartment(&dep)
				dep.AddCourse(c)
				c.Save(ds.ClientDS)
			}
			dep.Save(ds)
		}
		fmt.Println("Done!")
	}

}
