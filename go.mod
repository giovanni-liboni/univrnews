module gitlab.com/giovanni-liboni/univrnews

go 1.12

require (
	4d63.com/gochecknoglobals v0.0.0-20210416044342-fb0abda3d9aa // indirect
	cloud.google.com/go/datastore v1.1.0
	cloud.google.com/go/logging v1.1.0
	cloud.google.com/go/pubsub v1.3.1
	github.com/Jeffail/gabs v1.4.0
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/antchfx/htmlquery v1.2.2 // indirect
	github.com/antchfx/xmlquery v1.2.3 // indirect
	github.com/antchfx/xpath v1.1.4 // indirect
	github.com/araddon/dateparse v0.0.0-20201001162425-8aadafed4dc4
	github.com/bgentry/que-go v1.0.1
	github.com/blend/go-sdk v1.1.1 // indirect
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/facebookgo/ensure v0.0.0-20200202191622-63f1cf65ac4c // indirect
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/subset v0.0.0-20200203212716-c811ad88dec4 // indirect
	github.com/fatih/color v1.9.0 // indirect
	github.com/garyburd/redigo v1.6.2 // indirect
	github.com/gemnasium/logrus-graylog-hook/v3 v3.0.2
	github.com/gobuffalo/envy v1.9.0 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/gocolly/colly v1.2.0
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/gorilla/mux v1.7.4
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jmoiron/sqlx v1.2.0
	github.com/juju/ratelimit v1.0.1
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/lib/pq v1.3.0
	github.com/mailgun/mailgun-go v2.0.0+incompatible
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/olekukonko/tablewriter v0.0.4
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/panjf2000/ants/v2 v2.4.2
	github.com/pkg/errors v0.9.1
	github.com/r3labs/diff/v2 v2.14.0
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/sirupsen/logrus v1.6.0
	github.com/stripe/safesql v0.2.0 // indirect
	github.com/tdewolff/minify v2.3.6+incompatible
	github.com/tdewolff/parse v2.3.4+incompatible // indirect
	github.com/tdewolff/test v1.0.6 // indirect
	github.com/temoto/robotstxt v1.1.1 // indirect
	github.com/tsenart/deadcode v0.0.0-20160724212837-210d2dc333e9 // indirect
	github.com/wcharczuk/go-chart v2.0.1+incompatible
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	google.golang.org/api v0.30.0
	google.golang.org/appengine v1.6.6
	gopkg.in/bsm/ratelimit.v1 v1.0.0-20160220154919-db14e161995a // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/cheggaaa/pb.v1 v1.0.28
	gopkg.in/redis.v4 v4.2.4
	gopkg.in/tucnak/telebot.v2 v2.0.0-20200301001213-9852df39ae6c
	gopkg.in/yaml.v2 v2.2.8 // indirect
	mvdan.cc/unparam v0.0.0-20211002134041-24922b6997ca // indirect
)
