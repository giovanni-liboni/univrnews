<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Pipeline][pipeline-shield]][pipeline-url] -->
[![MIT License][license-shield]][license-url]

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/giovanni-liboni/univrnews">
    <img src="doc/images/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">UnivrNews Telegram Bot</h3>

  <p align="center">
    A fast and reliable telegram bot to scrape and notify news from Univr website
    <br />
    <br />
    <a href="https://gitlab.com/giovanni-liboni/univrnews/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/giovanni-liboni/univrnews/-/issues">Request Feature</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
      </ul>
    </li>
    <li><a href="#development">Development</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

The UnivrNews Bot is a Telegram Bot to retrieve and notify the latest news of the University of Verona. Currently, it handles more than 11000 subscribers. A student can choose her/his course(s) and s/he will be notified as soon as news is published.

According to the latest definition of *students*, they have no money. So, my first goal was to run this bot for free. It is composed of 5 Heroku applications running on the free tier offer. For redundancy: three of them are in charge to retrieve the news using different techniques, one is in charge to handle the Telegram Bot backend (subscription,..), and the last one is in charge to dispatch the news to the subscribers.

The sensible structures are hosted by the Google Cloud Platform (for free, of course ;) ) using the Free Tiers. In particular, as a database, it is based on the Datastore. As queues system, it is based on the Pub/Sub service. The logging is handled using a Graylog server.

The project started as a simple notification for a couple of friends, they were inpatients to receive their notes, and every 30 seconds, they refreshed the web page with the news (and, somewhere in the future, their notes). The first version was hosted on Heroku, but the limits imposed by Heroku did not permit to scale of the application when it reached 5000 subscribers. So, I created this distributed system as a learning project on a distributed system, Google Cloud, Continuous Integration (with Gitlab), Redis, Google Datastore, PostgreSQL, Graylog, and more.

<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

1. Install Golang
  - Ubuntu : Install the latest version using `snap`
    ```sh
      sudo snap install go --classic
    ```
  - OsX: Install the latest version using `brew` or download the package from the offical website

2. Set up the `GOPATH` variable

<!-- USAGE EXAMPLES -->
## Development

The latest version was tested with Go 1.12 with modules enabled. Depending to the `cmd` to develop, there are several environment varibles to set. Each `cmd` and `pkg` has its own variables to set specified in the file `env.sh`.

### Description

<div align="center">
  <img src="doc/images/UnivrNews_architecture.png" alt="Architecture overview" width="600" height="600">
</div>

This is a general schema for the dataflow.
1. Feedcrawler (hosted by Heroku) retrieves all the news from the RSS feeds and then push them to the Google Pub/Sub system. The departments are store in the Google Datastore.
2. Google Pub/Sub pushes the message to the Dispatcher (hosted by Heroku). The Dispatcher checks the news, retrieves from the Datastore all the subscriptions and sent the news to the users.

### Logging

The Logging activities are handled using Graylog as collector and viewer. Due to the multi-applications struture, each application sends its own logs to a single Graylog server.

### CI

`gitlab.com` CI is used to continuous integrate the new versions and release them as soon as possible, avoiding the manual deployment of each Heroku applications.


### Testing

In order to run the tests for the project, it is necessary to install `gitlab-runner` and `docker`.
`Docker` is used to test the project without the need to install any additional package (Google Datastore, Google Pub/Sub, ...). Install `Docker` and `docker-compose` to use the `docker-compose.yml` file containing all the packages needed to run tests.

```sh
  cd test
  docker-compose up
  source test.env
  cd ../cmd/importer-datastore
  go run importer-datastore.go -dep ../../test/deps.json
```

```sh
  gitlab-runner exec docker test --docker-pull-policy="if-not-present" \\
-env DATASTORE_EMULATOR_HOST=datastore:8081 \\
-env PUBSUB_EMULATOR_HOST=pubsub:8085 \\
-env DATASTORE_LISTEN_ADDRESS="0.0.0.0:8081" \\
-env PUBSUB_LISTEN_ADDRESS="0.0.0.0:8085" \\
-env PUBSUB_PROJECT_ID="p-test" \\
-env DATASTORE_PROJECT_ID="p-test" \\
-env POSTGRES_DB=test \\
-env POSTGRES_USER=test \\
-env POSTGRES_PASSWORD="test" \\
-env DATABASE_URL="postgres://test:test@postgres/test?sslmode=disable" \\
-env PGPASSWORD=test \\
-env TELEBOT_SECRET_TEST="<TELEBOT-SECRET-HERE>" \\
-env TELEGRAM_USER_ID_TEST="TELEGRAM-USER-ID-HERE"
```

<!-- ROADMAP -->
<!-- ## Roadmap

See the [open issues](https://github.com/othneildrew/Best-README-Template/issues) for a list of proposed features (and known issues). -->



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.


<!-- CONTACT -->
## Contact

Giovanni Liboni - giovanni@liboni.me

Project Link: [https://gitlab.com/giovanni-liboni/univrnews](https://gitlab.com/giovanni-liboni/univrnews)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Img Shields](https://shields.io)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[pipeline-shield]: https://img.shields.io/gitlab/pipeline/giovanni-liboni/univrnews/master?style=for-the-badge
[pipeline-url]: https://gitlab.com/giovanni-liboni/univrnews/-/pipelines
[tag-shield]: https://img.shields.io/gitlab/v/tag/giovanni-liboni/univrnews?sort=date&style=for-the-badge
[tag-url]: https://gitlab.com/giovanni-liboni/univrnews/-/tags
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://gitlab.com/giovanni-liboni/univrnews/-/graphs/master
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/badge/license-MIT-blue.svg?style=for-the-badge
[license-url]: https://gitlab.com/giovanni-liboni/univrnews/-/blob/master/LICENSE
[product-screenshot]: images/screenshot.png
