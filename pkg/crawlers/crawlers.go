package crawlers

import (
	"errors"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode"

	"gitlab.com/giovanni-liboni/univrnews/pkg/utils"

	"github.com/gocolly/colly"
	log "github.com/sirupsen/logrus"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
	news "gitlab.com/giovanni-liboni/univrnews/pkg/news"

	_ "gitlab.com/giovanni-liboni/univrnews/pkg/utils"
)

// RetrivesNews gets the news for the specified course and return the last maxNews news
// Scraper method based on https://www.di.univr.it/?ent=avviso which customized a POST request to retrieve only the
// news for the specified course
func RetrievesNews(department *googlecloud.Department, course *googlecloud.Course, maxNews int) (res []news.News, err error) {
	var pubtime, modtime, oggetto int

	if &department == nil || department.ID == 0 {
		return res, errors.New("Must specified at least a valid department")
	}

	// Build the POST Request
	// -> Parse the Notices URL (example: www.scienzeingegneria.univr.it/?ent=avvisoin&cs=386)
	host, err := url.Parse(utils.NormalizeURL(department.NoticesURL))
	if err != nil {
		return res, err
	}
	// Check if the scheme is http, otherwise set it
	if strings.Compare(host.Scheme, "http") != 0 && strings.Compare(host.Scheme, "https") != 0 {
		host.Scheme = "http"
	}

	departmentNewsCollector := colly.NewCollector()
	departmentNewsCollector.UserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41"

	// Get maxNews news from the the main page
	departmentNewsCollector.OnHTML("tr", func(e *colly.HTMLElement) {
		if e.Index == 0 {
			e.ForEach("th", func(ii int, el *colly.HTMLElement) {
				if strings.Contains(el.Text, "Data") {
					pubtime = ii
				} else if strings.Contains(el.Text, "modifica") {
					modtime = ii
				} else if strings.Contains(el.Text, "Oggetto") {
					oggetto = ii
				}
			})
		}
		// Skip index 0 because it contains only the titles
		if e.Index == 0 || e.Index > maxNews {
			return
		}
		var it news.News
		var err error
		e.ForEach("td", func(ii int, el *colly.HTMLElement) {
			switch ii {
				case pubtime:
					dataString := strings.ReplaceAll(el.Text, "\n", "")
					dataString = removeExtraSpaces(dataString)
					it.PubTime, err = utils.GetDate(dataString)
					if err != nil {
						log.WithError(err).WithField("text", el.Text).Errorln("Error retrieving pubTime")
					}
				case oggetto:
					it.Title = strings.TrimSpace(el.Text)
					if strings.Compare(it.Title, "") == 0 {
						// Try with the text field for the child a
						it.Title = el.ChildText("a")
					}
				case modtime:
					dataString := strings.ReplaceAll(el.Text, "\n", "")
					dataString = removeExtraSpaces(dataString)
					it.ModTime, err = utils.GetDate(dataString)
					if err != nil && strings.Compare(err.Error(), "the length of the string must be > 0") != 0 {
						log.WithError(err).WithField("text", el.Text).Errorln("Error retrieving pubTime")
					}
			}

			it.Link, err = url.Parse(utils.NormalizeURL(host.Hostname() + e.ChildAttr("a", "href")))

			if err != nil {
				log.Fatalln(err)
			}
		})

		// Set the news ID from the URL
		err = it.SetIDFromURL(it.Link.String())
		if err != nil {
			log.WithError(err).Errorln("SetIDFromURL")
		}

		res = append(res, it)
	})

	// Crawler for the medicina.univr.it website (different from the others)
	departmentNewsCollector.OnHTML(".CardAvvisi", func(element *colly.HTMLElement) {
		if len(res) == 0 {
			var avvisi []news.News
			element.ForEach("a[href]", func(i int, e *colly.HTMLElement) {
				avvisoURL, err := url.Parse(utils.NormalizeURL(host.Hostname() + "/" + e.Attr("href")))
				if err != nil {
					log.WithError(err).Errorln("onHTML for cardAvvisi")
				}

				avvisoID, err := strconv.Atoi(avvisoURL.Query().Get("id"))
				if err != nil {
					log.WithError(err).Errorln("onHTML for cardAvvisi")
				}

				avvisi = append(avvisi, news.News{
					Link: avvisoURL,
					ID: avvisoID,
				})
			})
			res = append(res, avvisi[:maxNews]...)
			for _, item := range res {
				log.Println("Retrieving single news for", item.Link.String())
				err := item.GetContentFromURL()
				if err != nil {
					log.WithError(err).Errorln("onHTML Medicina getContentFromURL")
				}
			}
		}
	})

	csMittente := "0"
	// Set the course ID if specified, otherwise search for all the news
	if &course == nil || course.ID != 0 {
		csMittente = strconv.FormatInt(course.ID, 10)
	}

	data := url.Values{
		"csMittente": {csMittente},
		"datai":  {time.Now().Format("02/01/2006")}, // Required to execute the POST request
		"dataf" : {time.Now().AddDate(5,0,0).Format("02/01/2006")}, // Required to execute the POST request
		//"testo" : {""},
		//"personeMittente" : {"0"},
		//"oiMittente" : {"0"},
		//"struttureMittente" : {"0"},
		//"uoMittente" : {"0"},
		//"biblioCRMittente" : {"0"},
		"dest" : { host.Query().Get("dest") },
	}
	err = departmentNewsCollector.Request("POST", host.String(), strings.NewReader(data.Encode()), nil, nil)
	return res, err
}

// RetrieveDep returns a list with all the departments/schools
func RetrieveDep() []googlecloud.Department {

	var deps []googlecloud.Department

	mainSite := colly.NewCollector()
	mainSite.UserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41"

	mainSite.OnHTML("div", func(e *colly.HTMLElement) {
		if e.Attr("class") == "articolo-desc" {
			var d googlecloud.Department
			d.Name = e.ChildText("a")
			d.CrawlingURL = e.ChildAttr("a", "href")
			deps = append(deps, d)
			// departmentInMainSite.Visit(e.ChildAttr("a", "href"))
		}
	})
	err := mainSite.Visit("http://www.univr.it/it/corsi-di-studio")
	if err != nil {
		log.WithError(err).Errorln("visit mainSite @ http://www.univr.it/it/corsi-di-studio")
	}
	return deps
}

func RetrieveCourses(d *googlecloud.Department) {

	var cs map[string]string = make(map[string]string)
	departmentInMainSite := colly.NewCollector()
	departmentInMainSite.UserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41"

	coursesInMainSite := colly.NewCollector()
	coursesInMainSite.UserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41"

	singleCourseInMainSiste := colly.NewCollector()
	singleCourseInMainSiste.UserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41"

	departmentInMainSite.OnHTML("li", func(e *colly.HTMLElement) {
		// Selezione le tipologie di corso (corso di laurea, magistrale, ..)
		if strings.Contains(e.Attr("class"), "ui-corner-top") {
			// log.Println(e.ChildAttr("a", "href"))
			err := coursesInMainSite.Visit(e.ChildAttr("a", "href"))
			if err != nil {
				log.WithError(err).Errorln("coursesInMainSite @", e.ChildAttr("a", "href"))
			}
		}
	})

	coursesInMainSite.OnHTML("div", func(e *colly.HTMLElement) {
		if e.Attr("class") == "item-corso" {
			err := singleCourseInMainSiste.Visit(e.ChildAttr("a", "href"))
			if err != nil {
				log.WithError(err).Errorln("coursesInMainSite @", e.ChildAttr("a", "href"))
			}
		}
	})

	singleCourseInMainSiste.OnHTML("div", func(e *colly.HTMLElement) {
		// Gestisco le sedi
		if strings.Contains(e.Attr("class"), "tabs") {
			e.ForEach("li", func(ii int, el *colly.HTMLElement) {
				for _, site := range e.ChildAttrs("a", "href") {
					err := singleCourseInMainSiste.Visit(site)
					if err != nil {
						log.WithError(err).Errorln("singleCourseInMainSiste @", site)
					}
				}
			})
		}
		if strings.Contains(e.Attr("class"), "corso-dettaglio") {
			cs[e.ChildAttr("a[title]", "href")] = e.ChildAttr("a[title]", "href")
		}
	})

	err := departmentInMainSite.Visit(utils.NormalizeURL(d.CrawlingURL))
	if err != nil {
		log.WithError(err).Errorln("departmentInMainSite @", utils.NormalizeURL(d.CrawlingURL))
	}

	// Create the courses
	for _, value := range cs {
		var c googlecloud.Course
		c.CrawlingURL = value
		d.Courses = append(d.Courses, &c)

		// Set the URL for the notices
		host, err := url.Parse(c.CrawlingURL)
		if err != nil {
			log.Fatalln(err)
		}
		d.URL = host.Hostname()
	}

}

func RetrieveSingleCourse(c *googlecloud.Course) {
	colly.UserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41")
	courseMainPage := colly.NewCollector()
	courseMainPage.OnHTML("h1", func(e *colly.HTMLElement) {
		c.Name = e.Text
	})
	err := courseMainPage.Visit(c.CrawlingURL)
	if err != nil {
		log.WithError(err).Errorln("courseMainPage @", c.CrawlingURL)
	}

	// Set the URL for the notices
	host, err := url.Parse(c.CrawlingURL)
	if err != nil {
		log.Fatalln(err)
	}
	id := host.Query().Get("cs")
	c.NoticesURL = utils.NormalizeURL(host.Hostname() + "/?ent=avvisoin&cs=" + id)
	c.ID, _ = strconv.ParseInt(id, 10, 64)
}

func SetRSS(d *googlecloud.Department) {
	colly.UserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41")
	depAvvisiPage := colly.NewCollector()
	depAvvisiPage.OnHTML("p", func(e *colly.HTMLElement) {
		if strings.Contains(e.Attr("class"), "rss") {
			d.RSSLink = utils.NormalizeURL(d.URL + e.ChildAttr("a", "href"))
			if d.ID == 0 {
				m, _ := url.ParseQuery(d.RSSLink)
				if val, ok := m["dest"]; ok {
					d.ID, _ = strconv.ParseInt(val[0], 10, 64)
				}
			}
		}
	})
	err := depAvvisiPage.Visit(utils.NormalizeURL(d.URL + "/?ent=avviso"))
	if err != nil {
		log.WithError(err).Errorln("depAvvisiPage @", utils.NormalizeURL(d.URL+"/?ent=avviso"))
	}
}

func SetNoticesURL(d *googlecloud.Department) {
	colly.UserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41")
	depPage := colly.NewCollector()
	depPage.OnHTML("a", func(e *colly.HTMLElement) {
		if strings.Contains(e.Attr("href"), "avviso") {
			d.NoticesURL = utils.NormalizeURL(d.URL + e.Attr("href"))
		}
	})
	err := depPage.Visit(utils.NormalizeURL(d.URL))
	if err != nil {
		log.WithError(err).Errorln("depPage @", utils.NormalizeURL(d.URL))
	}
}

func SetDegreeIDs(ds *googlecloud.DatastoreGC, n *news.News) error {
	// Retrieve the department
	var err error
	var dep googlecloud.Department
	var courses []*googlecloud.Course

	if n.Department.ID == 0 {
		dep := googlecloud.Department{
			URL: n.Link.Hostname(),
		}
		err = dep.Load(ds)
		if err != nil {
			log.Errorln("SetIDsCourses.dep.Load :", err)
			return err
		}
		courses, err = dep.GetCourses(ds)
	} else {
		dep = n.Department
		courses, err = n.Department.GetCourses(ds)
	}

	if err != nil {
		log.Errorln("SetIDsCourses.GetCourses :", err)
		return err
	}
	for _, c := range courses {
		newsPerCourse, err := RetrievesNews(&dep, c, 10)
		if err != nil {
			log.Errorln("SetIDsCourses.RetrievesNews :", err)
			return err
		}
		// Retrives all the IDS for the news
		for _, npc := range newsPerCourse {
			if npc.ID == n.ID {
				// We found the course!
				n.DegreeIds = append(n.DegreeIds, int(c.ID))
				n.Courses = append(n.Courses, *c)
				break
			}
		}
	}
	return nil
}

// -----------------------------------------------------------------------------
// Some useful methods
// -----------------------------------------------------------------------------

// SpaceMap remove unwanted symbol inside a string
func SpaceMap(str string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		if unicode.IsSymbol(r) {
			return -1
		}
		if strings.IndexRune(".!$", r) == 0 {
			return -1
		}
		return r
	}, str)
}

func removeExtraSpaces(s string) string {
	reLeadcloseWhtsp := regexp.MustCompile(`^[\s\p{Zs}]+|[\s\p{Zs}]+$`)
	reInsideWhtsp := regexp.MustCompile(`[\s\p{Zs}]{2,}`)
	final := reLeadcloseWhtsp.ReplaceAllString(s, "")
	final = reInsideWhtsp.ReplaceAllString(final, " ")
	return final
}

func HtmlBRDivisorTOArray(html string) (res []string) {
	res = strings.Split(html, "<br/>")
	for i, tmp := range res {
		res[i] = removeExtraSpaces(tmp)
	}
	return res
}

