package news

import (
	"encoding/json"
	"errors"
	"github.com/gocolly/colly"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode"

	"gitlab.com/giovanni-liboni/univrnews/pkg/utils"

	"github.com/PuerkitoBio/goquery"
	log "github.com/sirupsen/logrus"
	"gitlab.com/giovanni-liboni/univrnews/pkg/googlecloud"
	rss "gitlab.com/giovanni-liboni/univrnews/pkg/rss"
)

// Attachment file to the news
type Attachment struct {
	Title   string
	Link    string
	Preview string
}

// News represents single news
type News struct {
	ID          int
	Title       string
	Description string
	Content     string
	Link        *url.URL
	Attachments []Attachment
	DipURL      string
	Author      string
	PubTime     time.Time // Pubblication time
	ModTime     time.Time // Modification time
	Courses     []googlecloud.Course
	DegreeIds   []int // Lauree a cui e' rivolto l'avviso
	CreatedAt   time.Time
	Department  googlecloud.Department // Dipartimento a cui appartiene l'avviso
}

// =============================================================================
// Parse functions
// =============================================================================

// Parse function to parse rss item passes as argument
func Parse(rssitem *rss.Item) (*News, error) {
	var err error
	news := new(News)

	// Pubblication date
	news.PubTime, err = rssitem.ParsedPubDate()
	if err != nil {
		return nil, err
	}

	// Title
	news.Title = rssitem.Title

	// Description
	news.Description = rssitem.Description

	// Link
	news.Link, err = url.Parse(rssitem.Links[0].Href)
	if err != nil {
		return nil, err
	}

	// Retrive news ID
	m, _ := url.ParseQuery(news.Link.RawQuery)
	news.ID, _ = strconv.Atoi(m.Get("id"))

	// Retrive other infos
	err = news.GetContentFromURL()
	if err != nil {
		return nil, err
	}

	// Return the new obj
	return news, nil
}

// ParseFromLink parse a new from a direct link
func ParseFromLink(link *url.URL) (*News, error) {
	news := new(News)
	news.Link = link

	// Retrive news ID
	m, _ := url.ParseQuery(news.Link.RawQuery)
	news.ID, _ = strconv.Atoi(m.Get("id"))

	// Retrive other infos
	err := news.GetContentFromURL()
	if err != nil {
		return nil, err
	}

	return news, nil
}

// CompleteParse retrive all the informations
func (item *News) CompleteParse() error {
	// Get all IDs courses
	err := item.SetIDsCourses()
	if err != nil {
		return err
	}
	return nil
}

func (item *News) EqualsTo(other *News) bool {
	return item.ID == other.ID && other.ModTime.UTC().Equal(item.ModTime.UTC()) && other.PubTime.UTC().Equal(item.PubTime.UTC())
}

// =============================================================================
// Secondary functions (IsNew)
// =============================================================================

// IsNew Return true if the news was sent BEFORE activation date
func (item *News) IsNew(activationTime time.Time) bool {
	return activationTime.UTC().Before(item.PubTime.UTC()) || activationTime.UTC().Equal(item.PubTime.UTC()) || activationTime.UTC().Before(item.ModTime.UTC()) || activationTime.UTC().Equal(item.ModTime.UTC())
}

// GetContentFromURL builds content and files attached to the news
func (item *News) GetContentFromURL() (err error) {
	return item.crawlerNewsPage()
}

// SetIDsCourses sets id courses
func (item *News) SetIDsCourses() error {
	var newsPageList []string
	var err error
	// Get all news pages
	if strings.Contains(item.Link.Host, "medicina") {
		newsPageList, err = getNewsPagesFromHostMedicina(item.Link.Host)
	} else {
		newsPageList, err = getNewsPagesFromHost(item.Link.Host)
	}
	if err != nil {
		return err
	}
	for _, val := range newsPageList {
		//Recupero gli ultimi 5 avvisi da ogni corso e vedo dove e' presente
		ids, err := RetriveLast5NewsIDsFromNewsPage(val)
		if err != nil {
			return err
		}
		if contains(ids, item.ID) {
			item.DegreeIds = append(item.DegreeIds, getIDFromCompleteURL(val))
		}
	}

	return nil
}

// SetIDFromURL set the news's ID from the direct link
func (item *News) SetIDFromURL(url string) error {
	if strings.Contains(url, "avviso") {
		if strings.Contains(url, "univr.it") {
			// host gia' presente
			item.ID = getIDFromCompleteURL(url)
		} else {
			item.ID = getIDFromCompleteURL("wwww.example.com" + url)
		}
	} else {
		return errors.New("No valid url")
	}
	return nil
}

func (item *News) String() string {
	enc, _ := json.Marshal(item)
	return string(enc)
}

// =============================================================================
// HTML utils functions
// =============================================================================

func getNewsPagesFromHost(host string) ([]string, error) {
	var res []string
	coursesType := []string{
		"N",
		"MA",
		"mu",
		"SP",
		"R",
		"F",
		"T",
	}
	for _, courseType := range coursesType {
		tmpRes, err := NewsPageLinksFromURLCorso(host + "/?ent=cs&tcs=" + courseType)
		if err != nil {
			return nil, err
		}
		res = append(res, tmpRes...)
	}
	return res, nil
}

// getNewsPagesFromHostMedicina recupera i link delle pagine
func getNewsPagesFromHostMedicina(host string) ([]string, error) {
	var res []string
	coursesType := []string{
		"N",
		"MA",
		"mu",
		"SP",
		"R",
		"F",
		"T",
	}
	for _, courseType := range coursesType {
		tmpRes, err := NewsPageLinksFromURLCorsoMedicina(utils.NormalizeURL(host + "/?ent=cs&tcs=" + courseType))
		if err != nil {
			return nil, err
		}
		res = append(res, tmpRes...)
	}
	return res, nil
}

// NewsPageLinksFromURLCorso Ritorna la lista dei link alle pagine che contengono gli avvisi del
// dipartimento passato come parametro a partire dall'url che contiene
// tutte le laure del corso
func NewsPageLinksFromURLCorso(urlString string) ([]string, error) {
	var res []string

	urlString = utils.NormalizeURL(urlString)

	rootURL, err := url.Parse(urlString)
	if err != nil {
		return nil, err
	}

	doc, err := goquery.NewDocument(urlString)

	if err != nil {
		return nil, err
	}
	doc.Find("#contenutoPagina").Find("div").First().Find("dl").Find("dt").Find("a").Each(func(i int, s *goquery.Selection) {

		// Costrisco l'intero url
		idString, idBool := s.Attr("href")
		if idBool {
			id := getIDFromString(idString)
			// log.Println(rootURL.Host + "/?ent=avvisoin&cs=" + strconv.Itoa(id))
			res = append(res, rootURL.Host+"/?ent=avvisoin&cs="+strconv.Itoa(id))
		}
	})

	return res, nil
}

// NewsPageLinksFromURLCorsoMedicina retrive information from a url based on "medicina" url.
func NewsPageLinksFromURLCorsoMedicina(urlString string) ([]string, error) {
	var res []string

	urlString = utils.NormalizeURL(urlString)

	rootURL, err := url.Parse(urlString)
	if err != nil {
		return nil, err
	}

	doc, err := goquery.NewDocument(urlString)

	if err != nil {
		return nil, err
	}
	doc.Find("#centroservizi").Find("dl").Find("dt").Find("a").Each(func(i int, s *goquery.Selection) {
		// Start OLD SITE
		tokens := strings.Split(s.Text(), "(")
		var stringToTest string

		if len(tokens) > 1 {
			stringToTest = tokens[1]
		} else {
			stringToTest = tokens[0]
		}
		// Elimino tutti i corsi che non sono piu' validi
		if !strings.Contains(stringToTest, "until") {
			// Recupero l'ID
			idString, idBool := s.Attr("href")
			if idBool {
				id := getIDFromString(idString)
				res = append(res, rootURL.Host+"/?ent=avvisoin&cs="+strconv.Itoa(id))
			}
		}
		// END OLD SITE
	})

	return res, nil
}

// RetriveLast5NewsIDsFromNewsPage retrives last 5 news ids from a news page
func RetriveLast5NewsIDsFromNewsPage(newsPageURL string) ([]int, error) {
	var res []int

	if !strings.HasPrefix(newsPageURL, "http") && !strings.HasPrefix(newsPageURL, "https") {
		newsPageURL = "http://" + newsPageURL
	}

	doc, err := goquery.NewDocument(newsPageURL)
	if err != nil {
		return nil, err
	}
	if doc.Find("table").Find("tbody").Find("tr").Find("a").Size() > 5 {
		doc.Find("table").Find("tbody").Find("tr").Find("a").Slice(0, 5).Each(func(i int, s *goquery.Selection) {
			idString, idBool := s.Attr("href")
			if idBool {
				id := getIDFromString(idString)
				// log.Println(rootURL.Host + "/?ent=avvisoin&cs=" + strconv.Itoa(id))
				res = append(res, id)
			}
		})
	} else {
		doc.Find("table").Find("tbody").Find("tr").Find("a").Each(func(i int, s *goquery.Selection) {
			idString, idBool := s.Attr("href")
			if idBool {
				id := getIDFromString(idString)
				// log.Println(rootURL.Host + "/?ent=avvisoin&cs=" + strconv.Itoa(id))
				res = append(res, id)
			}
		})
	}

	return res, nil
}

// GetIDFromString retrive int ID from a string like this /?ent=avvisoin&id=432
func getIDFromString(urlString string) int {
	var id int
	// Costruisco l'url completo alla pagina
	singleURL, _ := url.Parse("www.example.com" + urlString)

	// recupero l'ID del corso
	m, _ := url.ParseQuery(singleURL.RawQuery)
	if val, ok := m["id"]; ok {
		id, _ = strconv.Atoi(val[0])
	} else if val, ok := m["cs"]; ok {
		id, _ = strconv.Atoi(val[0])
	} else {
		log.Warnln(urlString)
		id = 0
	}

	return id
}

// GetIDFromString retrive int ID from a string like this www.univr.it/?ent=avvisoin&id=432
func getIDFromCompleteURL(urlString string) int {
	var id int
	// Costruisco l'url completo alla pagina
	singleURL, _ := url.Parse(urlString)

	// recupero l'ID del corso
	m, _ := url.ParseQuery(singleURL.RawQuery)
	if val, ok := m["id"]; ok {
		id, _ = strconv.Atoi(val[0])
	} else if val, ok := m["cs"]; ok {
		id, _ = strconv.Atoi(val[0])
	} else {
		log.Warnln(urlString)
		id = 0
	}

	return id
}

// =============================================================================
// Utils functions
// =============================================================================

func contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// HtmlBRDivisorTOArray splits HTML text inside <br> into an array
func HtmlBRDivisorTOArray(html string) (res []string) {
	res = strings.Split(html, "<br/>")
	for i, tmp := range res {
		res[i] = removeExtraSpaces(tmp)
	}
	return res
}

func removeExtraSpaces(s string) string {
	ReLeadCloseWhtsp := regexp.MustCompile(`^[\s\p{Zs}]+|[\s\p{Zs}]+$`)
	ReInsideWhtsp := regexp.MustCompile(`[\s\p{Zs}]{2,}`)
	final := ReLeadCloseWhtsp.ReplaceAllString(s, "")
	final = ReInsideWhtsp.ReplaceAllString(final, " ")
	return final
}

// SpaceMap remove unwanted symbol inside a string
func SpaceMap(str string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		if unicode.IsSymbol(r) {
			return -1
		}
		if strings.IndexRune(".!$", r) == 0 {
			return -1
		}
		return r
	}, str)
}

// =============================================================================
// Crawler methods
// =============================================================================

// RetrieveSingleNews gets the content of a news from its URL. It gets:
// - ID
// - Title
// - Link
// - Publication date
// - Modification date
// - Attachments
// - Content
// - Author
func (n *News) crawlerNewsPage() (err error) {
	// Retrieve the host
	// Set the URL for the notices
	urlAddressNormalized, _ := url.Parse(utils.NormalizeURL(n.Link.String()))
	// Check if the scheme is http
	if urlAddressNormalized.Scheme != "http" && urlAddressNormalized.Scheme != "https" {
		urlAddressNormalized.Scheme = "http"
	}
	values, _ := url.ParseQuery(urlAddressNormalized.RawQuery)
	values.Set("lang", "en")
	urlAddressNormalized.RawQuery = values.Encode()
	urlAddressNormalized, _ = url.Parse(urlAddressNormalized.String())

	// Set the link into the news
	n.Link = urlAddressNormalized

	// Set the ID if any
	if n.ID == 0 {
		n.ID, err = strconv.Atoi(values.Get("id"))
		if err != nil {
			log.WithError(err).Errorln("Error during the conversion from string to int of the ID")
		}
	}

	singleNewsCollector := colly.NewCollector()
	singleNewsCollector.UserAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41"

	singleNewsCollector.OnHTML("dl[id=dettagliAvviso]", func(e *colly.HTMLElement) {
		e.ForEach("dt", func(i int, el *colly.HTMLElement) {
			// log.WithField("Field", el.Text).Infoln("dl[id=dettagliAvviso]")
			// Check which fields are present
			if strings.Contains(el.Text, "Publication date") || strings.Contains(el.Text, "Data pubblicazione") {
				// Publication date is present
				if n.PubTime.IsZero() {
					n.PubTime, err = utils.GetDate(e.ChildText("dd:nth-child(" + strconv.Itoa((i+1)*2) + ")"))
					if err != nil {
						log.WithError(err).WithField("text", el.Text).Errorln("Error retrieving ModTime")
					}
				}
			}
			if strings.Contains(el.Text, "modifica") || strings.Contains(el.Text, "Modified") {
				// Modification date is present
				if n.ModTime.IsZero() {
					n.ModTime, err = utils.GetDate(e.ChildText("dd:nth-child(" + strconv.Itoa((i+1)*2) + ")"))
					if err != nil {
						log.WithError(err).Errorln("Error retriveDataMod on", e.Text)
					}
				}
			}
			if strings.Contains(el.Text, "Pubblicato") || strings.Contains(el.Text, "Published") {
				if n.Author == "" {
					n.Author = getAuthor(e.ChildText("dd:nth-child(" + strconv.Itoa((i+1)*2) + ")"))
				}
			}
			// Add the title
			// Fix the problem (empty news, only the pubtime, modtime and author available) during the parsing of http://www.dsu.univr.it/?ent=avviso&dest=&id=133341
			if strings.Contains(el.Text, "Subject") {
				if n.Title == "" {
					n.Title = e.ChildText("dd:nth-child(" + strconv.Itoa((i+1)*2) + ")")
				}
			}
		})
	})

	// Retrieve the content removing extra spaces
	singleNewsCollector.OnHTML("div", func(e *colly.HTMLElement) {
		if e.Attr("class") == "sezione" {
			n.Content = removeExtraSpaces(e.Text)
		} else if e.Attr("class") == "main-text" {
			// First try with the text
			n.Content = removeExtraSpaces(e.Text)
			if strings.Compare(n.Content, "") == 0 {
				// then with the child
				n.Content = removeExtraSpaces(e.ChildText("span"))
			}
		}
	})

	// Retrieve the list of attachments
	singleNewsCollector.OnHTML("a[href]", func(e *colly.HTMLElement) {
		if strings.Contains(e.Attr("href"), "documenti") {
			// Link to the attachment
			attachmentURL, _ := url.ParseRequestURI(e.Attr("href"))
			attachmentURL.Host = n.Link.Host
			attachmentURL.Scheme = n.Link.Scheme
			n.Attachments = append(n.Attachments, Attachment{
				Title:   removeExtraSpaces(e.Text),
				Link:    attachmentURL.String(),
				Preview: "",
			})
		}
	})

	err = singleNewsCollector.Visit(urlAddressNormalized.String())
	if err != nil {
		log.WithError(err).Errorln("singleNewsCollector @", urlAddressNormalized.String())
	}
	return err
}

// Not the best function written
// It finds the substring between two \n
func getAuthor(s string) string {
	first, last := -1, -1
	for i, c := range s {
		// Find the two indexes
		if first < 0 && unicode.IsLetter(c) {
			first = i
		} else if first != -1 && last < 0 && c == '\n' {
			last = i
		}
	}
	if first > -1 && last > -1 {
		return s[first:last]
	}
	return ""
}