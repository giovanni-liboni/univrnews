package queue

import (
	"os"
	"sync"

	"github.com/bgentry/que-go"
	"github.com/jackc/pgx"
	"github.com/sirupsen/logrus"
)

var (
	log     = logrus.WithField("core", "queue")
	qc      *que.Client
	pgxpool *pgx.ConnPool
	once    sync.Once
)

func GetQueue() *que.Client {
	once.Do(func() {
		dbURL := os.Getenv("DATABASE_URL")
		var err error
		pgxpool, qc, err = Setup(dbURL)
		if err != nil {
			log.WithField("DATABASE_URL", dbURL).Fatal("Unable to setup queue / database: ", err)
		}
	})
	return qc
}
