package googlecloud

import (
	"testing"
)

func TestDepartmentLoad(t *testing.T) {
	var ds DatastoreGC
	err := ds.InitializeDSOnHeroku()
	if err != nil {
		t.Fatal(err)
	}
	PopulateDatastore(t, ds.ClientDS)

	var dep Department
	dep.URL = "http://test.com"
	err = dep.Load(&ds)
	if err != nil {
		t.Fatal(err)
	}
	if dep.ID != 1 {
		t.Fatal("Expected 1, got", dep.ID)
	}

	dep.ID = 0
	// Redo the test to
	err = dep.Load(&ds)
	if err != nil {
		t.Fatal(err)
	}
	if dep.ID != 1 {
		t.Fatal("Expected 1, got", dep.ID)
	}

}
