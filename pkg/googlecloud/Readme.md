# Google Cloud Support Package
In this package there are some structs used common to the commands deployed on
Heroku.

## Environment on Heroku
If the env variable `GOOGLE_APP_ENGINE_KEY` is not found, then the following variables must be set:
- `GOOGLE_KEY` : Google Key to access to the datastore and PubSub Services
- `GOOGLE_APPLICATION_CREDENTIALS` : a fake path where to save the Google Key

The `GOOGLE_PROJECT_ID` env variable must be set also.

If the env variable `GOOGLE_APP_ENGINE_KEY` is found, then `GOOGLE_APPLICATION_CREDENTIALS` should contain the path to the `credentials.json` file and it will be used to load the key to access the Google services.

## Test Environment

Set `GOOGLE_APPLICATION_SET` (empty) to use the `GOOGLE_APPLICATION_CREDENTIALS` already set.
