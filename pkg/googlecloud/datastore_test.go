package googlecloud

import (
	"testing"
)

func TestInitializeDSOnHeroku(t *testing.T) {
	var ds DatastoreGC
	err := ds.InitializeDSOnHeroku()
	if err != nil {
		t.Fatal(err)
	}
}
