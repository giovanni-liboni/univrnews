package googlecloud

import (
	"testing"

	"cloud.google.com/go/datastore"
)

func TestCourseLoad(t *testing.T) {
	var ds DatastoreGC
	err := ds.InitializeDSOnHeroku()
	if err != nil {
		t.Fatal(err)
	}
	PopulateDatastore(t, ds.ClientDS)

	var c Course
	c.ID = 2
	c.DepartmentKey = datastore.IDKey(departmentKind, 1, nil)
	err = c.Load(&ds)
	if err != nil {
		t.Fatal(err)
	}
	if len(c.FollowersKeys) != 2 {
		t.Fatal("Expected 2 followers, got", len(c.FollowersKeys))
	}
}

func TestGetDepartmentUsingParent(t *testing.T) {
	var ds DatastoreGC
	err := ds.InitializeDSOnHeroku()
	if err != nil {
		t.Fatal(err)
	}
	PopulateDatastore(t, ds.ClientDS)

	var c Course
	c.ID = 2
	c.Key = datastore.IDKey(courseKind, 2, datastore.IDKey(departmentKind, 1, nil))
	err = c.GetDepartmentUsingParent(&ds)
	if err != nil {
		t.Fatal(err)
	}
	if len(c.Department.FollowersKeys) != 1 {
		t.Fatal("Expected 1 followers for the department, got", len(c.Department.FollowersKeys))
	}
}
