package googlecloud

import (
	"testing"

	"cloud.google.com/go/datastore"
)

func TestChatLoad(t *testing.T) {
	var ds DatastoreGC
	err := ds.InitializeDSOnHeroku()
	if err != nil {
		t.Fatal(err)
	}
	PopulateDatastore(t, ds.ClientDS)

	var c Chat
	c.ID = 1
	err = c.Load(&ds)
	if err != nil {
		t.Fatal(err)
	}
	if len(c.CoursesKeys) != 2 {
		t.Fatal("Expected to follow 2 courses, got", len(c.CoursesKeys))
	}
}

func TestChatDelete(t *testing.T) {
	var ds DatastoreGC
	err := ds.InitializeDSOnHeroku()
	if err != nil {
		t.Fatal(err)
	}
	// First simple test
	PopulateDatastore(t, ds.ClientDS)
	var c Chat

	// Set the ID of the chat
	c.ID = 1
	err = c.Delete(&ds)
	if err != nil {
		t.Fatal(err)
	}

	err = c.Load(&ds)
	if err != datastore.ErrNoSuchEntity {
		t.Fatal(err)
	}
	// The chat with the ID 1 is in the followers list of Chat 3, check if the
	// function delete also the associated keys from followers list
	course := Course{
		ID: 3,
	}
	err = course.Load(&ds)
	if err != nil {
		t.Fatal(err)
	}
	if len(course.FollowersKeys) != 1 {
		t.Error("Expected len 1 but got ", len(course.FollowersKeys))
	}
}
