package googlecloud

import (
	"context"
	"os"
	"testing"

	"cloud.google.com/go/datastore"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
)

type DatastoreGC struct {
	ClientDS    *datastore.Client
	urlToDep    map[string]Department // Used in department load method
	departments []Department
}

func NewDS() (*DatastoreGC, error) {
	ds := new(DatastoreGC)
	ds.ClientDS = new(datastore.Client)
	log.Info("Initializing Datastore...")
	err := ds.InitializeDSOnHeroku()
	log.Infoln("Done!")
	return ds, err
}

func (d *DatastoreGC) InitializeDSOnHeroku() error {
	var err error
	// Load the context
	ctx := context.Background()
	googleProjectID := os.Getenv("GOOGLE_PROJECT_ID")

	if os.Getenv("GOOGLE_KEY") != "" {
		credentials, err := google.CredentialsFromJSON(ctx, []byte(os.Getenv("GOOGLE_KEY")), datastore.ScopeDatastore)
		if err != nil {
			log.Errorln("google.CredentialsFromJSON:", err)
			return err
		}
		// config, err := google.JWTConfigFromJSON([]byte(os.Getenv("GOOGLE_KEY")), datastore.ScopeDatastore)
		// if err != nil {
		// 	log.Errorln("google.CredentialsFromJSON:", err)
		// 	return err
		// }

		d.ClientDS, err = datastore.NewClient(ctx, googleProjectID, option.WithTokenSource(credentials.TokenSource))
		if err != nil {
			log.Errorln("datastore.NewClient:", err)
			return err
		}

	} else {
		// Here we are in the Test environment
		// Create a new Datastore Client
		d.ClientDS, err = datastore.NewClient(ctx, googleProjectID)
		if err != nil {
			log.Errorln("datastore.NewClient:", err)
			return err
		}
	}

	// Verify that we can communicate and authenticate with the datastore service.
	tr, err := d.ClientDS.NewTransaction(ctx)
	if err != nil {
		log.Errorln("d.ClientDS.NewTransaction", err)
		log.Fatal(err)
	}
	if err = tr.Rollback(); err != nil {
		log.Fatal(err)
	}
	return nil
}

// -----------------------------------------------------------
// Provide some methods to ease the development
// e.g. GetAllDepartments, Key handlers
// -----------------------------------------------------------

func (d *DatastoreGC) GetAllDepartments() ([]Department, error) {
	// Se sono già presenti allora ritorno quelli che ho salvato in memoria
	if len(d.departments) > 0 {
		return d.departments, nil
	}
	// Load the context
	ctx := context.Background()
	q := datastore.NewQuery(departmentKind).Order("ID")
	_, err := d.ClientDS.GetAll(ctx, q, &d.departments)

	if err != nil && err != err.(*datastore.ErrFieldMismatch) {
		log.Warnln("Datastore GetAllDepartments get :", err)
		log.Infoln("Loaded : ", len(d.departments), " departments")
	} else if err != nil {
		log.Errorf("Datastore GetDepartments get: %v", err)
		return d.departments, err
	}
	return d.departments, nil
}

func (d *DatastoreGC) GetDepartmentFromURL(url string) (Department, error) {
	if d.urlToDep == nil {
		d.urlToDep = make(map[string]Department)
	}
	return d.urlToDep[url], nil
}

func (d *DatastoreGC) SetDepartmentFromURL(dep *Department) error {
	if d.urlToDep == nil {
		d.urlToDep = make(map[string]Department)
	}
	d.urlToDep[dep.URL] = *dep
	return nil
}

func (d *DatastoreGC) GetAllChats() ([]Chat, error) {
	// Load the context
	var chats []Chat
	ctx := context.Background()
	q := datastore.NewQuery(chatKind)
	_, err := d.ClientDS.GetAll(ctx, q, &chats)
	if err != nil {
		log.Errorf("Datastore GetAllChats get: %v", err)
		return chats, err
	}
	return chats, nil
}

// -----------------------------------------------------------
// Only for test purpose
// -----------------------------------------------------------

func PopulateDatastore(t *testing.T, client *datastore.Client) {
	ctx := context.Background()
	// Populate Department
	depKey1 := datastore.IDKey(departmentKind, 1, nil)
	depKey2 := datastore.IDKey(departmentKind, 2, nil)

	// Populate datastore
	chatKey1 := datastore.IDKey(chatKind, 1, nil)
	chatKey2 := datastore.IDKey(chatKind, 2, nil)
	chatKey3 := datastore.IDKey(chatKind, 3, nil)
	chatKey4 := datastore.IDKey(chatKind, 4, nil)

	courseKey1 := datastore.IDKey(courseKind, 1, depKey2)
	if _, err := client.Put(ctx, courseKey1, &Course{ID: 1, Name: "Test", FollowersKeys: []*datastore.Key{chatKey3}}); err != nil {
		t.Fatal(err)
	}

	courseKey2 := datastore.IDKey(courseKind, 2, depKey1)
	if _, err := client.Put(ctx, courseKey2, &Course{ID: 2, Name: "Test", FollowersKeys: []*datastore.Key{chatKey1, chatKey2}}); err != nil {
		t.Fatal(err)
	}
	courseKey3 := datastore.IDKey(courseKind, 3, depKey1)
	if _, err := client.Put(ctx, courseKey3, &Course{ID: 3, Name: "Test", FollowersKeys: []*datastore.Key{chatKey1, chatKey2}}); err != nil {
		t.Fatal(err)
	}

	_, err := client.Put(ctx, chatKey1, &Chat{ID: 1, CoursesKeys: []*datastore.Key{courseKey2, courseKey3}})
	if err != nil {
		t.Fatal(err)
	}

	_, err = client.Put(ctx, depKey1, &Department{ID: 1, CoursesKeys: []*datastore.Key{courseKey2, courseKey3}, FollowersKeys: []*datastore.Key{chatKey4}, URL: "http://test.com"})
	if err != nil {
		t.Fatal(err)
	}
	_, err = client.Put(ctx, depKey2, &Department{ID: 2, CoursesKeys: []*datastore.Key{}, FollowersKeys: []*datastore.Key{}, URL: "http://test2.com"})
	if err != nil {
		t.Fatal(err)
	}
}
