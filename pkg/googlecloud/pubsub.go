package googlecloud

import (
	"context"
	"os"

	"cloud.google.com/go/pubsub"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
)

type PubSubGC struct {
	ClientPS *pubsub.Client
}

func NewPS() (*PubSubGC, error) {
	ps := new(PubSubGC)
	ps.ClientPS = new(pubsub.Client)
	return ps, ps.InitializePSOnHeroku()
}

func (ps *PubSubGC) InitializePSOnHeroku() error {
	var err error

	// Load the context
	ctx := context.Background()
	googleProjectID := os.Getenv("GOOGLE_PROJECT_ID")

	if os.Getenv("GOOGLE_KEY") != "" {
		// If we are on Heroku
		credentials, err := google.CredentialsFromJSON(ctx, []byte(os.Getenv("GOOGLE_KEY")), pubsub.ScopePubSub)
		if err != nil {
			return err
		}
		ps.ClientPS, err = pubsub.NewClient(ctx, googleProjectID, option.WithTokenSource(credentials.TokenSource), option.WithScopes(pubsub.ScopePubSub))
		if err != nil {
			log.Infoln("pubsub.NewClient:", err)
			return err
		}
		log.Infoln("pubsub.Client: Heroku init ok")

	} else {
		// Here we are in the Test environment
		// Create a new Datastore Client
		ps.ClientPS, err = pubsub.NewClient(ctx, googleProjectID)
		if err != nil {
			return err
		}
	}

	return nil
}

func (ps *PubSubGC) Publish(topic, msg string) error {
	ctx := context.Background()
	// [START publish]
	t := ps.ClientPS.Topic(topic)
	result := t.Publish(ctx, &pubsub.Message{
		Data: []byte(msg),
	})
	// Block until the result is returned and a server-generated
	// ID is returned for the published message.
	_, err := result.Get(ctx)
	if err != nil {
		return err
	}
	// fmt.Printf("Published a message ==> ID: %v\n", id)
	// [END publish]
	return nil
}
