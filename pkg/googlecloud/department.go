package googlecloud

import (
	"context"
	"encoding/json"
	"errors"
	"net/url"
	"strings"

	"cloud.google.com/go/datastore"
	log "github.com/sirupsen/logrus"
	"google.golang.org/api/iterator"
)

const departmentKind = "Department"

type Department struct {
	Name          string `db:"nome"`
	ID            int64  `db:"id"`
	RSSLink       string `db:"rss_link"`
	URL           string `db:"url"`
	NoticesURL    string
	CrawlingURL   string           `datastore:"-"`
	CoursesKeys   []*datastore.Key `json:"-"`
	FollowersKeys []*datastore.Key `json:"-"`
	Key           *datastore.Key   `datastore:"__key__" json:"-"`
	Courses       []*Course        `datastore:"-"`
}

func (d *Department) ToByteArray() []byte {
	b, _ := json.Marshal(d)
	return []byte(b)
}

// Save the department to the Datastore
func (d *Department) Save(ds *DatastoreGC) error {
	ctx := context.Background()

	if strings.Contains(d.URL, "http") {
		// Change URL to remove http:// or https://
		u, err := url.Parse(d.URL)
		if err != nil {
			log.WithField("model", "department").WithField("method", "save").Errorln(err)
			return err
		}
		d.URL = u.Hostname()
	}

	if d.ID == 0 {
		err := errors.New("Set a valid ID for the Department")
		log.WithField("model", "department").WithField("method", "save").Errorln(err)
		return err
	}

	d.Key = datastore.IDKey(departmentKind, d.ID, nil)

	if err := d.checkConsistencyCourses(); err != nil {
		log.WithField("model", "department").WithField("method", "save").Errorln(err)
		return err
	}

	if _, err := ds.ClientDS.Put(ctx, d.Key, d); err != nil {
		log.WithField("model", "department").WithField("method", "save/put").Errorln(err)
		return err
	}

	// Update also the department in memory
	return ds.SetDepartmentFromURL(d)
}

// Load the department from the Datastore. If the KEY is not specified, then it
// tries to load it by URL. It overwrites the entity with the data contains in
// the datastore.
// If the REDIS client is specified then it will be used as cache
func (d *Department) Load(ds *DatastoreGC) error {
	ctx := context.Background()
	var query *datastore.Query
	var err error
	// Try first with the key
	if d.ID != 0 {
		d.Key = datastore.IDKey(departmentKind, d.ID, nil)
	} else if d.URL != "" {
		// This means that we are looking up by URL
		// We can lookup at the DatastoreGC in-memory system to find the ID
		dep, _ := ds.GetDepartmentFromURL(d.URL)
		if dep.ID != 0 {
			// URL found, build the Key and use it
			d.Key = datastore.IDKey(departmentKind, dep.ID, nil)
		} else {
			query = datastore.NewQuery(departmentKind).Filter("URL =", d.URL)
			if query == nil {
				return errors.New("It is not possible to load the department: any identifier is specified")
			}
			it := ds.ClientDS.Run(ctx, query)
			d.Key, err = it.Next(d)
			if err != nil && err != iterator.Done {
				log.WithField("model", "department").WithField("method", "load/Run").Errorln(err)
				return err
			}
		}
	}
	if d.Key != nil {
		err = ds.ClientDS.Get(ctx, d.Key, d)
		if err != nil && err != datastore.ErrNoSuchEntity {
			log.WithField("model", "department").WithField("method", "load/get").Errorln(err)
			return err
		}
	}
	return ds.SetDepartmentFromURL(d)
}

func (d *Department) GetFollowers(ds *DatastoreGC) ([]int64, error) {
	var res []int64
	// Check if the department is already loaded
	err := d.Load(ds)
	if err != nil {
		log.WithField("model", "department").WithField("method", "GetFollowers").Errorln(err)
		return res, err
	}
	for _, chatKey := range d.FollowersKeys {
		res = append(res, chatKey.ID)
	}
	return res, nil
}

func (d *Department) GetFollowersWithCourses(ds *DatastoreGC) ([]int64, error) {
	// Load the department followers
	res, err := d.GetFollowers(ds)
	if err != nil {
		log.WithField("model", "department").WithField("method", "GetFollowersWithCourses/GetFollowers").Errorln(err)
		return res, err
	}
	for _, courseKey := range d.CoursesKeys {
		c := Course{
			ID:  courseKey.ID,
			Key: courseKey,
		}
		c.SetDepartment(d)
		err = c.Load(ds)
		if err != nil {
			log.WithField("model", "department").WithField("method", "GetFollowersWithCourses/LoadDepartment").Errorln(err)
			return res, err
		}
		for _, chatKey := range c.FollowersKeys {
			res = append(res, chatKey.ID)
		}
	}

	return res, nil
}

func (d *Department) GetCourses(ds *DatastoreGC) ([]*Course, error) {
	var res []*Course
	err := d.Load(ds)
	if err != nil {
		return res, err
	}
	for _, courseKey := range d.CoursesKeys {
		c := Course{
			ID:  courseKey.ID,
			Key: courseKey,
		}
		c.SetDepartment(d)
		err = c.Load(ds)
		if err != nil && err != datastore.ErrNoSuchEntity {
			log.Println(datastore.ErrNoSuchEntity)
			log.WithField("model", "department").WithField("method", "GetCourses/LoadCourse").Errorln(err)
			return res, err
		}
		res = append(res, &c)
	}
	return res, nil
}

// SetKey of the department
func (d *Department) SetKey() {
	d.Key = datastore.IDKey(departmentKind, d.ID, nil)
}

// AddCourse saves a course in the coursesKeys
func (d *Department) AddCourse(c *Course) error {
	if d.CoursesKeys == nil {
		d.CoursesKeys = []*datastore.Key{}
	}
	if c.Key == nil {
		if c.ID == 0 || d.ID == 0 {
			return errors.New("AddCourse: Set the Course Key before add it to a Department")
		}
		// Otherwise it is possible to create a new key
		d.Key = datastore.IDKey(departmentKind, d.ID, nil)
		c.Key = datastore.IDKey(courseKind, c.ID, d.Key)
	}
	// Check if course already exists in the FollowersKeys of the course
	insertCourse := true
	for _, chatCourseTmp := range d.CoursesKeys {
		if chatCourseTmp.Equal(c.Key) {
			insertCourse = false
		}
	}
	// If not, append it
	if insertCourse {
		d.CoursesKeys = append(d.CoursesKeys, c.Key)
		d.Courses = append(d.Courses, c)
	}
	return nil
}

// AddChat Add a follower chat key to the followers of this course
func (d *Department) AddChat(id int64) {
	chatKey := datastore.IDKey(chatKind, id, nil)
	if d.FollowersKeys == nil {
		d.FollowersKeys = []*datastore.Key{}
	}
	// Check if chat already exists in the FollowersKeys of the course
	insertChat := true
	for _, chatKeyTmp := range d.FollowersKeys {
		if chatKeyTmp.Equal(chatKey) {
			insertChat = false
		}
	}
	// If not, append it
	if insertChat {
		d.FollowersKeys = append(d.FollowersKeys, chatKey)
	}
}

// checkConsistencyCourses checks if all courses in the department are valid and
// it is possible to save them without (possibly) any errors
func (d *Department) checkConsistencyCourses() error {
	// If there are courses, then fix all the keys to avoid problem during the save
	// method
	// if len(d.Courses) > 0 {
	// 	for _, c := range d.Courses {
	// 		if c.ID == 0 {
	// 			return errors.New("Error saving department : Missing ID Course for " + c.Name)
	// 		}
	// 		c.SetDepartment(d)
	// 		d.AddCourse(c)
	// 	}
	// }
	return nil
}

func (c *Department) RemoveChat(ds *DatastoreGC, id int64) error {
	err := c.Load(ds)
	if err != nil {
		return err
	}
	for i := len(c.FollowersKeys) - 1; i >= 0; i-- {
		if c.FollowersKeys[i].ID == id {
			c.FollowersKeys = append(c.FollowersKeys[:i], c.FollowersKeys[i+1:]...)
		}
	}
	return nil
}

// func (d *Dispatcher) GetDepartmentFromURL(urlS string) (int64, error) {
// 	// Used an instance of the connection to the redis database to check if an
// 	// entity with the "url" key is inside the db
// 	res, err := rds.GetDB().Get(urlS).Int64()
// 	if err != nil {
// 		// Value not found
// 		// We have to populate the redis db with all the departments and their ids
// 		if err = d.loadAllDepartmentInRedis(); err != nil {
// 			return 0, err
// 		}
// 		res, err = rds.GetDB().Get(urlS).Int64()
// 		if err != nil {
// 			return 0, err
// 		}
// 		return res, nil
// 	}
// 	return res, nil
// }

// func loadAllDepartmentInRedis() error {
// 	ctx := context.Background()
// 	i := 0
// 	query := datastore.NewQuery("Department")
// 	it := d.ds.ClientDS.Run(ctx, query)
// 	for {
// 		var dep Department
// 		_, err := it.Next(&dep)
// 		if err == iterator.Done {
// 			break
// 		}
// 		if err != nil {
// 			return err
// 		}
//
// 		err = d.rds.Set(dep.URL, dep.ID, 24*time.Hour).Err()
// 		if err != nil {
// 			return err
// 		}
// 		i++
// 	}
// 	return nil
// }
