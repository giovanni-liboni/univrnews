package googlecloud

import (
	"context"
	"encoding/json"
	"errors"
	"time"

	"cloud.google.com/go/datastore"
	log "github.com/sirupsen/logrus"
)

const chatKind = "Chat"

// Chat structs represents the Telegram Chat
type Chat struct {
	ID          int64     `db:"telegram_id"`
	CreatedAT   time.Time `db:"created_at"`
	Status      int       `db:"status"`
	CoursesKeys []*datastore.Key
	DepKeys     []*datastore.Key
	Key         *datastore.Key `datastore:"__key__"`
}

func (c *Chat) ToByteArray() []byte {
	b, _ := json.Marshal(c)
	return []byte(b)
}

func (c *Chat) GetKey() *datastore.Key {
	if c.ID != 0 {
		c.Key = datastore.IDKey(chatKind, c.ID, nil)
	}
	return c.Key
}

// Save the chat to the Datastore
func (c *Chat) Save(client *datastore.Client) {
	ctx := context.Background()
	c.Key = datastore.IDKey(chatKind, c.ID, nil)
	if _, err := client.Put(ctx, c.Key, c); err != nil {
		log.Fatalf("Failed to save chat: %v", err)
	}
}

// Load the chat from the Datastore. It overwrites the entity with the data
// contains in the datastore.
// If the REDIS client is specified then it will be used as cache
func (c *Chat) Load(ds *DatastoreGC) error {
	ctx := context.Background()
	// Try first with the key
	if c.ID != 0 {
		c.Key = datastore.IDKey(chatKind, c.ID, nil)
	}
	if c.Key != nil {
		return ds.ClientDS.Get(ctx, c.Key, c)
	}
	return errors.New("It is not possible to load the chat: any identifier specified")
}

// Delete the chat from the Datastore
func (c *Chat) Delete(ds *DatastoreGC) error {
	ctx := context.Background()
	err := c.Load(ds)
	if err != nil {
		log.Errorln("Delete.Load:", err)
		return err
	}
	for _, v := range c.CoursesKeys {
		course := Course{
			ID:  v.ID,
			Key: v,
		}
		err = course.Load(ds)
		if err != nil {
			log.Errorln("Delete.LoadCourse:", err)
		} else {
			err = course.RemoveChat(ds, c.ID)
			if err != nil {
				log.Errorln("Delete.RemoveChatCourse:", err)
			} else {
				course.Save(ds.ClientDS)
			}
		}
	}
	for _, v := range c.DepKeys {
		dep := Department{
			ID:  v.ID,
			Key: v,
		}
		err = dep.Load(ds)
		if err != nil {
			log.Errorln("Delete.LoadDepartment:", err)
		} else {
			err = dep.RemoveChat(ds, c.ID)
			if err != nil {
				log.Errorln("Delete.RemoveChatDepartment:", err)
			} else {
				dep.Save(ds)
			}
		}
	}
	err = ds.ClientDS.Delete(ctx, c.Key)
	if err != nil {
		log.Errorln("Delete.RemoveChat:", err)
		return err
	}
	log.Infoln("Deleted user", c.ID)
	return nil
}

func (c *Chat) GetSubscribedCourses(ds *DatastoreGC) ([]Course, error) {
	var res []Course
	err := c.Load(ds)
	if err != nil {
		return nil, err
	}
	for _, courseKey := range c.CoursesKeys {
		c := Course{
			ID:  courseKey.ID,
			Key: courseKey,
		}
		err = c.Load(ds)
		if err != nil {
			return res, err
		}
		res = append(res, c)
	}
	return res, nil
}

func (c *Chat) AddCourse(courseKey *datastore.Key) {
	if c.CoursesKeys == nil {
		c.CoursesKeys = []*datastore.Key{}
	}
	// Check if course already exists in the FollowersKeys of the course
	insertCourse := true
	for _, chatCourseTmp := range c.CoursesKeys {
		if chatCourseTmp.Equal(courseKey) {
			insertCourse = false
		}
	}
	// If not, append it
	if insertCourse {
		c.CoursesKeys = append(c.CoursesKeys, courseKey)
	}
}

func (c *Chat) AddDepartment(d Department) {
	if c.DepKeys == nil {
		c.DepKeys = []*datastore.Key{}
	}
	// Check if course already exists in the FollowersKeys of the course
	insertCourse := true
	for _, chatCourseTmp := range c.DepKeys {
		if chatCourseTmp.Equal(d.Key) {
			insertCourse = false
		}
	}
	// If not, append it
	if insertCourse {
		c.DepKeys = append(c.DepKeys, d.Key)
	}
}

func (c *Chat) RemoveCourse(ds *DatastoreGC, id int64) error {
	err := c.Load(ds)
	if err != nil {
		return err
	}

	for i := len(c.CoursesKeys) - 1; i >= 0; i-- {
		if c.CoursesKeys[i].ID == id {
			c.CoursesKeys = append(c.CoursesKeys[:i], c.CoursesKeys[i+1:]...)
		}
	}
	return nil
}
