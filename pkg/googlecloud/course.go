package googlecloud

import (
	"context"
	"encoding/json"
	"errors"

	"cloud.google.com/go/datastore"
	log "github.com/sirupsen/logrus"
	"google.golang.org/api/iterator"
)

const courseKind = "Course"

type Course struct {
	ID            int64  `db:"id"`
	Name          string `db:"nome"`
	CrawlingURL   string
	NoticesURL    string
	FollowersKeys []*datastore.Key
	Key           *datastore.Key `datastore:"__key__"`
	DepartmentKey *datastore.Key `datastore:"-"`
	Department    *Department    `json:"-"`
}

func (c *Course) ToByteArray() []byte {
	b, _ := json.Marshal(c)
	return []byte(b)
}

// Save the course to the Datastore
func (c *Course) Save(client *datastore.Client) {
	ctx := context.Background()
	if _, err := client.Put(ctx, c.Key, c); err != nil {
		log.Fatalf("Failed to save course: %v", err)
	}
}

// Load the chat from the Datastore. It overwrites the entity with the data
// contains in the datastore.
// If the REDIS client is specified then it will be used as cache
func (c *Course) Load(ds *DatastoreGC) error {
	ctx := context.Background()
	var err error
	// Check if the department key is set
	if c.Key == nil && c.DepartmentKey == nil {
		if c.ID != 0 {
			// Go with a normal query
			query := datastore.NewQuery(courseKind).Filter("ID =", c.ID)
			it := ds.ClientDS.Run(ctx, query)
			c.Key, err = it.Next(c)
			if err != nil && err != iterator.Done {
				return err
			}
			if c.Key != nil {
				c.DepartmentKey = c.Key.Parent
			}
			return nil
		}
		return errors.New("Error loading course: set the department key")
	}
	// Try first with the ID
	if c.ID != 0 && c.DepartmentKey != nil {
		c.Key = datastore.IDKey(courseKind, c.ID, c.DepartmentKey)
	}
	if c.Key != nil {
		return ds.ClientDS.Get(ctx, c.Key, c)
	}
	return errors.New("It is not possible to load the course: any identifier specified")
}

func (c *Course) GetFollowers(ds *DatastoreGC) ([]int64, error) {
	var res []int64
	err := c.Load(ds)
	if err != nil {
		return res, err
	}
	for _, chatKey := range c.FollowersKeys {
		res = append(res, chatKey.ID)
	}
	return res, nil
}

func (c *Course) GetFollowersWithDepartment(ds *DatastoreGC) ([]int64, error) {
	res, err := c.GetFollowers(ds)
	if err != nil {
		return res, err
	}
	err = c.GetDepartmentUsingParent(ds)
	if err != nil {
		return res, err
	}

	for _, chatKey := range c.Department.FollowersKeys {
		res = append(res, chatKey.ID)
	}

	return res, nil
}

// SetDepartment set the department key and modifies the course key according
// to the new department key
func (c *Course) SetDepartment(d *Department) {
	c.DepartmentKey = datastore.IDKey(departmentKind, d.ID, nil)
	c.Key = datastore.IDKey(courseKind, c.ID, c.DepartmentKey)
	c.Department = d
}

// AddChat Add a follower chat key to the followers of this course
func (c *Course) AddChat(chatKey *datastore.Key) {
	if c.FollowersKeys == nil {
		c.FollowersKeys = []*datastore.Key{}
	}
	// Check if chat already exists in the FollowersKeys of the course
	insertChat := true
	for _, chatKeyTmp := range c.FollowersKeys {
		if chatKeyTmp.Equal(chatKey) {
			insertChat = false
		}
	}
	// If not, append it
	if insertChat {
		c.FollowersKeys = append(c.FollowersKeys, chatKey)
	}
}

// GetDepartmentUsingParent uses the parent option to retrieve the department
// from the Datastore. The Course Key MUST be set.
func (c *Course) GetDepartmentUsingParent(ds *DatastoreGC) error {
	ctx := context.Background()
	if c.Key == nil {
		return errors.New("The Course KEY must be set")
	}
	var dep Department
	err := ds.ClientDS.Get(ctx, c.Key.Parent, &dep)
	if err != nil {
		return err
	}
	c.SetDepartment(&dep)
	return nil
}

func (c *Course) RemoveChat(ds *DatastoreGC, id int64) error {
	err := c.Load(ds)
	if err != nil {
		return err
	}
	for i, key := range c.FollowersKeys {
		if key.ID == id {
			c.FollowersKeys = append(c.FollowersKeys[:i], c.FollowersKeys[i+1:]...)
			return nil
		}
	}
	// for i := len(c.FollowersKeys) - 1; i >= 0; i-- {
	// 	if c.FollowersKeys[i].ID == id {
	// 		c.FollowersKeys = append(c.FollowersKeys[:i], c.FollowersKeys[i+1:]...)
	// 	}
	// }
	return nil
}
