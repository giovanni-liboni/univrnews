package googlecloud

import (
	"context"
	"time"

	"cloud.google.com/go/datastore"
	log "github.com/sirupsen/logrus"
)

const newsKind = "News"

type News struct {
	ID           int64
	SentTime     time.Time
	PubTime      time.Time // Pubblication time
	ModTime      time.Time // Modification time
	DepartmentID int64
	CourseID     int64
}

// Sent tags the news as Sent and saves it into the Datastore
func (n *News) Sent(ds *DatastoreGC) error {
	ctx := context.Background()
	var key *datastore.Key
	if n.DepartmentID == 0 && n.CourseID == 0 {
		key = datastore.IDKey(newsKind, n.ID, nil)
	} else if n.CourseID == 0 {
		key = datastore.IDKey(newsKind, n.ID, datastore.IDKey(departmentKind, n.DepartmentID, nil))
	} else {
		key = datastore.IDKey(newsKind, n.ID, datastore.IDKey(courseKind, n.CourseID, datastore.IDKey(departmentKind, n.DepartmentID, nil)))
	}
	tx, err := ds.ClientDS.NewTransaction(ctx)
	if err != nil {
		log.Fatalf("Sent.client.NewTransaction: %v", err)
		return err
	}
	n.SentTime = time.Now().Round(time.Second)
	if _, err = tx.Put(key, n); err != nil {
		tx.Rollback()
		log.Errorln("Sent.tx.PutMulti:", err)
		return err
	}
	if _, err = tx.Commit(); err != nil {
		log.Fatalf("Sent.tx.Commit: %v", err)
	}
	return nil
}

// IsSendable returns true if it is allowed to send the news, false otherwise
// MUST Set both ModTime and PubTime
func (n *News) IsSendable(ds *DatastoreGC) bool {
	var nn News
	var key *datastore.Key
	res := false
	ctx := context.Background()
	if n.DepartmentID == 0 && n.CourseID == 0 {
		key = datastore.IDKey(newsKind, n.ID, nil)
	} else if n.CourseID == 0 {
		key = datastore.IDKey(newsKind, n.ID, datastore.IDKey(departmentKind, n.DepartmentID, nil))
	} else {
		key = datastore.IDKey(newsKind, n.ID, datastore.IDKey(courseKind, n.CourseID, datastore.IDKey(departmentKind, n.DepartmentID, nil)))
	}

	tx, err := ds.ClientDS.NewTransaction(ctx)
	if err != nil {
		log.Fatalf("IsSendable.client.NewTransaction: %v", err)
	}
	if err = tx.Get(key, &nn); err != nil && err != datastore.ErrNoSuchEntity {
		tx.Rollback()
		log.Errorln("IsSendable.tx.Get:", err)
	}

	// Entity found
	if err == nil {
		// It has already been sent once but if the modification time is greater it
		// means that we need to resend it
		nn.ModTime = nn.ModTime.Round(time.Second)
		n.ModTime = n.ModTime.Round(time.Second)

		if nn.ModTime.Equal(n.ModTime) {
			res = false
		} else if nn.ModTime.Before(n.ModTime) {
			res = true
		}

		// Last check
		// If the sent time is greater than the modification time => Already sent
		if nn.SentTime.After(n.ModTime) {
			res = false
		}
		// Not saved at the end of the sent phase
		if nn.SentTime.IsZero() {
			res = false
			log.Errorln("Sent time for", nn.ID, "is not set but someone ask to send it")
		}
		if n.ModTime.IsZero() && nn.SentTime.After(n.PubTime) {
			res = false
		}

		if _, err = tx.Commit(); err != nil {
			log.Fatalf("IsSendable.tx.Commit: %v", err)
		}
		return res
	}
	// First miss
	if err == datastore.ErrNoSuchEntity {
		res = true
	} else {
		log.Errorln("IsSendable.IsSendable:", err)
	}
	// Entity not found
	if _, err = tx.Put(key, n); err != nil {
		tx.Rollback()
		log.Fatalf("IsSendable.tx.PutMulti: %v", err)
	}
	if _, err = tx.Commit(); err != nil {
		log.Fatalf("IsSendable.tx.Commit: %v", err)
	}
	return res
}

func (n *News) Remove(ds *DatastoreGC) error {
	var key *datastore.Key
	ctx := context.Background()
	if n.DepartmentID == 0 && n.CourseID == 0 {
		key = datastore.IDKey(newsKind, n.ID, nil)
	} else if n.CourseID == 0 {
		key = datastore.IDKey(newsKind, n.ID, datastore.IDKey(departmentKind, n.DepartmentID, nil))
	} else {
		key = datastore.IDKey(newsKind, n.ID, datastore.IDKey(courseKind, n.CourseID, datastore.IDKey(departmentKind, n.DepartmentID, nil)))
	}
	tx, err := ds.ClientDS.NewTransaction(ctx)
	if err != nil {
		log.Fatalf("client.NewTransaction: %v", err)
		return err
	}
	err = tx.Delete(key)
	if err != nil {
		log.Fatalf("tx.Delete: %v", err)
		return err
	}
	if _, err = tx.Commit(); err != nil {
		log.Fatalf("tx.Commit: %v", err)
	}
	return nil
}
