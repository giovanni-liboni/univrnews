package googlecloud

import (
	"testing"
	"time"
)

func TestNewsIsSendable(t *testing.T) {
	ds, err := NewDS()
	if err != nil {
		t.Fatal(err)
	}
	n := News{
		ID:           1,
		PubTime:      time.Now(),
		ModTime:      time.Now(),
		DepartmentID: 1,
	}
	err = n.Remove(ds)
	if err != nil {
		t.Error(err)
	}

	res := n.IsSendable(ds)
	if !res {
		t.Fatal("Expected true but get false")
	}
	err = n.Sent(ds)
	if err != nil {
		t.Error(err)
	}

	// It should be false
	res = n.IsSendable(ds)
	if res {
		t.Fatal("Expected false but get true")
	}

	n.ModTime = time.Now().AddDate(0, 0, 1)

	res = n.IsSendable(ds)
	if !res {
		t.Fatal("Expected true but get false")
	}
	err = n.Sent(ds)
	if err != nil {
		t.Error(err)
	}

	res = n.IsSendable(ds)
	if res {
		t.Fatal("Expected false but get true")
	}

	// New version with the course
	n = News{
		ID:           1,
		PubTime:      time.Now(),
		ModTime:      time.Now(),
		DepartmentID: 1,
		CourseID:     1,
	}
	err = n.Remove(ds)
	if err != nil {
		t.Error(err)
	}

	res = n.IsSendable(ds)
	if !res {
		t.Fatal("Expected true but get false")
	}
	err = n.Sent(ds)
	if err != nil {
		t.Error(err)
	}

	// It should be false
	res = n.IsSendable(ds)
	if res {
		t.Fatal("Expected false but get true")
	}

	n.ModTime = time.Now().AddDate(0, 0, 1)

	res = n.IsSendable(ds)
	if !res {
		t.Fatal("Expected true but get false")
	}
	err = n.Sent(ds)
	if err != nil {
		t.Error(err)
	}

	res = n.IsSendable(ds)
	if res {
		t.Fatal("Expected false but get true")
	}
}
