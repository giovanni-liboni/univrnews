package utils

import (
	"strings"
	"testing"
)

func TestNormalize(t *testing.T) {
	var tests = []struct {
		address string // input
		addressNormalized string // expected result
	}{
		{"http://www.di.univr.it/?ent=avviso&dest=&id=1", "http://www.di.univr.it/?ent=avviso&dest=&id=1"},
		{"https://www.di.univr.it/?ent=avviso&dest=&id=2", "https://www.di.univr.it/?ent=avviso&dest=&id=2"},
		{"hps://www.di.univr.it/?ent=avviso&dest=&id=3", "http://www.di.univr.it/?ent=avviso&dest=&id=3"},
		{"www.di.univr.it/?ent=avviso&dest=&id=4", "http://www.di.univr.it/?ent=avviso&dest=&id=4"},
		{"/?ent=avviso&dest=&id=5", "/?ent=avviso&dest=&id=5"},
	}
	for _, tt := range tests {
		if strings.Compare(tt.addressNormalized, NormalizeURL(tt.address)) != 0 {
			t.Error("Expected result is ", tt.addressNormalized, "but got", NormalizeURL(tt.address))
		}
	}
}

func TestRetrieveData(t *testing.T) {
	res, err := retrieveData("", "")
	if err != nil {
		t.Error(err)
	}
	if !res.IsZero() {
		t.Error("Passed zero data but got", res)
	}
}