package utils

import (
	"errors"
	"net/url"
	"regexp"
	"strings"
	"time"
	"unicode"
)

// Given an URL in the form https://example.org or example.org or http://example.org, it returns always the
// form http://example.org
func NormalizeURL(address string) string {
	// Just to be sure
	address = strings.TrimSuffix(address, "\n")
	// Retrieve the host
	// Set the URL for the notices
	urlAddressNormalized, _ := url.Parse(address)
	// Check if the scheme is http
	if urlAddressNormalized.Scheme != "http" && urlAddressNormalized.Scheme != "https" {
		urlAddressNormalized.Scheme = "http"
	}
	urlAddressNormalized, _ = url.Parse(urlAddressNormalized.String())
	if strings.Compare("", urlAddressNormalized.Host) == 0 {
		return address
	}
	return urlAddressNormalized.String()
}


// getDate gets the date and time from the date string.
// For future reference to how format the date and time: https://programming.guide/go/format-parse-string-time-date-example.html
func GetDate(s string) (t time.Time, err error) {
	s = RemoveExtraSpaces(s)
	s = strings.ReplaceAll(s, "\n", "")

	if len(s) > 0 {
		romeLoc, _ := time.LoadLocation("Europe/Rome")
		t, err = time.ParseInLocation("02/01/06 15.04", s, romeLoc)
		if t.IsZero() {
			t, err = time.ParseInLocation("Monday,January2,2006-15:4:5PM", SpaceMap(s), romeLoc)
		}
		return t, err
	}
	return t, errors.New("the length of the string must be > 0")
}

func RemoveExtraSpaces(s string) string {
	reLeadcloseWhtsp := regexp.MustCompile(`^[\s\p{Zs}]+|[\s\p{Zs}]+$`)
	reInsideWhtsp := regexp.MustCompile(`[\s\p{Zs}]{2,}`)
	final := reLeadcloseWhtsp.ReplaceAllString(s, "")
	final = reInsideWhtsp.ReplaceAllString(final, " ")
	return final
}

// SpaceMap remove unwanted symbol inside a string
func SpaceMap(str string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		if unicode.IsSymbol(r) {
			return -1
		}
		if strings.IndexRune(".!$", r) == 0 {
			return -1
		}
		return r
	}, str)
}