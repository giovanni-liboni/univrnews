package telebot

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

type WebHook struct {
	Host   string
	Router *mux.Router
	Path   string
}

type HookURL interface {
	Listen(b *Bot)
}

func (w *WebHook) Listen(b *Bot) {
	http.HandleFunc(w.Path, func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "POST" {
			b.RequestHTTP = r
			// if b.Me == nil {
			// 	user, err := b.getMe()
			// 	if err != nil {
			// 		b.debug(err)
			// 		return
			// 	}
			// 	b.Me = user
			// }
			b.ResponseWriter = w

			decoder := json.NewDecoder(r.Body)
			var update Update
			err := decoder.Decode(&update)
			if err != nil {
				b.debug(err)
				return
			}
			update.HTTPRequest = r
			if update.Callback != nil {
				update.Callback.HTTPRequest = r
			}
			if update.Message != nil {
				update.Message.HTTPRequest = r
			}
			b.incomingUpdate(&update)
		}
	})

	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), nil))
}
