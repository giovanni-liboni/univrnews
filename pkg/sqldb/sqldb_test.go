package sqldb

import "testing"

func TestGetDB(t *testing.T) {
	err := GetDB().Ping()
	if err != nil {
		t.Error("Expected valid SQL client")
	}
}
