package sqldb

import (
	"log"
	"os"
	"sync"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// POSTGRESQL
var DB *sqlx.DB
var once sync.Once

func GetDB() *sqlx.DB {
	once.Do(func() {
		var err error
		log.Println("Connetting to Postgres...")
		DB, err = sqlx.Open("postgres", os.Getenv("DATABASE_URL"))
		if err != nil {
			log.Fatalln(err)
		}

		if err = DB.Ping(); err != nil {
			log.Fatalln(err)
		}
	})
	return DB
}
