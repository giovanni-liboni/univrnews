package rds

import (
	"log"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"gopkg.in/redis.v4"
)

/*
	Link to documentation:
		https://godoc.org/gopkg.in/redis.v4#example-Client-Incr
*/

// REDIS
var onceRedis sync.Once
var CL *redis.Client

func GetDB() *redis.Client {
	onceRedis.Do(func() {
		log.Println("Connetting to REDIS...")
		opts, err := getRedisOptions(os.Getenv("REDIS_URL"))
		CL = redis.NewClient(opts)

		_, err = CL.Ping().Result()
		if err != nil {
			log.Fatalln(err.Error())
		}
		log.Println("Connected successfully to REDIS")
	})
	return CL
}

func getRedisOptions(redisURL string) (*redis.Options, error) {
	options := new(redis.Options)
	redisInfo, err := url.Parse(redisURL)
	if err != nil {
		return nil, err
	}

	options.Addr = redisInfo.Host
	if redisInfo.User != nil {
		options.Password, _ = redisInfo.User.Password()
	}

	tmpDB := strings.Split(redisInfo.Path, "/")
	if len(tmpDB) > 1 {
		options.DB, err = strconv.Atoi(tmpDB[1])
		if err != nil {
			return nil, err
		}
	} else {
		options.DB = 0
	}
	options.MaxRetries = 2
	options.IdleTimeout = 5 * time.Minute

	return options, nil
}
