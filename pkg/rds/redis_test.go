package rds

import "testing"

func TestGetRedisOptions(t *testing.T) {
	redisString := "redis://user:password@example.com:8080/1?foo=bar&qux=baz"

	res, err := getRedisOptions(redisString)

	if err != nil {
		t.Error(err)
	}

	if res.Addr != "example.com:8080" {
		t.Error("Expected example.com:8080 but got", res.Addr)
	}
	if res.Password != "password" {
		t.Error("Expected password but got", res.Addr)
	}
}

func TestGetDB(t *testing.T) {
	cl := GetDB().Ping()
	if cl.Err() != nil {
		t.Error("Expected a valid Redis Client")
	}
}
